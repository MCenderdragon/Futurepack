package futurepack.client.model;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import javax.annotation.Nullable;

import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.mojang.datafixers.util.Pair;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.renderer.block.model.BlockModel;
import net.minecraft.client.renderer.block.model.ItemOverrides;
import net.minecraft.client.renderer.block.model.ItemTransforms;
import net.minecraft.client.renderer.block.model.ItemTransforms.TransformType;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.resources.model.Material;
import net.minecraft.client.resources.model.ModelBakery;
import net.minecraft.client.resources.model.ModelState;
import net.minecraft.client.resources.model.UnbakedModel;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.client.model.IModelConfiguration;
import net.minecraftforge.client.model.IModelLoader;
import net.minecraftforge.client.model.SeparatePerspectiveModel;

public class FixedSeparatePerspektiveModel extends SeparatePerspectiveModel 
{
	protected final BlockModel baseModel;
	protected final ImmutableMap<ItemTransforms.TransformType, BlockModel> perspectives;
	protected final ThreadLocal<Integer> stackDepth = ThreadLocal.withInitial(() -> 0);

    
	public FixedSeparatePerspektiveModel(BlockModel baseModel, ImmutableMap<TransformType, BlockModel> perspectives) 
	{
		super(baseModel, perspectives);
		this.baseModel = baseModel;
		this.perspectives = perspectives;
	}
	
	@Override
    public net.minecraft.client.resources.model.BakedModel bake(IModelConfiguration owner, ModelBakery bakery, Function<Material, TextureAtlasSprite> spriteGetter, ModelState modelTransform, ItemOverrides overrides, ResourceLocation modelLocation)
    {
        return new SBakedModel(
                owner.useSmoothLighting(), owner.isShadedInGui(), owner.isSideLit(),
                spriteGetter.apply(owner.resolveTexture("particle")), overrides,
                baseModel.bake(bakery, baseModel, spriteGetter, modelTransform, modelLocation, owner.isSideLit()),
                ImmutableMap.copyOf(Maps.transformValues(perspectives, value -> {
                    return value.bake(bakery, value, spriteGetter, modelTransform, modelLocation, owner.isSideLit());
                }))
        );
    }
	
	@Override
	public Collection<Material> getTextures(IModelConfiguration owner, Function<ResourceLocation, UnbakedModel> modelGetter, Set<Pair<String, String>> missingTextureErrors) 
	{
		if(stackDepth.get() > 1)
			return Collections.emptySet();
		stackDepth.set(stackDepth.get()+1);
		Collection<Material> col = super.getTextures(owner, modelGetter, missingTextureErrors);
		stackDepth.set(stackDepth.get()-1);
		return col;
	}

	public static class SBakedModel extends SeparatePerspectiveModel.BakedModel
    {
		private final net.minecraft.client.resources.model.BakedModel baseModel;
        private final ImmutableMap<ItemTransforms.TransformType, net.minecraft.client.resources.model.BakedModel> perspectives;
        
        public SBakedModel(boolean isAmbientOcclusion, boolean isGui3d, boolean isSideLit, TextureAtlasSprite particle, ItemOverrides overrides, net.minecraft.client.resources.model.BakedModel baseModel, ImmutableMap<ItemTransforms.TransformType, net.minecraft.client.resources.model.BakedModel> perspectives)
        {
           super(isAmbientOcclusion, isGui3d, isSideLit, particle, overrides, baseModel, perspectives);
           this.baseModel = baseModel;
           this.perspectives = perspectives;
        }

        @Override
        public ItemOverrides getOverrides() 
        {
        	return new ItemOverrides() 
        	{
        		public BakedModel resolve(net.minecraft.client.resources.model.BakedModel pModel, ItemStack pStack, @Nullable ClientLevel pLevel, @Nullable LivingEntity pEntity, int pSeed) 
        		{
        			net.minecraft.client.resources.model.BakedModel baseM = baseModel.getOverrides().resolve(pModel, pStack, pLevel, pEntity, pSeed);
        			
        			var build = ImmutableMap.<ItemTransforms.TransformType, net.minecraft.client.resources.model.BakedModel>builder();
        			perspectives.forEach((t,m) -> {
        				build.put(t, m.getOverrides().resolve(pModel, pStack, pLevel, pEntity, pSeed));
        			});
        			
        			return new SeparatePerspectiveModel.BakedModel(useAmbientOcclusion(), isGui3d(), usesBlockLight(), getParticleIcon(), SBakedModel.super.getOverrides(), baseM, build.build());
        		}
        	};
        }
    }
	
	public static class Loader implements IModelLoader<SeparatePerspectiveModel>
    {
        public static final Loader INSTANCE = new Loader();

        @Deprecated(forRemoval = true, since = "1.18.2")
        public static final ImmutableBiMap<String, ItemTransforms.TransformType> PERSPECTIVES = ImmutableBiMap.<String, ItemTransforms.TransformType>builder()
                .put("none", ItemTransforms.TransformType.NONE)
                .put("third_person_left_hand", ItemTransforms.TransformType.THIRD_PERSON_LEFT_HAND)
                .put("third_person_right_hand", ItemTransforms.TransformType.THIRD_PERSON_RIGHT_HAND)
                .put("first_person_left_hand", ItemTransforms.TransformType.FIRST_PERSON_LEFT_HAND)
                .put("first_person_right_hand", ItemTransforms.TransformType.FIRST_PERSON_RIGHT_HAND)
                .put("head", ItemTransforms.TransformType.HEAD)
                .put("gui", ItemTransforms.TransformType.GUI)
                .put("ground", ItemTransforms.TransformType.GROUND)
                .put("fixed", ItemTransforms.TransformType.FIXED)
                .build();

        @Deprecated(forRemoval = true, since = "1.18.2")
        private static final ImmutableBiMap<String, ItemTransforms.TransformType> BACKWARD_COMPATIBILITY = ImmutableBiMap.<String, ItemTransforms.TransformType>builder()
                .put("third_person_left_hand", ItemTransforms.TransformType.THIRD_PERSON_LEFT_HAND)
                .put("third_person_right_hand", ItemTransforms.TransformType.THIRD_PERSON_RIGHT_HAND)
                .put("first_person_left_hand", ItemTransforms.TransformType.FIRST_PERSON_LEFT_HAND)
                .put("first_person_right_hand", ItemTransforms.TransformType.FIRST_PERSON_RIGHT_HAND)
                .build();

        @Override
        public void onResourceManagerReload(final ResourceManager pResourceManager) {

        }

        @Override
        public SeparatePerspectiveModel read(JsonDeserializationContext deserializationContext, JsonObject modelContents)
        {
            BlockModel baseModel = deserializationContext.deserialize(GsonHelper.getAsJsonObject(modelContents, "base"), BlockModel.class);

            JsonObject perspectiveData = GsonHelper.getAsJsonObject(modelContents, "perspectives");

            Map<ItemTransforms.TransformType, BlockModel> perspectives = new HashMap<>();
            for(Map.Entry<String, ItemTransforms.TransformType> entry : BACKWARD_COMPATIBILITY.entrySet())
            {
                if (perspectiveData.has(entry.getKey()))
                {
                    BlockModel perspectiveModel = deserializationContext.deserialize(GsonHelper.getAsJsonObject(perspectiveData, entry.getKey()), BlockModel.class);
                    perspectives.put(entry.getValue(), perspectiveModel);
                }
            }
            for(ItemTransforms.TransformType perspective : ItemTransforms.TransformType.values())
            {
                if (perspectiveData.has(perspective.getSerializeName()))
                {
                    BlockModel perspectiveModel = deserializationContext.deserialize(GsonHelper.getAsJsonObject(perspectiveData, perspective.getSerializeName()), BlockModel.class);
                    perspectives.put(perspective, perspectiveModel);
                }
            }

            return new FixedSeparatePerspektiveModel(baseModel, ImmutableMap.copyOf(perspectives));
        }
    }
}
