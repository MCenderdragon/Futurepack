package futurepack.client.model;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.datafixers.util.Pair;

import it.unimi.dsi.fastutil.objects.Object2IntArrayMap;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.BlockElement;
import net.minecraft.client.renderer.block.model.BlockElementFace;
import net.minecraft.client.renderer.block.model.BlockModel;
import net.minecraft.client.renderer.texture.MissingTextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.resources.model.Material;
import net.minecraft.client.resources.model.ModelBakery;
import net.minecraft.client.resources.model.ModelState;
import net.minecraft.client.resources.model.UnbakedModel;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.util.GsonHelper;
import net.minecraftforge.client.model.IModelBuilder;
import net.minecraftforge.client.model.IModelConfiguration;
import net.minecraftforge.client.model.IModelLoader;
import net.minecraftforge.client.model.ModelLoaderRegistry.VanillaProxy;
import net.minecraftforge.client.model.ModelLoaderRegistry.VanillaProxy.Loader;
import net.minecraftforge.client.model.geometry.ISimpleModelGeometry;

public class EmissiveTextureModel implements ISimpleModelGeometry<EmissiveTextureModel>
{
	private final List<BlockElement> elements;
	private final Object2IntArrayMap<String> map;

    public EmissiveTextureModel(List<BlockElement> list, Object2IntArrayMap<String> map)
    {
        this.elements = list;
        this.map = map;
    }

    @Override
    public void addQuads(IModelConfiguration owner, IModelBuilder<?> modelBuilder, ModelBakery bakery, Function<Material, TextureAtlasSprite> spriteGetter, ModelState modelTransform, ResourceLocation modelLocation)
    {
        for(BlockElement blockpart : elements) {
            for(Direction direction : blockpart.faces.keySet()) {
                BlockElementFace blockpartface = blockpart.faces.get(direction);
                TextureAtlasSprite textureatlassprite1 = spriteGetter.apply(owner.resolveTexture(blockpartface.texture));
                
                int light = map.getOrDefault(blockpartface.texture.replace("#", ""), 0);
                
                BakedQuad base = BlockModel.makeBakedQuad(blockpart, blockpartface, textureatlassprite1, direction, modelTransform, modelLocation);
                base = setLight(base, light);
                
                if (blockpartface.cullForDirection == null) {
                    modelBuilder.addGeneralQuad(base);
                } else {
                    modelBuilder.addFaceQuad(
                            modelTransform.getRotation().rotateTransform(blockpartface.cullForDirection),
                            base);
                }
            }
        }
    }

    @Override
    public Collection<Material> getTextures(IModelConfiguration owner, Function<ResourceLocation, UnbakedModel> modelGetter, Set<Pair<String, String>> missingTextureErrors)
    {
        Set<Material> textures = Sets.newHashSet();

        for(BlockElement part : elements) {
            for(BlockElementFace face : part.faces.values()) {
                Material texture = owner.resolveTexture(face.texture);
                if (Objects.equals(texture, MissingTextureAtlasSprite.getLocation().toString())) {
                    missingTextureErrors.add(Pair.of(face.texture, owner.getModelName()));
                }

                textures.add(texture);
            }
        }

        return textures;
    }
    
    public static BakedQuad setLight(BakedQuad quad, int light)
    {
    	int[] vertexData = quad.getVertices();
    	int stride = DefaultVertexFormat.BLOCK.getIntegerSize();
    	int uv2 = DefaultVertexFormat.BLOCK.getOffset(DefaultVertexFormat.BLOCK.getElements().indexOf(DefaultVertexFormat.ELEMENT_UV2)) / 4;
    	for (int vert = 0; vert < 4; vert++) {
    	    int offset = vert * stride + uv2;
    	    vertexData[offset] = LightTexture.pack(light, light);
    	}
    	
    	return new BakedQuad(vertexData, quad.getTintIndex(), quad.getDirection(), quad.getSprite(), quad.isShade());
    }

    public static class Loader implements IModelLoader<EmissiveTextureModel>
    {
        public static final Loader INSTANCE = new Loader();

        private Loader()
        {
        }

        @Override
        public void onResourceManagerReload(ResourceManager resourceManager)
        {
        	
        }

        @Override
        public EmissiveTextureModel read(JsonDeserializationContext deserializationContext, JsonObject modelContents)
        {
            List<BlockElement> list = this.getModelElements(deserializationContext, modelContents);
            Object2IntArrayMap<String> map;
            JsonObject texture2light = GsonHelper.getAsJsonObject(modelContents, "emissive");
            map = new Object2IntArrayMap<>(texture2light.size());
            for(var e : texture2light.entrySet())
            {
            	map.put(e.getKey(), e.getValue().getAsInt());
            }
            return new EmissiveTextureModel(list, map);
        }

        private List<BlockElement> getModelElements(JsonDeserializationContext deserializationContext, JsonObject object) {
            List<BlockElement> list = Lists.newArrayList();
            if (object.has("elements")) {
                for(JsonElement jsonelement : GsonHelper.getAsJsonArray(object, "elements")) {
                    list.add(deserializationContext.deserialize(jsonelement, BlockElement.class));
                }
            }

            return list;
        }
    }
}
