package futurepack.client.sos;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.depend.api.helper.HelperRendering;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.resources.ResourceLocation;

public class AnimationStartOS extends AnimationBase
{
	private ResourceLocation scrench = new ResourceLocation(Constants.MOD_ID, "textures/os/s_os.png");
	
	private long ticks = -1;
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY)
	{
		GuiComponent.fill(matrixStack, xPos, yPos, xPos+width, yPos+height, 0xff000000);
		
		if(ticks==-1)
		{
			ticks = System.currentTimeMillis();
		}
		RenderSystem.setShaderTexture(0, scrench);
		
		float a = (float) Math.sin(Math.PI * (System.currentTimeMillis()-ticks) / 2500.0 );
//		a+=1;
//		a *= 0.5;
		HelperRendering.glColor4f(a, a, a, 1.0f);
		int x = (int) (xPos + (width -width*0.75)/2 );
		int y = (int) (yPos + (height -height*0.75*3/4)/2 );
		GuiComponent.blit(matrixStack, x, y,(int)(width * 0.75),  (int)(height*0.75 * 3/4),  0,0, 1,1, 1, 1);
//		Gui.blit(matrixStack, x, y, 64, 48, width/2, height/2, 64, 48);	
	}

	@Override
	public boolean isFinished()
	{
		return ticks!=-1 && System.currentTimeMillis()-ticks >= 2500;
	}

}
