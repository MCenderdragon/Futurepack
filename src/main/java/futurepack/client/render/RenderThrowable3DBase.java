package futurepack.client.render;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Matrix3f;
import com.mojang.math.Matrix4f;
import com.mojang.math.Vector3f;
import com.mojang.math.Vector4f;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.world.entity.projectile.ThrowableProjectile;

public abstract class RenderThrowable3DBase<T extends ThrowableProjectile> extends EntityRenderer<T> 
{

	protected RenderThrowable3DBase(EntityRendererProvider.Context renderManager) 
	{
		super(renderManager);
	}

	/**
	 * u0,v0 the start of the texture
	 * u1,v1 the end of the texture
	 * This area is used for the sides
	 * 
	 * @param entity
	 * @return a vector containing u0, v0, u1, v1
	 */
	public abstract Vector4f getUVForSides(T entity);
	
	/**
	 * u0,v0 the start of the texture
	 * u1,v1 the end of the texture
	 * 
	 * This area is used for the front
	 * 
	 * @param entity
	 * @return a vector containing u0, v0, u1, v1
	 */
	public abstract Vector4f getUVForFront(T entity);
	
	public abstract float getWidth(T entity);
	public abstract float getDepth(T entity);
	
	@Override
	public void render(T entityIn, float entityYaw, float partialTicks, PoseStack matrixStack, MultiBufferSource bufferIn, int packedLightIn)
	{
	    matrixStack.pushPose();
	    matrixStack.mulPose(Vector3f.YP.rotationDegrees(entityYaw));
	    matrixStack.mulPose(Vector3f.XN.rotationDegrees(entityIn.getYRot()));
	    
	    float u1,u2,v1,v2;
        
//        u1=3F/32F;
//        v1=0F;
//        u2=13F/32F;
//        v2=1F;
	    Vector4f uvs = getUVForSides(entityIn);
	    u1 = uvs.x();
	    v1 = uvs.y();
	    u2 = uvs.z();
	    v2 = uvs.w();
        float fw = getWidth(entityIn);
        
        VertexConsumer buffer = bufferIn.getBuffer(RenderType.entityCutout(this.getTextureLocation(entityIn)));
        
        PoseStack.Pose current = matrixStack.last();
        Matrix4f matrix = current.pose();
        Matrix3f normals = current.normal(); 
        
        quad(matrix, normals, buffer, -fw, 0, 0, u1, v1, 0, 1, 0 ,packedLightIn);//TODO: if grenades render weird, we need to flip the normals
        quad(matrix, normals, buffer, fw, 0, 0, u2, v1, 0, 1, 0 ,packedLightIn);
        quad(matrix, normals, buffer, fw, 0, -1, u2, v2, 0, 1, 0 ,packedLightIn);
        quad(matrix, normals, buffer, -fw, 0, -1, u1, v2, 0, 1, 0 ,packedLightIn);
        
        quad(matrix, normals, buffer, fw, 0, 0, u1, v1, 0, -1, 0 ,packedLightIn);
        quad(matrix, normals, buffer, -fw, 0, 0, u2, v1, 0, -1, 0 ,packedLightIn);
        quad(matrix, normals, buffer, -fw, 0, -1, u2, v2, 0, -1, 0 ,packedLightIn);
        quad(matrix, normals, buffer, fw, 0, -1, u1, v2, 0, -1, 0 ,packedLightIn);
        
        quad(matrix, normals, buffer, 0, -fw, 0, u1, v1, 1, 0, 0 ,packedLightIn);
        quad(matrix, normals, buffer, 0, fw, 0, u2, v1, 1, 0, 0 ,packedLightIn);
        quad(matrix, normals, buffer, 0, fw, -1, u2, v2, 1, 0, 0 ,packedLightIn);
        quad(matrix, normals, buffer, 0, -fw, -1, u1, v2, 1, 0, 0 ,packedLightIn);
        
        quad(matrix, normals, buffer, 0, fw, 0, u1, v1, -1, 0, 0 ,packedLightIn);
        quad(matrix, normals, buffer, 0, -fw, 0, u2, v1, -1, 0, 0 ,packedLightIn);
        quad(matrix, normals, buffer, 0, -fw, -1, u2, v2, -1, 0, 0 ,packedLightIn);
        quad(matrix, normals, buffer, 0, fw, -1, u1, v2, -1, 0, 0 ,packedLightIn);
        
//        u1=0.0625F * 1.5F +0.5F; 
//        u2=0.0625F * 6.5F +0.5F;
//        v1=0.0625F * 1.5F ;
//        v2=0.0625F * 6.5F ;
        uvs = getUVForFront(entityIn);
        u1 = uvs.x();
	    v1 = uvs.y();
	    u2 = uvs.z();
	    v2 = uvs.w();
        
        float fz = getDepth(entityIn);
        
        quad(matrix, normals, buffer, -fw, fw, -fz, u1, v1, 0, 0, 1 ,packedLightIn);
        quad(matrix, normals, buffer, fw, fw, -fz, u2, v1, 0, 0, 1 ,packedLightIn);
        quad(matrix, normals, buffer, fw, -fw, -fz, u2, v2, 0, 0, 1 ,packedLightIn);
        quad(matrix, normals, buffer, -fw, -fw, -fz, u1, v2, 0, 0, 1 ,packedLightIn);
        
        quad(matrix, normals, buffer, fw, fw, -fz, u1, v1, 0, 0, -1 ,packedLightIn);
        quad(matrix, normals, buffer, -fw, fw, -fz, u2, v1, 0, 0, -1 ,packedLightIn);
        quad(matrix, normals, buffer, -fw, -fw, -fz, u2, v2, 0, 0, -1 ,packedLightIn);
        quad(matrix, normals, buffer, fw, -fw, -fz, u1, v2, 0, 0, -1 ,packedLightIn);
        
        
        matrixStack.popPose();
	    
		super.render(entityIn, entityYaw, partialTicks, matrixStack, bufferIn, packedLightIn);
	}
	
	public void quad(Matrix4f matrix, Matrix3f normals, VertexConsumer buffer, float x, float y, float z, float u, float v, int normalX, int normalY, int normalZ, int packetLight) 
	{
	    buffer.vertex(matrix, x, y, z).color(255, 255, 255, 255).uv(u, v).overlayCoords(OverlayTexture.NO_OVERLAY).uv2(packetLight).normal(normals, (float)normalX, (float)normalZ, (float)normalY).endVertex();
	}
}
