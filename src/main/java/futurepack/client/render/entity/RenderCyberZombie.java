package futurepack.client.render.entity;

import futurepack.api.Constants;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.ZombieRenderer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.monster.Zombie;

public class RenderCyberZombie extends ZombieRenderer
{
	private static final ResourceLocation ZOMBIE_TEXTURES = new ResourceLocation(Constants.MOD_ID, "textures/entity/cyberzombie.png");
	
	public RenderCyberZombie(EntityRendererProvider.Context renderManagerIn)
	{
		super(renderManagerIn);
	}

	@Override
	public ResourceLocation getTextureLocation(Zombie entity)
    {
        return ZOMBIE_TEXTURES;
    }
}
