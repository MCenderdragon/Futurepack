package futurepack.client.render.entity;

import com.google.common.collect.ImmutableList;

import futurepack.api.Constants;
import futurepack.common.entity.EntityMiner;
import net.minecraft.client.model.ListModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.resources.ResourceLocation;

public class ModelMiner extends ListModel<EntityMiner>
{
	// This layer location should be baked with EntityRendererProvider.Context in
	// the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(Constants.MOD_ID, "miner"), "main");
	private final ModelPart miner;
	private final ModelPart thruster_right;
	private final ModelPart thruster_left;
	
	public ModelMiner(ModelPart root) 
	{
		this.miner = root.getChild("miner");
		this.thruster_left = this.miner.getChild("thruster_left");
		this.thruster_right = this.miner.getChild("thruster_right");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition miner = partdefinition.addOrReplaceChild("miner", CubeListBuilder.create().texOffs(0, 5).addBox(-2.0F, -3.0F, -2.0F, 4.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 1.0F, 0.0F));

		PartDefinition head = miner.addOrReplaceChild("head", CubeListBuilder.create().texOffs(16, 5).addBox(-1.0F, -1.0F, -2.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -1.0F, -2.0F));

		PartDefinition laser_right = miner.addOrReplaceChild("laser_right", CubeListBuilder.create().texOffs(8, 13).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-2.0F, 1.0F, -2.0F));

		PartDefinition laser_left = miner.addOrReplaceChild("laser_left", CubeListBuilder.create().texOffs(8, 13).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(2.0F, 1.0F, -2.0F));

		PartDefinition thruster_right = miner.addOrReplaceChild("thruster_right", CubeListBuilder.create().texOffs(0, 0).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(-3.0F, -2.0F, 1.0F));

		PartDefinition thruster_left = miner.addOrReplaceChild("thruster_left", CubeListBuilder.create().texOffs(0, 0).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(3.0F, -2.0F, 1.0F));

		return LayerDefinition.create(meshdefinition, 64, 64);
	}

	@Override
	public Iterable<ModelPart> parts()
	{
		return ImmutableList.of(this.miner);
	}

	@Override
	public void setupAnim(EntityMiner entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		thruster_left.xRot = entityIn.rot;
		thruster_right.xRot = entityIn.rot;
		
	}

}
