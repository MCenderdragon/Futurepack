package futurepack.client.render;

import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.math.Matrix4f;

import futurepack.api.Constants;
import net.minecraft.resources.ResourceLocation;

public class RenderSkyEnvia extends RenderSkyBase
{
	protected ResourceLocation locationSunPng = new ResourceLocation(Constants.MOD_ID, "textures/sun_envia.png");
	
	@Override
	protected void renderMoon(float size, int phase, BufferBuilder bufferbuilder, Matrix4f matrix4f1)
	{
	}
	
	@Override
	public float getSunSize()
	{
		return 3F;
	}
}
