package futurepack.client.render.block;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Matrix4f;
import com.mojang.math.Vector3f;

import futurepack.api.laser.LightProperties;
import futurepack.common.block.logistic.light.BlockPrisma;
import futurepack.common.block.logistic.light.TileEntityPrisma;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;

public class RenderPrismaBlock  implements BlockEntityRenderer<TileEntityPrisma>
{
	public RenderPrismaBlock(BlockEntityRendererProvider.Context rendererDispatcherIn) 
	{
//		super(rendererDispatcherIn);
	}

	@Override
	public void render(TileEntityPrisma pBlockEntity, float pPartialTick, PoseStack pPoseStack, MultiBufferSource pBufferSource, int pPackedLight, int pPackedOverlay) 
	{
		pPoseStack.pushPose();
		pPoseStack.translate(0.5, 0.5, 0.5);
		
		Direction dir = pBlockEntity.getBlockState().getValue(BlockPrisma.FACING);
		
		LightProperties light = pBlockEntity.getBeam();
//		if(light.isEmpty())
//		{
//			light = new LightProperties(Color.HSBtoRGB( (System.currentTimeMillis()%3000)/3000.0F  , 0.9F, 0.8F), 0, 1000);
//			pBlockEntity.rayLength = 16;
//		}
			
		
		if(light.intensity() > 1)
		{

			double intensitiy = light.intensity()/ 1361.0;
			
			float width =  Math.min(Math.max(0.0625F, (float)Math.log10(intensitiy) * 0.125F), 1F);
			float rotX = (float) (dir.getRotation().toXYZ().x() - Math.PI/2F);
			float rotY = (float) Math.toRadians(dir.toYRot());
			
			
			pPoseStack.pushPose();
			float xr = Minecraft.getInstance().getCameraEntity().getViewXRot(pPartialTick);
			float yr = Minecraft.getInstance().getCameraEntity().getViewYRot(pPartialTick);
			
			pPoseStack.mulPose(Vector3f.ZP.rotationDegrees(90));
			pPoseStack.mulPose(Vector3f.XP.rotationDegrees(-yr - 90));
			pPoseStack.mulPose(Vector3f.ZP.rotationDegrees(-xr));
			
			//pPoseStack.mulPose(Vector3f.ZP.rotationDegrees(90));
			
			renderLaserStart(-0.25F * width/0.125F, 0.75F * width/0.125F, rotX, rotY, light.getRenderColorRGBA(), new ResourceLocation("futurepack", "textures/model/laser_start.png"), pPoseStack, pBufferSource, pPackedOverlay, width);
			pPoseStack.popPose();
			
			RenderLaserBase.renderBeam(0.0F, (float) pBlockEntity.rayLength, rotX, rotY, light.getRenderColorRGBA(), new ResourceLocation("futurepack", intensitiy >= 5  ? "textures/model/laser_thick.png" : "textures/model/laser_healer.png"), pPoseStack, pBufferSource, pPackedOverlay, width);
			
		}		
		
		pPoseStack.popPose();
	}
	
	public static void renderLaserStart(float from, float dis, float rotX, float rotY, int col3i, ResourceLocation laser, PoseStack matrixStack, MultiBufferSource bufferIn, int overlay, float width)
	{
		matrixStack.pushPose();
//		RenderSystem.setShaderTexture(0, laser);
//		
//		RenderSystem.enableBlend();
//		GlStateManager._blendFuncSeparate(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA, GL11.GL_ONE, GL11.GL_ZERO);
//		RenderSystem.enableDepthTest();
//        GL11.glDepthMask(true);
//		GL11.glDisable(GL11.GL_LIGHTING);
//		GL11.glEnable(GL11.GL_ALPHA_TEST);
		
		
		matrixStack.mulPose(Vector3f.YP.rotation(-rotY));
		matrixStack.mulPose(Vector3f.XP.rotation(rotX));
		
		float r = ((col3i>>16) & 0xFF) /255F;
		float g = ((col3i>>8) & 0xFF) /255F;
		float b = ((col3i>>0) & 0xFF) /255F;
		
		float middle = (dis - from) / 2F;

		width *= 2;
		float texStart = (float) (dis / (2*width) - (System.currentTimeMillis()%1000)*0.005);
		float texEnd = (float) (- (System.currentTimeMillis()%1000)*0.005);
		
		VertexConsumer consumer = bufferIn.getBuffer(RenderType.entityTranslucentCull(laser));

		Matrix4f mat = matrixStack.last().pose();
		texStart = 0;
		texEnd = 1;
		
		dis -= middle;
		float width2 = width ;
		
		RenderLaserBase.addVertexWithUV(-width, 0F, from, 0F, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(+width, 0F, from, 1F, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(+width2, 0F, dis, 1F, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(-width2, 0F, dis, 0F, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
		
		RenderLaserBase.addVertexWithUV(+width, 0, from, 0, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(-width, 0, from, 1, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(-width2, 0, dis, 1, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(+width2, 0, dis, 0, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
//
//		RenderLaserBase.addVertexWithUV(0, +width, from, 0, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
//		RenderLaserBase.addVertexWithUV(0, -width, from, 1, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
//		RenderLaserBase.addVertexWithUV(0, -width2, dis, 1, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
//		RenderLaserBase.addVertexWithUV(0, +width2, dis, 0, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
//
//		RenderLaserBase.addVertexWithUV(0, -width, from, 0, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
//		RenderLaserBase.addVertexWithUV(0, +width, from, 1, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
//		RenderLaserBase.addVertexWithUV(0, +width2, dis, 1, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
//		RenderLaserBase.addVertexWithUV(0, -width2, dis, 0, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
//		
//		from+=middle;
//		dis += middle; 
//		float f = width;
//		width = width2;
//		width2 = f;
		/**
		RenderLaserBase.addVertexWithUV(-width, 0F, from, 0F, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(+width, 0F, from, 1F, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(+width2, 0F, dis, 1F, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(-width2, 0F, dis, 0F, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
		
		RenderLaserBase.addVertexWithUV(+width, 0, from, 0, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(-width, 0, from, 1, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(-width2, 0, dis, 1, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(+width2, 0, dis, 0, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);

		RenderLaserBase.addVertexWithUV(0, +width, from, 0, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(0, -width, from, 1, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(0, -width2, dis, 1, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(0, +width2, dis, 0, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);

		RenderLaserBase.addVertexWithUV(0, -width, from, 0, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(0, +width, from, 1, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(0, +width2, dis, 1, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
		RenderLaserBase.addVertexWithUV(0, -width2, dis, 0, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
		*/
//		matrixStack.mulPose(Vector3f.XP.rotationDegrees((float) -Math.toDegrees(rotX)));
//		matrixStack.mulPose(Vector3f.YP.rotationDegrees((float) Math.toDegrees(rotY)));
//		matrixStack.translate(0, -0.625, 0);
//		GL11.glDepthMask(true);
//		RenderSystem.disableBlend();
//		GL11.glEnable(GL11.GL_LIGHTING);
		
//		RenderSystem.setShaderColor(1, 1, 1, 1);//Tessellator.getInstance().getVertexBuffer().addVertexWithUV(p_178985_1_, p_178985_3_, p_178985_5_, p_178985_7_, p_178985_9_);
		matrixStack.popPose();
	}

}
