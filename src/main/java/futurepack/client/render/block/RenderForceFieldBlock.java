package futurepack.client.render.block;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.FacingUtil;
import futurepack.client.render.entity.RenderForceField;
import futurepack.common.block.misc.TileEntityForceField;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;

public class RenderForceFieldBlock extends RenderFastHologram<TileEntityForceField>
{
	
	public RenderForceFieldBlock(BlockEntityRendererProvider.Context rendererDispatcherIn) 
	{
		super(rendererDispatcherIn);
	}
	
	@Override
	public void renderDefault(TileEntityForceField te, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		Level w = te.getLevel();
		BlockPos pos = te.getBlockPos();
		BlockState state = w.getBlockState(pos);
		
		boolean[] visible;
		visible = new boolean[FacingUtil.VALUES.length];
		
		for(Direction face : FacingUtil.VALUES)
		{
			visible[face.ordinal()] = !state.skipRendering(w.getBlockState(pos.relative(face)), face);//  !w.isSideSolid(pos.offset(face), face.getOpposite(), false);	
		}
		//sides = new boolean[]{true,true,true, true, true, true};
		
		float minU= -0.5F;
		float minV= -0.5F;
		float maxU= 0.5F;
		float maxV= 0.5F;
	        
		float offset = (System.currentTimeMillis() % 2300) / 2300F;
		
		minU +=offset;
		minV +=offset;
		maxU +=offset;
		maxV +=offset;
	        
		AABB bb = new AABB(0, 0, 0, 1, 1, 1);
		
		RenderForceField.renderSides(bufferIn, matrixStackIn, bb, minU, minV, maxU, maxV, visible, 0, 153, 255, combinedLightIn, combinedOverlayIn);
	}
}
