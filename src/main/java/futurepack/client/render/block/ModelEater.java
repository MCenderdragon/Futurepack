package futurepack.client.render.block;

import futurepack.api.Constants;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;

public class ModelEater extends ModelLaserBase
{
	
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(Constants.MOD_ID, "entity_eater_laser"), "main");
	private final ModelPart base;
	ModelPart RotationsfussSockel;
	ModelPart Laserkorper;
	
	public ModelEater(ModelPart root) 
	{
		super(RenderType::entitySolid);
		this.base = root.getChild("base");
		RotationsfussSockel = base.getChild("rotating_base");
		Laserkorper = RotationsfussSockel.getChild("laser");
	}

	public static LayerDefinition createBodyLayer() 
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition base = partdefinition.addOrReplaceChild("base", CubeListBuilder.create().texOffs(0, 42).addBox(-8.0F, -6.0F, -8.0F, 16.0F, 6.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 6.0F, 0.0F));

		PartDefinition rotating_base = base.addOrReplaceChild("rotating_base", CubeListBuilder.create().texOffs(0, 28).addBox(-6.0F, -1.0F, -6.0F, 12.0F, 2.0F, 12.0F, new CubeDeformation(0.0F))
				.texOffs(48, 32).addBox(-4.0F, -7.0F, -2.0F, 1.0F, 6.0F, 4.0F, new CubeDeformation(0.0F))
				.texOffs(48, 32).addBox(3.0F, -7.0F, -2.0F, 1.0F, 6.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -7.0F, 0.0F));

		PartDefinition laser = rotating_base.addOrReplaceChild("laser", CubeListBuilder.create().texOffs(34, 11).addBox(-5.0F, -2.0F, -2.0F, 3.0F, 4.0F, 4.0F, new CubeDeformation(0.0F))
				.texOffs(34, 11).addBox(2.0F, -2.0F, -2.0F, 3.0F, 4.0F, 4.0F, new CubeDeformation(0.0F))
				.texOffs(0, 14).addBox(-2.0F, -3.0F, -4.0F, 4.0F, 6.0F, 8.0F, new CubeDeformation(0.0F))
				.texOffs(30, 19).addBox(-1.5F, -2.5F, -10.0F, 3.0F, 3.0F, 6.0F, new CubeDeformation(0.0F))
				.texOffs(48, 28).addBox(-2.5F, 0.0F, -11.0F, 1.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
				.texOffs(48, 28).addBox(1.5F, 0.0F, -11.0F, 1.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
				.texOffs(48, 21).addBox(-2.5F, -3.0F, -12.0F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
				.texOffs(48, 21).addBox(1.5F, -3.0F, -12.0F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
				.texOffs(48, 16).addBox(-1.5F, -3.0F, -12.0F, 3.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
				.texOffs(0, 2).addBox(-1.0F, -4.0F, -1.0F, 2.0F, 6.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -9.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 64, 64);
	}

//    public ModelEater()
//    {
//    	super(RenderType::entitySolid);
//    	
//    	texWidth = 64;
//    	texHeight = 64;
//    
//    	Sockel = new ModelPart(this, 0, 42);
//        Sockel.addBox(-8F, 0F, -8F, 16, 6, 16);
//        Sockel.setPos(0F, 0F, 0F);
//        Sockel.setTexSize(64, 64);
//        Sockel.mirror = true;
//        setRotation(Sockel, 0F, 0F, 0F);
//        RotationsfussSockel = new ModelPart(this, 0, 28);
//        RotationsfussSockel.addBox(-6F, 0F, -6F, 12, 2, 12);
//        RotationsfussSockel.setPos(0F, -2F, 0F);
//        RotationsfussSockel.setTexSize(64, 64);
//        RotationsfussSockel.mirror = true;
//        setRotation(RotationsfussSockel, 0F, 0F, 0F);
//        Stutze1 = new ModelPart(this, 48, 32);
//        Stutze1.addBox(3F, -6F, -2F, 1, 6, 4);
//        Stutze1.setPos(0F, -2F, 0F);
//        Stutze1.setTexSize(64, 64);
//        Stutze1.mirror = true;
//        setRotation(Stutze1, 0F, 0F, 0F);
//        Stutze2 = new ModelPart(this, 48, 32);
//        Stutze2.addBox(-4F, -6F, -2F, 1, 6, 4);
//        Stutze2.setPos(0F, -2F, 0F);
//        Stutze2.setTexSize(64, 64);
//        Stutze2.mirror = true;
//        setRotation(Stutze2, 0F, 0F, 0F);
//        RotationsAchshalter1 = new ModelPart(this, 34, 11);
//        RotationsAchshalter1.addBox(2F, -2F, -2F, 3, 4, 4);
//        RotationsAchshalter1.setPos(0F, -10F, 0F);
//        RotationsAchshalter1.setTexSize(64, 64);
//        RotationsAchshalter1.mirror = true;
//        setRotation(RotationsAchshalter1, 0F, 0F, 0F);
//        RotationsAchshalter2 = new ModelPart(this, 34, 11);
//        RotationsAchshalter2.addBox(-5F, -2F, -2F, 3, 4, 4);
//        RotationsAchshalter2.setPos(0F, -10F, 0F);
//        RotationsAchshalter2.setTexSize(64, 64);
//        RotationsAchshalter2.mirror = true;
//        setRotation(RotationsAchshalter2, 0F, 0F, 0F);
//        Laserkorper = new ModelPart(this, 0, 14);
//        Laserkorper.addBox(-2F, -3F, -4F, 4, 6, 8);
//        Laserkorper.setPos(0F, -10F, 0F);
//        Laserkorper.setTexSize(64, 64);
//        Laserkorper.mirror = true;
//        setRotation(Laserkorper, 0F, 0F, 0F);
//        Laserschaft = new ModelPart(this, 30, 19);
//        Laserschaft.addBox(-1.5F, -2.5F, -10F, 3, 3, 6);
//        Laserschaft.setPos(0F, -10F, 0F);
//        Laserschaft.setTexSize(64, 64);
//        Laserschaft.mirror = true;
//        setRotation(Laserschaft, 0F, 0F, 0F);
//        Laserfront1 = new ModelPart(this, 48, 28);
//        Laserfront1.addBox(1.5F, 0F, -11F, 1, 1, 3);
//        Laserfront1.setPos(0F, -10F, 0F);
//        Laserfront1.setTexSize(64, 64);
//        Laserfront1.mirror = true;
//        setRotation(Laserfront1, 0F, 0F, 0F);
//        Laserfront2 = new ModelPart(this, 48, 28);
//        Laserfront2.addBox(-2.5F, 0F, -11F, 1, 1, 3);
//        Laserfront2.setPos(0F, -10F, 0F);
//        Laserfront2.setTexSize(64, 64);
//        Laserfront2.mirror = true;
//        setRotation(Laserfront2, 0F, 0F, 0F);
//        Lasermitte1 = new ModelPart(this, 48, 21);
//        Lasermitte1.addBox(1.5F, -3F, -12F, 1, 3, 4);
//        Lasermitte1.setPos(0F, -10F, 0F);
//        Lasermitte1.setTexSize(64, 64);
//        Lasermitte1.mirror = true;
//        setRotation(Lasermitte1, 0F, 0F, 0F);
//        Lasermitte2 = new ModelPart(this, 48, 21);
//        Lasermitte2.addBox(-2.5F, -3F, -12F, 1, 3, 4);
//        Lasermitte2.setPos(0F, -10F, 0F);
//        Lasermitte2.setTexSize(64, 64);
//        Lasermitte2.mirror = true;
//        setRotation(Lasermitte2, 0F, 0F, 0F);
//        Lasertop = new ModelPart(this, 48, 16);
//        Lasertop.addBox(-1.5F, -3F, -12F, 3, 1, 4);
//        Lasertop.setPos(0F, -10F, 0F);
//        Lasertop.setTexSize(64, 64);
//        Lasertop.mirror = true;
//        setRotation(Lasertop, 0F, 0F, 0F);
//        LaserkorperHinten = new ModelPart(this, 0, 2);
//        LaserkorperHinten.addBox(-1F, -4F, -1F, 2, 6, 6);
//        LaserkorperHinten.setPos(0F, -10F, 0F);
//        LaserkorperHinten.setTexSize(64, 64);
//        LaserkorperHinten.mirror = true;
//        setRotation(LaserkorperHinten, 0F, 0F, 0F);
//    }	
    
    @Override
    public void rotateYUnit(float y)
    {
    	RotationsfussSockel.yRot = y;

    }
    
    @Override
    public void rotateXUnit(float x)
    {
    	Laserkorper.xRot = x;
    }

	@Override
	public ModelPart[] getParts() 
	{
		return new ModelPart[] {base};
	}
	
}
