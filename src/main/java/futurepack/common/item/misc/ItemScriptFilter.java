package futurepack.common.item.misc;

import java.util.List;

import futurepack.common.sync.FPGuiHandler;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class ItemScriptFilter extends Item
{
	public ItemScriptFilter(Item.Properties props) 
	{
		super(props);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) 
	{
		CompoundTag nbt = stack.getTagElement("script");
		if(nbt != null)
		{
			String s = nbt.getString("script_name");
			tooltip.add(new TextComponent(s));
		}
		tooltip.add(new TextComponent("Use/RightClick to edit code"));
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		
	}
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level worldIn, Player pl, InteractionHand hand) 
	{
		ItemStack it = pl.getItemInHand(hand);
		if(!it.isEmpty())
		{
			if(!worldIn.isClientSide)
			{
				FPGuiHandler.JS_FILTER_TEST.openGui((ServerPlayer)pl, (Object[])null);
			}
			return InteractionResultHolder.success(it);
		}
		
		return super.use(worldIn, pl, hand);
	}
}
