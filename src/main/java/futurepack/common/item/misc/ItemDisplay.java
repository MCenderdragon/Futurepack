package futurepack.common.item.misc;

import futurepack.common.entity.EntityMonitor;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.decoration.HangingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;

public class ItemDisplay extends Item
{

	public ItemDisplay(Properties properties) 
	{
		super(properties);
	}
	
	@Override
	public InteractionResult useOn(UseOnContext context)
	{
//		if(true)
//		{
//			if(!context.getWorld().isRemote)
//			{
//				NBTTagCompound nbt = context.getItem().getOrCreateChildTag("pos");
//				
//				if(nbt.contains("xyz"))
//				{
//					StructureToJSON gen = new StructureToJSON(context.getWorld());
//					int[] xyz = nbt.getIntArray("xyz");
//					gen.generate(new BlockPos(xyz[0], xyz[1], xyz[2]), context.getPos());
//					context.getPlayer().sendMessage(new TextComponentString(gen.fileName));
//					nbt.remove("xyz");
//				}
//				else
//				{
//					nbt.putIntArray("xyz", new int[]{context.getPos().getX(), context.getPos().getY(), context.getPos().getZ()});
//				}
//			}
//			return EnumActionResult.SUCCESS;
//		}
		
		if (context.getClickedFace() == Direction.DOWN)
        {
            return InteractionResult.PASS;
        }
        else if (context.getClickedFace() == Direction.UP)
        {
        	return InteractionResult.PASS;
        }
        else
        {
            BlockPos blockpos1 = context.getClickedPos().relative(context.getClickedFace());
            Player pl = context.getPlayer();
            Level w = context.getLevel();
            ItemStack it = context.getItemInHand();
            
            if (!pl.mayUseItemAt(blockpos1, context.getClickedFace(), it))
            {
            	return InteractionResult.PASS;
            }
            else
            {
                HangingEntity entityhanging = new EntityMonitor(w, blockpos1, context.getClickedFace());

                if (entityhanging != null && entityhanging.survives())
                {
                    if (!w.isClientSide)
                    {
                    	entityhanging.playPlacementSound();
                        w.addFreshEntity(entityhanging);
                    }

                    it.shrink(1);
                }
                return InteractionResult.SUCCESS;
            }
        }
	}
	
}
