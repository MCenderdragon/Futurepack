package futurepack.common.item.misc;

import java.util.ArrayList;
import java.util.List;

import futurepack.api.ItemPredicateBase;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchManager;
import net.minecraft.ChatFormatting;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class ItemResearchBlueprint extends Item
{
	public ItemResearchBlueprint(Item.Properties props)
	{
		super(props);
	}
	
	public static ItemStack getItemForResearch(Research r)
	{
		ItemStack it = new ItemStack(MiscItems.RESEARCH_BLUEPRINT, 1);
		it.setTag(new CompoundTag());
		CompoundTag nbt = it.getTag();
		nbt.putString("research", r.getName());	
		return it;
	}
	
	public static Research getResearchFromItem(ItemStack it)
	{
		CompoundTag nbt = it.getTag();
		if(nbt!=null && nbt.contains("research"))
		{
			return ResearchManager.getResearch(nbt.getString("research"));
		}
		return null;
	}
	
	@Override
	public void fillItemCategory(CreativeModeTab tab, NonNullList<ItemStack> subItems)
	{
//		FIXME: blueprints in creative tab
//		if(this.isInGroup(tab) && FPConfig.CLIENT.enableBlueprintsInCreative.get())
//		{
//			for(int i=0;i<ResearchManager.getResearchCount();i++)
//			{
//				Research r = ResearchManager.getById(i);
//				subItems.add(getItemForResearch(r));
//			}
//		}
	}
	
	
	
	@Override
	public void appendHoverText(ItemStack stack, Level w, List<Component> tooltip, TooltipFlag advanced)
	{
		Research r = getResearchFromItem(stack);
		
		if(r!=null)
		{
			tooltip.add(r.getLocalizedName().plainCopy().setStyle(Style.EMPTY.withColor(ChatFormatting.WHITE)));
			tooltip.addAll(getResources(r));
			tooltip.addAll(getPowers(r));
		}
		super.appendHoverText(stack, w, tooltip, advanced);
	}
	
	private static List<Component> getResources(Research r)
	{
		List<Component> s = new ArrayList<Component>();
		if(r.getNeeded()!=null)
		{
			s.add(new TranslatableComponent("item.futurepack.research_blueprint.needed").setStyle(Style.EMPTY.withColor(ChatFormatting.GRAY)));
			for(ItemPredicateBase ip : r.getNeeded())
			{
				if(ip!=null)
				{
					ItemStack it = ip.getRepresentItem();
					if(it!=null)
					{
						s.add(it.getHoverName().plainCopy().setStyle(Style.EMPTY.withColor(ChatFormatting.GRAY)));
					}
				}		
			}
		}		
		return s;
	}
	
	private static List<Component> getPowers(Research r)
	{
		List<Component> s = new ArrayList<Component>();
		s.add(new TextComponent(new TranslatableComponent("item.futurepack.research_blueprint.time").getString() + " " + r.getTime()).setStyle(Style.EMPTY.withColor(ChatFormatting.WHITE)));
		s.add(new TextComponent(new TranslatableComponent("item.futurepack.research_blueprint.ne").getString() + " " + r.getNeonenergie()).setStyle(Style.EMPTY.withColor(ChatFormatting.AQUA)));
		s.add(new TextComponent(new TranslatableComponent("item.futurepack.research_blueprint.support").getString() + " " + r.getSupport()).setStyle(Style.EMPTY.withColor(ChatFormatting.YELLOW)));
		s.add(new TextComponent(new TranslatableComponent("item.futurepack.research_blueprint.xp").getString() + " " + r.getExpLvl()).setStyle(Style.EMPTY.withColor(ChatFormatting.GREEN)));
		return s;
	}
	
	@Override
	public Rarity getRarity(ItemStack stack)
	{
		Research r = getResearchFromItem(stack);
		if(r!=null)
		{
			switch(r.getTecLevel())
			{
			case 0: return Rarity.COMMON;
			case 1: return Rarity.UNCOMMON;
			case 2: return Rarity.RARE;
			case 3:
			default:return Rarity.EPIC;
			}
		}
		return super.getRarity(stack);
	}
}
