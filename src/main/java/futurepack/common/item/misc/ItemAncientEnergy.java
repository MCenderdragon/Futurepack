package futurepack.common.item.misc;

import java.util.List;
import java.util.WeakHashMap;

import javax.annotation.Nullable;

import com.mojang.math.Vector3f;

import futurepack.api.Constants;
import futurepack.api.FacingUtil;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.interfaces.IItemNeon;
import net.minecraft.advancements.Advancement;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.DustParticleOptions;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.common.util.LazyOptional;

public class ItemAncientEnergy extends Item implements IItemNeon
{
	public ItemAncientEnergy(Properties props) 
	{
		super(props);
	}

	@Override
	public void inventoryTick(ItemStack stack, Level worldIn, Entity entityIn, int itemSlot, boolean isSelected) 
	{
		if(isSelected)
		{
			if(absorbEnergy(stack, worldIn, entityIn.blockPosition(), 5))
			{
				if(!worldIn.isClientSide && entityIn instanceof ServerPlayer)
				{
					Advancement adv = worldIn.getServer().getAdvancements().getAdvancement(new ResourceLocation(Constants.MOD_ID, "energy_sponge"));
					((ServerPlayer)entityIn).getAdvancements().award(adv, "hold_near_energy");
				}
			}
		}
		super.inventoryTick(stack, worldIn, entityIn, itemSlot, isSelected);
	}
	
	private WeakHashMap<ItemStack, BlockPos> lastBlock = new WeakHashMap<ItemStack, BlockPos>(2);
	
	private static DustParticleOptions neon_particle = new DustParticleOptions(new Vector3f(0F, 1F, 1F), 0.6F);//cyan
	
	public boolean absorbEnergy(ItemStack stack, Level w, BlockPos pos, int range)
	{
		if(!w.isClientSide)
		{
			BlockPos src = lastBlock.compute(stack, (k,v) -> 
			{
				if(v!=null && pos.distSqr(v) < range*range)
					return v;
				else
				{
					int dx = w.random.nextInt(5) - w.random.nextInt(5);
					int dz = w.random.nextInt(5) - w.random.nextInt(5);
					for(int dy=5;dy>-5;dy--)
					{
						BlockPos xyz = pos.offset(dx,dy,dz);
						BlockEntity t = w.getBlockEntity(xyz);
						if(t!=null)
						{
							LazyOptional<INeonEnergyStorage> lo = getCapability(t);
							if(lo!=null)
								return xyz;
						}
					}
					return null;
				}
				
			});
			if(src != null)
			{
				LazyOptional<INeonEnergyStorage> opt = getCapability(w.getBlockEntity(src));
				if(opt != null && opt.map(s -> addEnergy(stack, s)).orElse(false))
				{
					ServerLevel server = (ServerLevel) w;
					
					float sx = src.getX() - pos.getX();
					float sy = src.getY() - pos.getY();
					float sz = src.getZ() - pos.getZ();
					
					sx *= 0.3F;
					sy *= 0.3F;
					sz *= 0.3F;		
					
					server.sendParticles(neon_particle, src.getX()+0.5, src.getY()+1.5, src.getZ()+0.5, 5, sx, sy, sz, 0.5F);
					return true;
				}
				else
				{
					lastBlock.remove(stack);
				}
			}
		}
		return false;
	}
	
	@Nullable
	private LazyOptional<INeonEnergyStorage> getCapability(BlockEntity tile)
	{
		if(tile==null)
			return null;
		
		for(Direction d : FacingUtil.VALUES)
		{
			LazyOptional<INeonEnergyStorage> opt = tile.getCapability(CapabilityNeon.cap_NEON, d);
			if(opt.isPresent())
				return opt;
		}
		return null;
	}
	
	private boolean addEnergy(ItemStack stack, INeonEnergyStorage storage)
	{
		if(isNeonable(stack))
		{
			int i = stack.getOrCreateTag().getInt("stored");
			int d = storage.use(100);
			i += d;
			stack.getTag().putInt("stored", 0);
			
			addNeon(stack, i);

			return d > 0;
		}
		else
		{
			return false;
		}
	}
	
	@Override
	public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) 
	{
		tooltip.add(new TranslatableComponent(this.getDescriptionId() + ".tooltip"));
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
	}

	public static float getFillState(ItemStack stack) 
	{
		int i = ((IItemNeon)MiscItems.ancient_energy).getNeon(stack);
		return i / 1e6F;
	}

	@Override
	public int getMaxNeon(ItemStack it)
	{
		return 100000000; //100M NE
	}
}
