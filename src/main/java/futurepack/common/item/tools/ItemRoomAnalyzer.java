package futurepack.common.item.tools;

import futurepack.api.interfaces.IChunkAtmosphere;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageAirFilledRoom;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.PacketDistributor;

public class ItemRoomAnalyzer extends Item
{
	private final static int cw = 3, cd = 3, ch = 3;
	
	public static byte[] getAirData(Level w, BlockPos pos)
	{
		if(AtmosphereManager.hasWorldOxygen(w))
			return null;
		
		byte[] data = new byte[4096 * cw * cd * ch]; 
		int cx = (pos.getX()>>4) -1;
		int cz = (pos.getZ()>>4) -1;
		int cy = (pos.getY()>>4) -1;
		
		int sx = cx * 16;
		int sy = cy * 16;
		int sz = cz * 16;
		int ex = sx + cw * 16;
		int ey = sy + ch * 16;
		int ez = sz + cd * 16;
		
		for(int fx=sx;fx<ex;fx++)
		{
			for(int fz=sz;fz<ez;fz++)
			{
				LevelChunk c = w.getChunk(fx >> 4, fz >> 4);
				LazyOptional<IChunkAtmosphere> opt = c.getCapability(AtmosphereManager.cap_ATMOSPHERE, null);
				int x = fx, z = fz;
				opt.ifPresent(atm -> 
				{
					int maxair = atm.getMaxAir();
					
					for(int y=sy;y<ey;y++)
					{
						int air = atm.getAirAt(x&15,y&255,z&15);
						int index = (x-sx)*cd*ch*256 + (y-sy)*cd*16 + (z-sz);
						if(air==0)
						{
							data[index] = -128;
						}
						else
						{
							int airCeiled = (air * 255 / maxair);
							if(airCeiled<0)
								airCeiled = 0;
							byte b = (byte) (airCeiled - 128);
									
							data[index] = b;
						}
					}
				});
			}
		}
		
		return data;
	}
	
	public ItemRoomAnalyzer(Item.Properties props)
	{
		super(props);
//		setCreativeTab(FPMain.tab_items);
	}
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level w, Player pl, InteractionHand handIn)
	{
		ItemStack held = pl.getItemInHand(handIn);
		if(!w.isClientSide)
		{
			BlockPos pos = pl.blockPosition();
			byte[] data = getAirData(w,pos);
			
			if(data==null)
			{
				pl.sendMessage(new TranslatableComponent("chat.escanner.athmosphere.breathable"), Util.NIL_UUID);
			}
			else
			{
				FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with( () -> (ServerPlayer)pl), new MessageAirFilledRoom(pos, data, new Vec3i(cw,ch,cd)));
			}
		}
		pl.getCooldowns().addCooldown(this, 20 * 2);
		return new InteractionResultHolder<ItemStack>(InteractionResult.SUCCESS, held);
	}
}
