package futurepack.common.item.tools;

import futurepack.common.item.ResourceItems;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.FishingHook;
import net.minecraft.world.item.FishingRodItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemFpFishingRod extends FishingRodItem
{
	public ItemFpFishingRod(Item.Properties props)
	{
		super(props);
	
//		this.setMaxDamage(512);
//		setCreativeTab(FPMain.tab_items);
	}
	
	@Override
	public int getEnchantmentValue()
	{
		return 4;
	}
	
	@Override
	public void inventoryTick(ItemStack stack, Level worldIn, Entity entityIn, int itemSlot, boolean isSelected) 
	{
		if(entityIn  instanceof Player)
		{
			Player pl = (Player) entityIn;
			InteractionHand held = null;
			if(stack == pl.getMainHandItem())
			{
				held = InteractionHand.MAIN_HAND;
				
			}
			else if(stack == pl.getOffhandItem())
			{
				held = InteractionHand.OFF_HAND;
			}
			else
			{
				return;
			}
				
			if (pl.fishing != null)
			{
				FishingHook hook = pl.fishing;
				boolean fish = isFishHocked(hook);
				
				if(fish)
				{
					int i = pl.fishing.retrieve(stack);
					stack.hurtAndBreak(i, pl, breaker -> {});
					pl.swing(held);
					worldIn.playSound((Player)null, pl.getX(), pl.getY(), pl.getZ(), SoundEvents.FISHING_BOBBER_RETRIEVE, SoundSource.NEUTRAL, 1.0F, 0.4F / (worldIn.random.nextFloat() * 0.4F + 0.8F));
				}
			}
		}
	}
	
	@Override
	public boolean isValidRepairItem(ItemStack toRepair, ItemStack repair)
	{
		ItemStack mat = new ItemStack(ResourceItems.composite_metal);
		
		if(!mat.isEmpty() && repair.sameItem(mat))
			return true;
		return super.isValidRepairItem(toRepair, repair);
	}
	
	private boolean isFishHocked(FishingHook hook)
	{		
		return hook.nibble > 0 && hook.nibble < 20;
	}
	
}
