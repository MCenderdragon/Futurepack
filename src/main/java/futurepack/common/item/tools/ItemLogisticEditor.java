package futurepack.common.item.tools;

import java.util.function.Supplier;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.client.render.block.RenderLogistic;
import futurepack.depend.api.helper.HelperChunks;
import futurepack.depend.api.interfaces.IItemMetaSubtypes;
import net.minecraft.client.Minecraft;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;

public class ItemLogisticEditor extends Item implements IItemMetaSubtypes
{
	static
	{
		LogisticStorage.callback = ItemLogisticEditor::updateLogistic;
	}
	
	public ItemLogisticEditor(Item.Properties prop)
	{
		super(prop.stacksTo(1));
	}
	
	public static EnumLogisticType getModeFromItem(ItemStack it)
	{
		CompoundTag nbt = it.getOrCreateTagElement("logistic");
		return EnumLogisticType.getType(nbt.getInt("mode"));
	}
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level w, Player pl, InteractionHand hand)
	{
		ItemStack it = pl.getItemInHand(hand);
		CompoundTag nbt = it.getOrCreateTagElement("logistic");
		EnumLogisticType[] types = EnumLogisticType.values();
		nbt.putInt("mode", (nbt.getInt("mode") + 1) % types.length);
		
		if(w.isClientSide)
		{
			Supplier<Supplier<Void>> s = () -> {
				return () -> 
				{
					RenderLogistic.refreshRenderng();
					return null;
				};
			}; 
			s.get().get();
			
		}
		return InteractionResultHolder.success(it);
	}
	
	@Override
	public InteractionResult useOn(UseOnContext context)
	{
		ItemStack it = context.getItemInHand();
		BlockPos pos = context.getClickedPos();
		Level w = context.getLevel();
		BlockEntity tile = w.getBlockEntity(pos);
		if(tile!=null)
		{
			if(!w.isClientSide)
			{
				tile.getCapability(CapabilityLogistic.cap_LOGISTIC, context.getClickedFace()).ifPresent(logistic -> 
				{
					EnumLogisticType mode = getModeFromItem(it);
					if(logistic.isTypeSupported(mode))
					{
						EnumLogisticIO[] inout = EnumLogisticIO.values();
						for(int i=0;i<inout.length;i++)
						{
							int j = ( logistic.getMode(mode).ordinal() +1+i ) % inout.length;
							if(logistic.setMode(inout[j], mode))
							{
								//updateLogistic(tile); moved to setMode call
								break;
							}
						}
					}				
//					w.sendBlockUpdated(pos, Blocks.AIR.defaultBlockState(), w.getBlockState(pos), 2); //sends the tile entity changes
//					w.updateNeighborsAt(pos, w.getBlockState(pos).getBlock());//causes the forge event for block update
//					w.getBlockState(pos).updateNeighbourShapes(w, pos, 2, 128);//triggers the post-placement update for state changes of tubes and such
				});
			}
			else
			{
				sheduledRenderUpdate(pos, w);
			}
			HelperChunks.renderUpdate(context.getLevel(), pos);//(pos.add(-1,-1,-1), pos.add(1,1,1));
			
			return InteractionResult.SUCCESS;
		}
		return InteractionResult.PASS;//super.onItemUse(stack, pl, w, pos, side, hitX, hitY, hitZ);
	}
	
	public static void updateLogistic(BlockEntity tile)
	{
		tile.setChanged();
		
		Level w = tile.getLevel();
		BlockPos pos = tile.getBlockPos();
		if(w!=null && pos !=null)
		{
			w.sendBlockUpdated(pos, Blocks.AIR.defaultBlockState(), w.getBlockState(pos), 2); //sends the tile entity changes
			w.updateNeighborsAt(pos, w.getBlockState(pos).getBlock());//causes the forge event for block update
			w.getBlockState(pos).updateNeighbourShapes(w, pos, 2, 128);//triggers the post-placement update for state changes of tubes and such
		}
	}
	
	public static void sheduledRenderUpdate(BlockPos pos, Level w)
	{
		Thread t = new Thread((Runnable)  () ->
		{
			try
			{
				Thread.sleep(100);
			} 
			catch (InterruptedException e) {
				e.printStackTrace();
			}
			Minecraft.getInstance().submitAsync(() -> {
				HelperChunks.renderUpdate(w, pos);//(pos.add(-1,-1,-1), pos.add(1,1,1));
			});
		}, "S");
		t.setDaemon(true);
		t.start();
	}
	
//	private <T> boolean inArray(T[] array, T obj)
//	{
//		for(T t : array)
//		{
//			if(obj.equals(t))
//				return true;
//		}
//		return false;
//	}
	
	@Override
	public String getDescriptionId(ItemStack stack)
	{
		EnumLogisticType mode = getModeFromItem(stack);
		return super.getDescriptionId(stack) + "." + mode.name();
	}

	@Override
	public int getMaxMetas()
	{
		return EnumLogisticType.values().length;
	}

	@Override
	public String getMetaName(int meta)
	{
		EnumLogisticType[] modes = EnumLogisticType.values();
		EnumLogisticType mode = modes[meta % modes.length];
		return "logistic_edit_" + mode.name().toLowerCase();
	}
	
	
	
}
