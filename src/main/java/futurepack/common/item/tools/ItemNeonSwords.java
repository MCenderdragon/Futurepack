package futurepack.common.item.tools;

import java.util.List;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;

import futurepack.api.interfaces.IItemNeon;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.core.NonNullList;
import net.minecraft.network.chat.Component;
import net.minecraft.util.Mth;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;

public class ItemNeonSwords extends Item implements IItemNeon
{
	private final float dmg;
	
	private final Multimap<Attribute, AttributeModifier> stats_dmg;
	private final Multimap<Attribute, AttributeModifier> stats_no_dmg;
	
	public ItemNeonSwords(float damage, Item.Properties props)
	{
		super(props);
		this.dmg = damage;
		stats_dmg = ImmutableMultimap.of(Attributes.ATTACK_DAMAGE, new AttributeModifier(BASE_ATTACK_DAMAGE_UUID, "Weapon modifier", dmg, AttributeModifier.Operation.ADDITION),
				Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Weapon modifier", +1.2, AttributeModifier.Operation.ADDITION));
		stats_no_dmg = ImmutableMultimap.of(Attributes.ATTACK_DAMAGE, new AttributeModifier(BASE_ATTACK_DAMAGE_UUID, "Weapon modifier", dmg, AttributeModifier.Operation.ADDITION),
				Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Weapon modifier", +1.2, AttributeModifier.Operation.ADDITION));
	}

	@Override
	public int getBarColor(ItemStack stack)
	{
		return Mth.hsvToRgb(0.52F, 1.0F, (0.5F + (float)getNeon(stack) / (float)getMaxNeon(stack) * 0.5F));
	}
	
	@Override
	public int getBarWidth(ItemStack stack)
	{
		return (int) (13 * ( ((double)getNeon(stack) / (double)getMaxNeon(stack))));
	}
	
	@Override
	public boolean isBarVisible(ItemStack stack)
	{
		return getNeon(stack) < getMaxNeon(stack);
	}
	
	@Override
	public int getMaxNeon(ItemStack it)
	{
		return 800;
	}
	
	@Override
	public Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlot equipmentSlot, ItemStack it)
    {
        if (equipmentSlot == EquipmentSlot.MAINHAND)
        {
           return getNeon(it) > 0 ? stats_dmg : stats_no_dmg;
        }

        return super.getAttributeModifiers(equipmentSlot, it);
    }
	
	@Override
	public boolean hurtEnemy(ItemStack stack, LivingEntity target, LivingEntity attacker)
	{
		if(getNeon(stack)>0)
			addNeon(stack, -1);
		
		if(this == ToolItems.sword_glowtite && target.isInvertedHealAndHarm())
		{
			target.setSecondsOnFire(10);
		}
		else if(this == ToolItems.sword_bioterium)
		{
			MobEffectInstance eff = new MobEffectInstance(MobEffects.POISON, 15*20 , 2);
			target.addEffect(eff);
		}
		return super.hurtEnemy(stack, target, attacker);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) 
	{
		tooltip.add(HelperItems.getTooltip(stack, this));
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
	}
	
	@Override
	public void fillItemCategory(CreativeModeTab group, NonNullList<ItemStack> items) 
	{
		if(this.allowdedIn(group))
		{
			ItemStack it = new ItemStack(this, 1);
			addNeon(it, getMaxNeon(it));
			items.add(it);
		}
	}
	
	@Override
	public float getDestroySpeed(ItemStack stack, BlockState state)
    {
        Block block = state.getBlock();

        if (block == Blocks.COBWEB)
        {
            return 15.0F;
        }
        else
        {
            Material material = state.getMaterial();
            return material != Material.PLANT && material != Material.REPLACEABLE_PLANT && material != Material.WATER_PLANT && material != Material.REPLACEABLE_WATER_PLANT && material != Material.LEAVES && material != Material.VEGETABLE ? 1.0F : 1.5F;
        }
    }
	
	@Override
    public boolean isCorrectToolForDrops(BlockState blockIn)
    {
        return blockIn.getBlock() == Blocks.COBWEB;
    }
}
