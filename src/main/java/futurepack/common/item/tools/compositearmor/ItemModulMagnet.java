package futurepack.common.item.tools.compositearmor;

import java.util.List;

import futurepack.common.MagnetActivisionHelper;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.EquipmentSlot.Type;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class ItemModulMagnet extends ItemModulNeonContainer 
{

	public ItemModulMagnet(Item.Properties props) 
	{
		super(null, 200, props);
	}

	@Override
	public void onArmorTick(Level world, Player pl, ItemStack it, CompositeArmorInventory armor)
	{
		pullEnergy(armor, it);
		
		if(!MagnetActivisionHelper.isMagnetOn(pl))
			return;
			
		int ne = getNeon(it);
		
		if(ne <= 2)
			return;
		
		int range = 1;
		
		int old_neon = ne;
		
		boolean first = true;
		
		
		EquipmentSlot slots[] = {EquipmentSlot.CHEST, EquipmentSlot.FEET, EquipmentSlot.LEGS, EquipmentSlot.HEAD};
		for(EquipmentSlot e : slots)
		{
			CompositeArmorPart p = armor.getPart(e);
			if(p!=null)
			{
				for(int i=0;i < p.getSlots();i++)
				{
					ItemStack m = p.getStackInSlot(i);
					
					if(m.getItem() instanceof ItemModulMagnet)
					{
						int local_ne = getNeon(m);
						
						if(local_ne <= 2)
							continue;
						
						if(first && m != it)
						{
							//i'am not the master 
							return;						
						}
						
						if(m==it)
						{
							first = false;
						}
						
						range ++;
					}
				}
			}	
		}
		
		
		List<ItemEntity> list = world.getEntitiesOfClass(ItemEntity.class, pl.getBoundingBox().inflate(range, range, range));
		for(ItemEntity item : list)
		{
			double d3 = (pl.getX()) - item.getX() ;
			double d4 = (pl.getY()) - item.getY() ;
			double d5 = (pl.getZ()) - item.getZ() ;
			
			double dis = Math.sqrt(pl.distanceToSqr(item.getX(), item.getY(), item.getZ()));
			
			d3 = d3/dis ;
			d4 = d4/dis ;
			d5 = d5/dis ;
			
			item.setExtendedLifetime();
			if(world.isClientSide)
			{
				item.setDeltaMovement(d3, d4, d5);
			}
			else
			{
				item.setPos(pl.getX(), pl.getY(), pl.getZ());
			}
			
			ne -= range;
			
			if(ne <= 0)
			{
				ne = 0;
				break;
			}
			
		}
		
		addNeon(it, ne - old_neon);

	}
	
	@Override
	public void appendHoverText(ItemStack it, Level w, List<Component> l, TooltipFlag p_77624_4_) 
	{
		super.appendHoverText(it, w, l, p_77624_4_);
		l.add(new TextComponent("Activate with Keyboard!"));
	}
	
	@Override
	public boolean isSlotFitting(ItemStack stack, EquipmentSlot type, CompositeArmorPart armorPart)
	{
		return type.getType() == Type.ARMOR;
	}
	
	@Override
	public boolean isEnergyConsumer()
	{
		return true;
		
	}
}


