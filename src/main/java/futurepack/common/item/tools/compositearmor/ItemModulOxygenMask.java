package futurepack.common.item.tools.compositearmor;

import futurepack.api.interfaces.IAirSupply;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemModulOxygenMask extends ItemModulOxygenContainer 
{

	public ItemModulOxygenMask(Item.Properties props) 
	{
		super(EquipmentSlot.HEAD, 600, props);
	}

	@Override
	public void onArmorTick(Level world, Player player, ItemStack it, CompositeArmorInventory armor)
	{			
 		int ox = getOxygen(it);
		
		IAirSupply supply = AtmosphereManager.getAirSupplyFromEntity(player);
		if(supply.getAir() >= 300)
		{	
			int filled = addOxygen(it, 6);
			
			if(filled > 0)
				supply.reduceAir(filled);
		}
		else
		{
			TankQueryResult query = queryAndDrainArmorOxygen(world, armor, it, 1);

			query.armorOxygenCapacity += getMaxOxygen(it);

			if(ox > 0 && query.drainedAmount <= 0 && !world.isClientSide)
			{
				addOxygen(it, -1);
				query.armorOxygenAmount += ox - 1;
			}
			else
			{
				query.armorOxygenAmount += ox;
			}

			if(ox > 1)
			{
				supply.addAir(1);
				AtmosphereManager.setAirTanks((float)query.armorOxygenAmount/query.armorOxygenCapacity, player);
			}
			else if(ox == 1)
			{
				AtmosphereManager.setAirTanks(0F, player);
			}
		}
	}
	
}
