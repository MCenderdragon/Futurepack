package futurepack.common.item.tools.compositearmor;

import futurepack.api.interfaces.IAirSupply;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemModulOxygenTank extends ItemModulOxygenContainer
{

	public ItemModulOxygenTank(Item.Properties props) 
	{
		super(null, 6000, props);
	}

	@Override
	public void onArmorTick(Level world, Player player, ItemStack it, CompositeArmorInventory inv)
	{
		if(!world.isClientSide)
		{
			IAirSupply supply = AtmosphereManager.getAirSupplyFromEntity(player);
			if(supply.getAir() >= 300)
			{	
				int filled = addOxygen(it, 6);
				
				if(filled > 0)
					supply.reduceAir(filled);
			}
		}
		
		
	}
	
	@Override
	public boolean isSlotFitting(ItemStack stack, EquipmentSlot type, CompositeArmorPart armorPart)
	{
		return type.getType() == EquipmentSlot.Type.ARMOR;
	}

	@Override
	public boolean isDrainableByOther()
	{
		return true;
	}
}
