package futurepack.common.item.tools.compositearmor;

import futurepack.api.interfaces.IItemNeon;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemModulBattery extends ItemModulNeonContainer implements IItemNeon
{

	public ItemModulBattery(int maxNE, Item.Properties props)
	{
		super(null, maxNE, props);
	}

	@Override
	public void onArmorTick(Level world, Player player, ItemStack it, CompositeArmorInventory inv)
	{
		if(getNeon(it)<=0)
		{
			return;
		}
		
		chargeItem(it, player.getMainHandItem());
		chargeItem(it, player.getOffhandItem());
	}
	
	
	private void chargeItem(ItemStack it, ItemStack hand)
	{
		if(hand.getItem() instanceof IItemNeon)
		{		
			int ne = getNeon(it);
			int other_max = ((IItemNeon)hand.getItem()).getMaxNeon(hand);
			int other_ne = ((IItemNeon)hand.getItem()).getNeon(hand);
		
			int charge = Math.min(Math.min(ne, other_max-other_ne), 5);
			
			if(charge > 0)
			{
				addNeon(it, -charge);
				((IItemNeon)hand.getItem()).addNeon(hand, charge);
			}
		}
	}
	
	@Override
	public boolean isSlotFitting(ItemStack stack, EquipmentSlot type, CompositeArmorPart armorPart)
	{
		return type.getType() == EquipmentSlot.Type.ARMOR;
	}
	
	@Override
	public boolean isEnergyProvider()
	{
		return true;
	}
	
	@Override
	public boolean isEnergyConsumer()
	{
		return true;
	}

}
