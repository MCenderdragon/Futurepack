package futurepack.common;

import futurepack.common.entity.CapabilityPlayerData;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.item.tools.compositearmor.CompositeArmorPart;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.KeyManager.EnumKeyTypes;
import futurepack.common.sync.KeyManager.EventFuturepackKey;
import futurepack.common.sync.MessageFPData;
import net.minecraft.Util;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.EquipmentSlot.Type;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.network.PacketDistributor;

public class MagnetActivisionHelper
{
	@SubscribeEvent
	public static void onKeyPressed(EventFuturepackKey key)
	{		
		if(key.type==EnumKeyTypes.MODUL_MAGNET)
		{
			if(!hasMagnetInstalled(key.player))
			{
				setMagnet(key.player, false);
				key.player.sendMessage(new TranslatableComponent("modular_armor.module.magnet.notinstalled"), Util.NIL_UUID);
			}
			else
			{	
				boolean on = isMagnetOn(key.player);
				setMagnet(key.player, !on);
				
				if(on)
				{
					key.player.sendMessage(new TranslatableComponent("modular_armor.module.magnet.off"), Util.NIL_UUID);
				}
				else
				{
					key.player.sendMessage(new TranslatableComponent("modular_armor.module.magnet.on"), Util.NIL_UUID);
				}
			}
		}
		
	}
	
	public static boolean isMagnetOn(Player pl)
	{
		final CompoundTag fp = CapabilityPlayerData.getPlayerdata(pl);
		return fp.getBoolean("magnet");
	}
	
	public static boolean hasMagnetInstalled(Player pl)
	{
		for(EquipmentSlot armor : EquipmentSlot.values())
		{
			if(armor.getType() == Type.ARMOR)
			{
				CompositeArmorPart part = CompositeArmorPart.getInventory(pl.getItemBySlot(armor));
				if(part != null)
				{
					int slots = part.getSlots();
					for(int i=0;i<slots;i++)
					{
						ItemStack modul = part.getStackInSlot(i);
						if(!modul.isEmpty() && modul.getItem() == ToolItems.modul_magnet)
							return true;
					}
				}
			}
		}

		return false;
	}
	
	public static void setMagnet(Player pl, boolean on)
	{
		final CompoundTag fp = CapabilityPlayerData.getPlayerdata(pl);;
		fp.putBoolean("magnet", on);
		if(pl instanceof ServerPlayer)
		{
			sync((ServerPlayer) pl);
		}
	}
	
	public static void sync(ServerPlayer pl)
	{
		final CompoundTag fp = CapabilityPlayerData.getPlayerdata(pl);;
		FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() -> pl), new MessageFPData(fp, pl.getId()));
	}
};
