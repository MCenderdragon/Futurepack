package futurepack.common.dim.structures.generation;

import it.unimi.dsi.fastutil.ints.Int2BooleanArrayMap;
import net.minecraft.nbt.CompoundTag;

public class PlacementConditions 
{
	
	public static And AND(IPlacementCondition...conditions)
	{
		return new And(conditions);
	}
	
	public static Or OR(IPlacementCondition...conditions)
	{
		return new Or(conditions);
	}
	
	public record And(IPlacementCondition...conditions) implements IPlacementCondition
	{
		@Override
		public boolean test(BakedDungeon t, CompoundTag u) 
		{
			for(var c : conditions)
				if(!c.test(t,u))
					return false;
			return true;
		}

		@Override
		public void onPlaced(DungeonRoom room) 
		{
			for(var c : conditions)
				c.onPlaced(room);
		}
		
	}
	
	public record Or(IPlacementCondition...conditions) implements IPlacementCondition
	{
		@Override
		public boolean test(BakedDungeon t, CompoundTag u) 
		{
			for(var c : conditions)
				if(c.test(t,u))
					return true;
			return false;
		}

		@Override
		public void onPlaced(DungeonRoom room) 
		{
			for(var c : conditions)
				c.onPlaced(room);
		}
	}
	
	public record MinTecLevel(int level) implements IPlacementCondition
	{
		@Override
		public boolean test(BakedDungeon t, CompoundTag u) 
		{
			return u.getInt("tecLevel") >= level;
		}
	}
	
	public record MaxTecLevel(int level) implements IPlacementCondition
	{
		@Override
		public boolean test(BakedDungeon t, CompoundTag u) 
		{
			return u.getInt("tecLevel") <= level;
		}
	}
	
	public record MinLevel(int level) implements IPlacementCondition
	{
		@Override
		public boolean test(BakedDungeon t, CompoundTag u) 
		{
			return u.getInt("level") >= level;
		}
	}
	
	public record MaxLevel(int level) implements IPlacementCondition
	{
		@Override
		public boolean test(BakedDungeon t, CompoundTag u) 
		{
			return u.getInt("level") <= level;
		}
	}
	
	public static class OncePerDungeon implements IPlacementCondition
	{
		private boolean placed = false;
		
		@Override
		public boolean test(BakedDungeon t, CompoundTag u) 
		{
			return !placed;
		}
		
		@Override
		public void onPlaced(DungeonRoom room) 
		{
			placed = true;
		}
	}
	
	public static class OncePerLevel implements IPlacementCondition
	{
		private Int2BooleanArrayMap placed = new Int2BooleanArrayMap(20);
		
		@Override
		public boolean test(BakedDungeon t, CompoundTag u) 
		{
			return placed.getOrDefault(u.getInt("level"), false);
		}
		
		@Override
		public void onPlaced(DungeonRoom room) 
		{
			placed.put(room.extra.getInt("level"), true);
			
		}
	}
}
