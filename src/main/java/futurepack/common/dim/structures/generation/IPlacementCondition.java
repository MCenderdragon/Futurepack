package futurepack.common.dim.structures.generation;

import java.util.function.BiPredicate;

import net.minecraft.nbt.CompoundTag;

public interface IPlacementCondition extends BiPredicate<BakedDungeon, CompoundTag>
{
	public static IPlacementCondition ALWAYS_TRUE = (a,b)->true;
	
	default void onPlaced(DungeonRoom room) 
	{
		
	}
	
	
}
