package futurepack.common.dim.structures.generation;


import futurepack.common.dim.structures.StructureBase;
import net.minecraft.nbt.CompoundTag;

public record Entry(StructureBase structure, EnumGenerationType type, int weight, IPlacementCondition con) implements IPlacementCondition
{

	@Override
	public boolean test(BakedDungeon t, CompoundTag u) 
	{
		return con.test(t, u);
	}

	@Override
	public void onPlaced(DungeonRoom room)
	{
		con.onPlaced(room);
	}
	
}
