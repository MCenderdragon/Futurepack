package futurepack.common.dim.structures.enemys;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

/**
 * This class is used to add waves to the huge dungeons
 */
public class WaveRegistry
{
	private static Map<Integer, ArrayList<Entry>> registry = new Int2ObjectOpenHashMap<ArrayList<Entry>>();
	
	static
	{
		register("L0_boss", 0, EnumWaveType.BOSS);
		register("L0_normal", 0, EnumWaveType.NORMAL);
		
		register("L1_boss", 1, EnumWaveType.BOSS);
		register("L1_normal_1", 1, EnumWaveType.NORMAL);
		register("L1_normal_2", 1, EnumWaveType.NORMAL);
		
		register("L2_boss", 2, EnumWaveType.BOSS);
		register("L2_normal_1", 2, EnumWaveType.NORMAL);
		register("L2_normal_2", 2, EnumWaveType.NORMAL);
		
		register("L3_boss", 3, EnumWaveType.BOSS);
		register("L3_normal_1", 3, EnumWaveType.NORMAL);
		register("L3_normal_2", 3, EnumWaveType.NORMAL);
		register("L3_normal_3", 3, EnumWaveType.NORMAL);
	}
	
	public static void register(String name, int tecLvl, EnumWaveType type)
	{
		ArrayList<Entry> entrys = registry.get(tecLvl);
		if(entrys==null)
		{
			entrys = new ArrayList();
			registry.put(tecLvl, entrys);
		}
		entrys.add(new Entry(name, type));
	}

	public static String[] getRandomWaves(int tecLvl, Random rand)
	{
		ArrayList<Entry> entrys = registry.get(tecLvl);
		if(entrys==null)
			return null;
		
		Entry[] bosses = entrys.stream().filter(e -> {return e.type==EnumWaveType.BOSS;}).toArray(Entry[]::new);
		Entry[] normal = entrys.stream().filter(e -> {return e.type==EnumWaveType.NORMAL;}).toArray(Entry[]::new);
		
		int min = normal.length / 2;
		if(min ==0 && normal.length>0)
		{
			min=1;
		}
		int amount = min;
		if(normal.length-min > 0)
		{
			amount += rand.nextInt(normal.length-min);
		}
		
		ArrayList<String> string = new ArrayList<String>();
		
		for(int i=0;i<amount;i++)
		{
			String wave = normal[rand.nextInt(normal.length)].name;
			if(string.size()>0)
			{
				String last = string.get(string.size()-1);
				if(wave.equals(last))
				{
					i--;
					continue;
				}
			}
			
			string.add(wave);
		}
		
		
		String wave = bosses[rand.nextInt(bosses.length)].name;
		string.add(wave);
		
		//System.out.println(Arrays.toString(string.toArray()));
		
		return string.toArray(new String[string.size()]);
	}
	
	private static class Entry
	{
		final String name;
		final EnumWaveType type;
		
		public Entry(String name, EnumWaveType type)
		{
			super();
			this.name = name;
			this.type = type;
		}	
	}
	
	public static enum EnumWaveType
	{
		BOSS,
		NORMAL;
		
		@Override
		public String toString()
		{
			return super.toString().toLowerCase();
		}
	}
}
