package futurepack.common.dim.structures;

import java.util.ArrayList;
import java.util.Random;

import futurepack.common.block.misc.MiscBlocks;
import futurepack.common.block.misc.TileEntityDungeonSpawner;
import futurepack.common.dim.structures.enemys.WaveRegistry;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.storage.loot.LootTables;

public class StructureBoss extends StructureBase
{
	BlockPos[] spawner;
	
	public StructureBoss(StructureBase base)
	{
		super(base);
		BlockState[][][] blocks = getBlocks();
		
		ArrayList<BlockPos> list = new ArrayList<>();
		for(int x=0;x<blocks.length;x++)
		{
			for(int y=0;y<blocks[x].length;y++)
			{
				for(int z=0;z<blocks[x][y].length;z++)
				{
					if(blocks[x][y][z]!=null)
					{
						if(blocks[x][y][z].getBlock() == MiscBlocks.dungeon_spawner)
						{
							list.add(new BlockPos(x,y,z));
						}
					}
				}
			}
		}
		spawner = list.toArray(new BlockPos[list.size()]);
		
		if(getRawDoors().length < 2)
		{
			throw new IllegalArgumentException("Invalid Boss room, has less then 2 doors! " + this);
		}
	}

	@Override
	public void addChestContentBase(ServerLevelAccessor w, BlockPos start, Random rand, CompoundTag extraData, LootTables manager)
	{
		super.addChestContentBase(w, start, rand, extraData, manager);
		int tecLvl = extraData.getInt("tecLevel");
		
		for(BlockPos pos : spawner)
		{
			BlockPos p = start.offset(pos);
			BlockEntity e = w.getBlockEntity(p);
			if(e!=null)
			{
				TileEntityDungeonSpawner tile = (TileEntityDungeonSpawner) e;
				tile.addWaves(WaveRegistry.getRandomWaves(tecLvl, rand));
			}
		}
		
	}
}
