package futurepack.common.dim.structures;

import java.util.Random;

public class NameGenerator 
{
	public static String[] d_adjective = {
			"shivering", "better", "boring", "precognitive", "severed", "royal", "beautiful", "influential", "sacred", "condemned", "dishonored", "nebulous", "polar",
			"cheesy", "bloody", "dark", "light", "gray", "scary", "tiny",
			"gigantic", "big", "rumbling"
	};
	
	public static String[] d_names = {
			"Cheesys", "Enders", "Dragons", "Mantes", "Wugis", "Skys",
			"Lexx", "Nicholas Basiles", "realm_18s", "Roy Remingtons", "Scott Jensens", "Sinan Islers", "SocksOns", //patrons
			"Zombies", "Creepers", "Spiders", "Villagers", "Bots", "Googles", "Apples", "Microsofts", "Mojangs", "Notchs", "Direwolf20s", "Vaskiis", "Darkostos",
			"Rhix's", "The Queens", "The Dragons"
	};
	
	public static String[] d_places = {
			"Cheese", "Teeparty",
			"Dungeon", "Cave", "Lab", "Labratory", "Facility", "Production", "Ship", "Spaceship",
			"Bed", "Butt", //DoD
			"Factory", "Engine", "Nexus", "Hall", "Virtuality", "Berth", "Slip", "Dock", "Garden", "Hydroponics", "Cloning Center", "Medical", "Annex", "Bridge", "Tesseract Edge"
	};
	
	public static final NameGenerator DUNGEON = new NameGenerator(d_names, d_adjective, d_places);
	
	private String[] name, adj, place;  
	
	public NameGenerator(String[] name, String[] adj, String[] place) 
	{
		this.name = name;
		this.adj = adj;
		this.place = place;
	}
	
	public String getRandomName(Random r)
	{
		String owner, desc, thing;
		owner = name[r.nextInt(name.length)];
		desc = adj[r.nextInt(adj.length)];
		thing = place[r.nextInt(place.length)];
		return String.format("%s %s %s", owner, desc, thing);
	}
}
