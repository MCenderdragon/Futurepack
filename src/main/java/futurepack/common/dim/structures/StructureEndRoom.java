package futurepack.common.dim.structures;

import futurepack.common.block.misc.TileEntityDungeonCore;
import futurepack.common.dim.structures.generation.BakedDungeon;
import futurepack.common.dim.structures.generation.IDungeonEventListener;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.levelgen.structure.BoundingBox;

public class StructureEndRoom extends StructureBase implements IDungeonEventListener
{

	public StructureEndRoom(StructureBase base)
	{
		super(base);
	}

	
	@Override
	public IDungeonEventListener getEventListener()
	{
		return this;
	}


	@Override
	public void onDungeonFinished(ServerLevel w, BakedDungeon dungeon, BlockPos pos)
	{
		BoundingBox box = dungeon.getBoundingBox(pos);
		
		TileEntityDungeonCore[] obj = BlockPos.MutableBlockPos.betweenClosedStream(box)
			.map(w::getBlockEntity)
			.filter(t -> t!=null)
			.filter(t -> t.getClass() == TileEntityDungeonCore.class)
			.toArray(TileEntityDungeonCore[]::new);
		
		for(TileEntityDungeonCore core : obj)
		{
			core.setDungeon(dungeon);
		}
	}
}
