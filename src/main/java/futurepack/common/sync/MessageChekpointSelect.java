package futurepack.common.sync;

import java.util.List;
import java.util.function.Supplier;

import futurepack.common.block.misc.TileEntityDungeonCheckpoint;
import futurepack.common.dim.structures.TeleporterMap;
import futurepack.common.dim.structures.TeleporterMap.TeleporterEntry;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

public class MessageChekpointSelect 
{

	private long teleporterPos;
	
	public MessageChekpointSelect(long l) 
	{
		teleporterPos = l;
	}
	
	public static MessageChekpointSelect decode(FriendlyByteBuf buf) 
	{
		return new MessageChekpointSelect(buf.readLong());
	}
		
	public static void encode(MessageChekpointSelect msg, FriendlyByteBuf buf) 
	{
		buf.writeLong(msg.teleporterPos);
	}
			
	public static void consume(MessageChekpointSelect message, Supplier<NetworkEvent.Context> ctx) 
	{
		if(ctx.get().getDirection() == NetworkDirection.PLAY_TO_SERVER)
		{
			ctx.get().enqueueWork(new Runnable()
			{	
				@Override
				public void run() 
				{
					ServerPlayer pl = ctx.get().getSender();
					TeleporterMap tmap = TeleporterMap.getTeleporterMap((ServerLevel) pl.getLevel());
					List<TeleporterEntry> l = tmap.getTeleporter(pl);
					for(TeleporterEntry e : l)
					{
						if(e.pos.asLong() == message.teleporterPos)
						{
							TileEntityDungeonCheckpoint tile = (TileEntityDungeonCheckpoint) pl.getLevel().getBlockEntity(e.pos);
							tile.teleportTo(pl, e.pos);
						}
					}
						
				}
			});
			ctx.get().setPacketHandled(true);
		}
		
	}
}
