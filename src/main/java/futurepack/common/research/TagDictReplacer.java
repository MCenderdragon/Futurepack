package futurepack.common.research;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;

public class TagDictReplacer
{
	private static Thread t;
	
	public static void fixResearches()
	{
		if(t!=null && t.isAlive())
			return;
		
		t = new Thread(new Runnable()
		{	
			@Override
			public void run()
			{
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				checkResearches();
			}
		}, "FP-Research Ingredient Replacer");
		t.setDaemon(true);
		t.start();
	}
	
	private static int lastId = 0;
	
	private static void checkResearches()
	{
		try
		{
			for(;lastId<ResearchManager.getResearchCount();lastId++)
			{
				Research r = ResearchManager.getById(lastId);
				replaceResearchPredicates(r);
			}
		}
		catch(ConcurrentModificationException e)
		{
			//the mapping have changed, so we need to do this again.
			lastId = 0;
			e=null;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException ee) {
				ee.printStackTrace();
			}
			checkResearches();
		}
	}
	
	private static void replaceResearchPredicates(Research r)
	{
		//GONE in favour of 1.13 Item Tags
		
//		ItemPredicateBase[] preds = r.getNeeded();
//		for(int i=0;i<preds.length;i++)
//		{
//			ItemPredicateBase p = preds[i];
//			Class<? extends ItemPredicateBase> cls = p.getClass();
//			
//			if(cls == ItemPredicate.class)
//			{
//				Item item = p.getRepresentItem().getItem();
//				final int id = Item.getIdFromItem(item);
//				Stream<ArrayList<Integer>> stream = FPTagDictionary.getRegisteredItems().stream().filter(n -> (n>>16) == id).map(FPTagDictionary::getTagsFromStackId);
//				ArrayList<Integer> list = getJoinedList(stream.toArray(ArrayList[]::new));	
//				if(!list.isEmpty())
//				{
//					int[] tags = new int[list.size()];
//					for(int j=0;j<tags.length;j++)
//						tags[j]=list.get(j);			
//					preds[i] = new TagDictPredicate(tags, 1, new ItemStack(item));
//					FPLog.logger.info("Replaced research ingredients %s with tags %s", item, Arrays.toString(FPTagDictionary.getNames(tags)));
//				}
//			}
//			else if(cls == ItemStackPredicate.class)
//			{
//				ItemStack it = ((ItemStackPredicate)p).getPredicate();
//				ArrayList<Integer> list;
//				
//				if(it.getItemDamage()==OreDictionary.WILDCARD_VALUE)
//				{
//					final int id = Item.getIdFromItem(it.getItem());
//					//System.out.println("" + id);
//					Stream<ArrayList<Integer>> stream = FPTagDictionary.getRegisteredItems().stream().filter(n -> (n>>16) == id).map(FPTagDictionary::getTagsFromStackId);
//					list = getJoinedList(stream.toArray(ArrayList[]::new));	
//				}
//				else
//				{
//					final int id = FPTagDictionary.hash(it);
//					list = FPTagDictionary.getTagsFromStackId(id);
//				}
//				
//				if(list != null && !list.isEmpty())
//				{
//					int[] tags = new int[list.size()];
//					for(int j=0;j<tags.length;j++)
//						tags[j]=list.get(j);			
//					preds[i] = new TagDictPredicate(tags, it.getCount(), p.getRepresentItem());
//					FPLog.logger.info("Replaced research ingredients %s with tags: %s", it, Arrays.toString(FPTagDictionary.getNames(tags)));
//				}
//			}
//			else if(cls == OreDictPredicate.class)
//			{ }
//			else
//			{
//				FPLog.logger.warn("Unkniown format " + p.getClass());
//			}
//		}
	}
	
	/**
	 * this will return a list with elements all lists have
	 */
	@SuppressWarnings("unchecked")
	private static ArrayList<?> getJoinedList(@SuppressWarnings("rawtypes") ArrayList[] lists)
	{
		ArrayList<?> all = new ArrayList<Object>();
		if(lists.length>0)
		{
			all.addAll(lists[0]);
			for(int i=1;i<lists.length;i++)
			{
				ArrayList<?> l = lists[i];
				all.removeIf(elm -> !l.contains(elm));
				
				if(all.isEmpty())
					break;
			}
		}
		return all;
	}
}
