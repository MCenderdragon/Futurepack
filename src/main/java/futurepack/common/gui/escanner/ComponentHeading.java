package futurepack.common.gui.escanner;

import java.util.Iterator;

import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.util.FormattedCharSequence;

public class ComponentHeading extends ComponentText
{

	public ComponentHeading(String text)
	{
		super(new TextComponent(text));
		
	}
	
	public ComponentHeading(MutableComponent text)
	{
		super(text);
		
	}

	@Override
	public void init(int maxWidth, Screen gui)
	{
		width = maxWidth;
		
		font = gui.getMinecraft().font;	
//		StringTextComponent tc = new StringTextComponent(rawText);
		rawText.setStyle(rawText.getStyle().withFont(null));
		
		parts = font.split(rawText, width);
		height = font.lineHeight * parts.size();
	}
	
	@Override
	public void render(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, GuiScannerBase gui)
	{
		Iterator<FormattedCharSequence> it = parts.iterator();
		for(;it.hasNext();y+= font.lineHeight)
		{
			font.draw(matrixStack, it.next() , x, y, 0x547bb1);
		}
	}

}
