package futurepack.common.gui.escanner;

import futurepack.common.research.Research;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.TranslatableComponent;

public abstract class GuiResearchMainOverviewBase extends Screen
{

	protected GuiResearchMainOverviewBase(String titleIn) 
	{
		super(new TranslatableComponent(titleIn));
	}

	public abstract void initBuffer(Screen[] buffer);

	public abstract boolean openResearchText(Research r);

}
