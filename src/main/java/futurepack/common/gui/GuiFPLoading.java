package futurepack.common.gui;

import java.util.Random;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.resources.ResourceLocation;

public class GuiFPLoading extends Screen
{
	ResourceLocation res = new ResourceLocation(Constants.MOD_ID,"fp_screen.png");
	
	long seed = 7528787;
	int i = 0;
	
	public GuiFPLoading() 
	{
		super(new TextComponent("futurepack loading screen"));
		seed = System.currentTimeMillis();
	}
	
	@Override
	public void render(PoseStack matrixStack, int p_73863_1_, int p_73863_2_, float p_73863_3_) 
	{
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		
		fill(matrixStack, 0, 0, width, height, 0xff000000);
		drawStars(matrixStack);
		
		RenderSystem.setShaderTexture(0, res);
		//GL11.glScalef(i/100F, i/100F, i/100F);
		//fill(matrixStack, 0, 0, width, height, 0xffffffff);
		RenderSystem.setShaderColor(1, 1, 1, 1);
		blit(matrixStack, (width-256)/2, (height-256)/2, 0, 0, 256, 256);
		
		super.render(matrixStack, p_73863_1_, p_73863_2_, p_73863_3_);
	}
	
	public void drawStars(PoseStack matrixStack)
	{
		Random r = new Random(seed);
		
		for(int x=0;x<width;x++)
		{
			for(int y=0;y<height;y++)
			{
				if(r.nextInt(150)==0)
				{
					
					int base= (100+r.nextInt(156))<<16 | (100+r.nextInt(156))<<8 | (100+r.nextInt(156))<<0;
					fill( matrixStack, x, y, x+1, y+1, 255<<24 | base);
					int a = (int) ((Math.sin(  (this.i+ x*y)/3.0)) * 64) +128;
					fill(matrixStack, x-1, y, x+2, y+1, a<<24 | base);
					fill(matrixStack, x, y-1, x+1, y+2, a<<24 | base);
				}
			}
		}
			
	}
	
	@Override
	public void tick() 
	{
		super.tick();
		i++;
		if(i==100)
		{
			this.minecraft.setScreen(null);
		}
	}
	
	
}
