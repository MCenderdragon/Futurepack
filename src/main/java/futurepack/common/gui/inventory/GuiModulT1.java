package futurepack.common.gui.inventory;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.block.inventory.TileEntityModulT1;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.depend.api.helper.HelperGui;
import futurepack.depend.api.helper.HelperRendering;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.SlotItemHandler;

public class GuiModulT1 extends ActuallyUseableContainerScreen
{
	//private TileEntityBaterieBox tile;
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID,"textures/gui/modul_t1.png");
		
	public GuiModulT1(Player pl, TileEntityModulT1 tile)
	{
		super(new ContainerModulT1(pl.getInventory(), tile), pl.getInventory(), "gui.modul.t1");
		//this.tile = tile;
	}
		
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderBg(PoseStack matrixStack, float var1, int var2, int var3) 
	{
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		RenderSystem.setShaderTexture(0, res);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);
			
		HelperGui.renderNeon(matrixStack, k+7, l+7, tile().power, var2, var3);
	}
		
	@Override
	protected void renderLabels(PoseStack matrixStack, int mx, int my)
	{
		//this.font.drawString(matrixStack, I18n.format("container.modulT1", new Object[0]), 57, 3, 4210752);
		//this.font.drawString(matrixStack, I18n.format("container.inventory", new Object[0]), 43, this.ySize - 96 + 4, 4210752);
		HelperGui.renderNeonTooltip(matrixStack, leftPos, topPos, 7, 7, tile().power, mx, my);
	}
	
	private TileEntityModulT1 tile()
	{
		return ((ContainerModulT1)getMenu()).tile;
	}
		
	public static class ContainerModulT1 extends ContainerSyncBase
	{
		TileEntityModulT1 tile;
//		int lp;
			
		public ContainerModulT1(Inventory inv, TileEntityModulT1 tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
				
			this.addSlot(new SlotItemHandler(tile.getGui(), 0, 58, 12));
			this.addSlot(new SlotItemHandler(tile.getGui(), 1, 102, 56));
			this.addSlot(new SlotItemHandler(tile.getGui(), 2, 102, 12));
			this.addSlot(new SlotItemHandler(tile.getGui(), 3, 58, 56));
			
			int l;
			int i1;
								
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
				
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
			}
		}
		
		@Override
		public ItemStack quickMoveStack(Player pl, int index)
		{
			ItemStack itemstack = ItemStack.EMPTY;
	        Slot slot = this.slots.get(index);
			
	        if(slot != null && slot.hasItem())
			{
	        	ItemStack itemstack1 = slot.getItem();
	        	itemstack = itemstack1.copy();
				
	        	if(index < 4)
	        	{
	        		if (!this.moveItemStackTo(itemstack1, 4, this.slots.size(), true))
	        		{
	        			return ItemStack.EMPTY;
	        		}
	        	}
	        	else if (!this.moveItemStackTo(itemstack1, 0, 4, false))
	            {
	                return ItemStack.EMPTY;
	            }
	        	
			
	        	if (itemstack1.isEmpty())
	        	{
	        		slot.set(ItemStack.EMPTY);
	        	}
	        	else
	        	{
	        		slot.setChanged();
	        	}
				
			}
			return itemstack;
		}
			
		@Override
		public boolean stillValid(Player var1)
		{
			return true; //tile.isUseableByPlayer(var1);
		}
			
//		@Override
//		public void addCraftingToCrafters(ICrafting c)
//		{
//			super.addCraftingToCrafters(c);
////			c.sendProgressBarUpdate(this, 0, (int)this.tile.power);
//		}
//			
//		@Override
//		public void detectAndSendChanges() 
//		{
//			super.detectAndSendChanges();
//			if(this.lp != (int)this.tile.power)
//			{
//				for (int i = 0; i < this.listeners.size(); ++i)
//				{
//					ICrafting c = (ICrafting)this.listeners.get(i);
//					c.sendProgressBarUpdate(this, 0, (int)this.tile.power);
//				}
//				this.lp = (int)this.tile.power;
//			}		
//		}
//			
//		@Override
//		public void updateProgressBar(int id, int val)
//		{
//			super.updateProgressBar(id, val);
//			if(id==0)
//			{
//				this.tile.power=val;
//			}
//		}
	}
}
