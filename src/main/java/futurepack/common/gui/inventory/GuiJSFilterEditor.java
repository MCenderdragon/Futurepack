package futurepack.common.gui.inventory;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;

import org.lwjgl.glfw.GLFW;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.FPLog;
import futurepack.common.filter.ScriptItemFilterFactory;
import futurepack.common.gui.FormatedTextEditor;
import futurepack.common.gui.TextEditorGui;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.ISyncable;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperRendering;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.client.gui.components.Button;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.util.FormattedCharSequence;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;

public class GuiJSFilterEditor extends ActuallyUseableContainerScreen<GuiJSFilterEditor.ContainerJSFilterEditor>
{
	private TextEditorGui editor;
	private boolean isAdded;
	private boolean defaultFont = false;

	private static final int minWidth = 266, minHeight = 256;
	public static int userWidth = minWidth, userHeight = minHeight;

	private Button save;

	public GuiJSFilterEditor(Player pl)
	{
		super(new ContainerJSFilterEditor(pl.getInventory()), pl.getInventory(), "gui.js_item_filter");
		this.imageWidth = userWidth;
		this.imageHeight = userHeight;
		getMenu().setCompleteText(() -> GuiJSFilterEditor.this.editor.getEditor().getFullText());
	}


	@Override
	public void init()
	{
		super.init();

		if(imageWidth > width || imageHeight > height)
		{
			imageWidth = userWidth = minWidth;
			imageHeight = userHeight = minHeight;
		}

		this.minecraft.keyboardHandler.setSendRepeatsToGui(true);

		if(editor==null)
		{
			FormatedTextEditor te =  new FormatedTextEditor(new FormatedTextEditor.JavaScriptFormatter());
			editor = new TextEditorGui(imageWidth-10, imageHeight-56, te);
			editor.setPosition(leftPos+5, topPos+5);
			isAdded = false;
		}
		else
		{
			FormatedTextEditor te =  editor.getEditor();
			editor = new TextEditorGui(imageWidth-10, imageHeight-56, te);
			editor.setPosition(leftPos+5, topPos+5);
			isAdded = true;
			addWidget(editor);
		}

		if(!getMenu().isSyncedWithServer())
		{
			getMenu().setTypeClient(RequestType.REQUEST_TEXT);
			FPPacketHandler.syncWithServer(getMenu());
		}
		int w = 0;
		int x = leftPos+3;
		int y = topPos + (imageHeight-56);

		addRenderableWidget(save = new Button(x, y +10, w = 18+this.font.width("Save"), 20, new TextComponent("Save"), b ->  {
			getMenu().setCompileMsg("");
			getMenu().setTypeClient(RequestType.SAVE_TEXT);
			FPPacketHandler.syncWithServer(getMenu());
		}));
		x+=w+5;
		addRenderableWidget(new Button(x, y +10, w = 18+this.font.width("Reload"), 20, new TextComponent("Reload"), b ->  {
			isAdded = false;
			removeWidget(editor);
			getMenu().setSyncedWithServer(false);
			getMenu().setTypeClient(RequestType.REQUEST_TEXT);
			FPPacketHandler.syncWithServer(getMenu());
		}));
		x+=w+5;
		addRenderableWidget(new Button(x, y +10, w = 18+this.font.width("Compile"), 20, new TextComponent("Compile"), b ->  {
			getMenu().setTypeClient(RequestType.COMPILE);
			FPPacketHandler.syncWithServer(getMenu());
		}));
		x+=w+5;
		addRenderableWidget(new Button(x, y+10, w= 18+this.font.width("Change Fontsize"), 20, new TextComponent("Change Font Size"), b -> {
			if(defaultFont)
			{
				editor.setFont(HelperComponent.getUnicodeFont());
				defaultFont = false;
			}
			else
			{
				editor.setFont(Style.DEFAULT_FONT);
				defaultFont = true;
			}

		}));
	}

	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
		if(getMenu().isSyncedWithServer() && !isAdded)
		{
			if(getMenu().getLines()!=null)
			{
				isAdded = true;
				addWidget(editor);
				editor.getEditor().clear();
				editor.getEditor().addAll(getMenu().getLines());
				getMenu().setLines(null);
			}
			else
			{
				isAdded = false;
				removeWidget(editor);
				getMenu().setSyncedWithServer(false);
				getMenu().setTypeClient(RequestType.REQUEST_TEXT);
				FPPacketHandler.syncWithServer(getMenu());
			}
		}

        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }

	private boolean resize = false;

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button)
	{
		resize = false;
		if(button==0)
		{
			if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, imageWidth-10, imageHeight-10, imageWidth, imageHeight))
			{
				resize = true;
			}
		}
		return super.mouseClicked(mouseX, mouseY, button);
	}

	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int button)
	{
		if(resize)
		{
			int newWidth,newHeight;

			newWidth = (int) ((mouseX - width/2D) * 2D);
			newHeight = (int) ((mouseY - height/2D) * 2D);

			newWidth = Math.max(minWidth, newWidth);
			newHeight = Math.max(minHeight, newHeight);
			newWidth = Math.min(this.width, newWidth);
			newHeight = Math.min(this.height, newHeight);

			if(newHeight != imageHeight || newWidth != imageWidth)
			{
				userWidth = imageWidth = newWidth;
				userHeight = imageHeight = newHeight;
				init(getMinecraft(), width, height);
			}

			resize = false;
			return true;
		}
		return super.mouseReleased(mouseX, mouseY, button);
	}

	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
//		this.minecraft.getTextureManager().bindTexture(res);
//		this.blit(matrixStack, k, l, 0, 0, this.xSize, this.ySize);
		fill(matrixStack, leftPos, topPos, leftPos+imageWidth, topPos+imageHeight, 0xd0d0d0d0);
		fill(matrixStack, leftPos+imageWidth-10, topPos+imageHeight-10, leftPos+imageWidth, topPos+imageHeight, 0xff001090);
		fill(matrixStack, leftPos+imageWidth-10, topPos+imageHeight-10, leftPos+imageWidth-1, topPos+imageHeight-1, 0xff0030c0);

		editor.render(matrixStack, mouseX, mouseY, partialTicks);

		List<FormattedCharSequence> widthWrapped = this.font.split(new TextComponent("Compiler: " + getMenu().getCompileMsg()), imageWidth-10);
		int yOffset = 0;
		for(FormattedCharSequence rp : widthWrapped)
		{
			this.font.drawShadow(matrixStack, rp, leftPos+5, topPos + (imageHeight-56)+20+15+yOffset, 0xFF11FF11);
			yOffset+= this.font.lineHeight;
		}

		if(resize)
		{
			int newWidth,newHeight;
			newWidth = (int) ((mouseX - width/2D) * 2D);
			newHeight = (int) ((mouseY - height/2D) * 2D);
			int k = (width-newWidth)/2;
			int l = (height -newHeight)/2;

			int green = 0xFF00FF00;
			fill(matrixStack, k, l, k+newWidth, l+2, green);
			fill(matrixStack, k, l+newHeight-2, k+newWidth, l+newHeight, green);
			fill(matrixStack, k, l, k+2, l+newHeight, green);
			fill(matrixStack, k+newWidth-2, l, k+newWidth, l+newHeight, green);
		}
	}

	private int autosave;

	@Override
	public void containerTick()
	{
		autosave++;
		if(autosave > 20 * 60 *10)//auto-save every 10 minutes
		{
			getMenu().setTypeClient(RequestType.SAVE_TEXT);
			FPPacketHandler.syncWithServer(getMenu());
			autosave = 0;
		}
		super.containerTick();
	}

	@Override
	public void removed()
	{
		getMenu().setTypeClient(RequestType.SAVE_TEXT);
		FPPacketHandler.syncWithServer(getMenu());
		this.minecraft.keyboardHandler.setSendRepeatsToGui(false);
		super.removed();
	}

	@Override
	public void mouseMoved(double mouseX, double mouseY)
	{
		super.mouseMoved(mouseX, mouseY);
		editor.mouseMoved(mouseX, mouseY);
	}

	@Override
	public boolean keyPressed(int keyCode, int scanCode, int modifiers)
	{
		if(hasControlDown() && keyCode == GLFW.GLFW_KEY_S)
		{
			save.mouseClicked(save.x+1, save.y+1, 0);//triggering the button with all actiaveted checks
			return true;
		}
		return super.keyPressed(keyCode, scanCode, modifiers);
	}

	public static class ContainerJSFilterEditor extends ContainerJSFilterEditorBase
	{
		public ContainerJSFilterEditor(Inventory inv)
		{
			super(inv);
		}
		
		protected int getScriptId()
		{
			ItemStack filter = pl.getItemInHand(pl.getUsedItemHand());
			return filter.hashCode();
		}
		
		protected Reader getReader() throws IOException
		{
			ItemStack filter = pl.getItemInHand(pl.getUsedItemHand());
			Reader r = ScriptItemFilterFactory.getFilterScript(filter);
			return r;
		}
		
		
		
		protected Writer getWriter(int scriptId) throws FileNotFoundException
		{
			FPLog.logger.debug("Saved item filter script %s", scriptId);
			ItemStack filter = pl.getItemInHand(pl.getUsedItemHand());
			Writer w = ScriptItemFilterFactory.getFilterScriptWriter(filter);
			return w;
		}
		
		protected Exception compile()
		{
			ItemStack filter = pl.getItemInHand(pl.getUsedItemHand());
			Exception e = ScriptItemFilterFactory.compileAndTest(filter);
			return e;
		}
		

	}
	
	public static abstract class ContainerJSFilterEditorBase extends ActuallyUseableContainer implements ISyncable, IGuiSyncronisedContainer
	{
		private boolean syncedWithServer = false;
		protected Player pl;

		private int scriptId = -1;
		private List<String> lines;

		private Supplier<String> completeText;

		private String compileMsg = "";

		public ContainerJSFilterEditorBase(Inventory inv)
		{
			this.pl = inv.player;
		}

		
		protected abstract int getScriptId();
		
		protected abstract Reader getReader() throws IOException;
		
		@Override
		public boolean stillValid(Player playerIn)
		{
			return true;
		}

		@Override
		public void writeAdditional(DataOutputStream buffer) throws IOException
		{
			buffer.writeByte(type_server.ordinal());

			//server

			switch (type_server)
			{
			case ANSWER_TEXT:
				scriptId = getScriptId();
				buffer.writeInt(scriptId);
				Reader r = getReader();
				BufferedReader read = new BufferedReader(r);

				while(read.ready())
				{
					buffer.writeUTF(read.readLine());
				}
				break;
			case ANSWER_COMPILE:
				buffer.writeUTF(getCompileMsg());
				break;
			default:
				break;
			}

		}
		


		@Override
		public void readAdditional(DataInputStream buffer) throws IOException
		{
			type_server = RequestType.values()[buffer.readByte()];

			//client

			switch (type_server)
			{
			case ANSWER_TEXT:
				setSyncedWithServer(true);

				scriptId = buffer.readInt();
				setLines(new LinkedList<String>());
				while(buffer.available()>0)
				{
					getLines().add(buffer.readUTF());
				}
				break;
			case ANSWER_COMPILE:
				setCompileMsg(buffer.readUTF());
				break;
			default:
				break;
			}

		}

		private RequestType type_client, type_server;

		@Override
		public void writeToBuffer(FriendlyByteBuf buf)
		{
			//client
			buf.writeByte(getTypeClient().ordinal());

			switch (getTypeClient())
			{
			case SAVE_TEXT:
			case COMPILE:
				buf.writeInt(scriptId);
				buf.writeUtf(getCompleteText().get());
				break;
			default:
				break;
			}
		}


		@Override
		public void readFromBuffer(FriendlyByteBuf buf)
		{
			//server
			setTypeClient(RequestType.values()[buf.readByte()]);

			switch (getTypeClient())
			{
			case REQUEST_TEXT:
				type_server = RequestType.ANSWER_TEXT;
				FPPacketHandler.sendToClient(this, pl);
				break;
			case SAVE_TEXT:
			case COMPILE:
				int id = buf.readInt();
				String text = buf.readUtf(32767);
				if(id == scriptId)
				{
					
					try
					{
						Writer w = getWriter(scriptId);
						w.write(text);
						w.close();
					}
					catch (FileNotFoundException e)  {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}

					if(getTypeClient()==RequestType.COMPILE)
					{
						Exception e = compile();
						if(e==null)
							setCompileMsg("Compiled Succesfully!");
						else
							setCompileMsg(e.getMessage());

						type_server = RequestType.ANSWER_COMPILE;
						FPPacketHandler.sendToClient(this, pl);
					}
				}
				else
				{
					FPLog.logger.warn("Mismatched Script ids between client(%s) and server(%s)", id, scriptId);
				}
				break;
			default:
				break;
			}
		}
		
		protected abstract Writer getWriter(int scriptId) throws FileNotFoundException;
		
		protected abstract Exception compile();


		public RequestType getTypeClient() {
			return type_client;
		}


		public void setTypeClient(RequestType type_client) {
			this.type_client = type_client;
		}


		public Supplier<String> getCompleteText() {
			return completeText;
		}


		public void setCompleteText(Supplier<String> completeText) {
			this.completeText = completeText;
		}


		public boolean isSyncedWithServer() {
			return syncedWithServer;
		}


		public void setSyncedWithServer(boolean syncedWithServer) {
			this.syncedWithServer = syncedWithServer;
		}


		public String getCompileMsg() {
			return compileMsg;
		}


		public void setCompileMsg(String compileMsg) {
			this.compileMsg = compileMsg;
		}


		public List<String> getLines() {
			return lines;
		}


		public void setLines(List<String> lines) {
			this.lines = lines;
		}
		

	}

	
	
	private enum RequestType
	{
		REQUEST_TEXT,
		SAVE_TEXT,
		COMPILE,
		ANSWER_TEXT,
		ANSWER_COMPILE;
	}


}
