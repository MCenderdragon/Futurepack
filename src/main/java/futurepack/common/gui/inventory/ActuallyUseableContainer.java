package futurepack.common.gui.inventory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import futurepack.api.interfaces.IContainerWithHints;
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.Slot;

public abstract class ActuallyUseableContainer extends AbstractContainerMenu implements IContainerWithHints
{

	public static final String FUEL = "fuel";
	public static final String NEON_BATTERY = "neon_battery";
	public static final String SUPPORT_BATTERY = "support_battery";
	
	
	protected Int2ObjectArrayMap<String> slotHints = new Int2ObjectArrayMap<>();
	
	protected ActuallyUseableContainer() 
	{
		super(null, -1);//will be ovrwritten later
	}
	
	protected void addHint(Slot s, String hint)
	{
		slotHints.put(s.index, "futurepack.slot." + hint);
	}
	
	@Override
	public void addHints(Map<Slot, List<Component>> slot2hints)
	{
		slotHints.forEach((i,s) -> slot2hints.computeIfAbsent(this.getSlot(i), p->new ArrayList<>()).add(new TranslatableComponent(s)));
		
	}
}

