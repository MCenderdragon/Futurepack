package futurepack.common.gui.inventory;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.api.interfaces.tilentity.ITileRenameable;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperRendering;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.block.entity.BlockEntity;

public class GuiRenameWaypoint extends ActuallyUseableContainerScreen
{
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/rename_gui.png");
	
	private EditBox nameField;
	private ITileRenameable rename;
	
	public GuiRenameWaypoint(Player pl, ITileRenameable tile)
	{
		super(new ContainerRenameWaypoint(pl.getInventory(), tile), pl.getInventory(), "gui.rename.waypoint");
		this.imageWidth = 176;
		this.imageHeight = 20;
		rename = tile;
	}

	@Override
	public void init()
	{
		super.init();
		this.minecraft.keyboardHandler.setSendRepeatsToGui(true);
		int i = (this.width - this.imageWidth) / 2;
		int j = (this.height - this.imageHeight) / 2;
		this.nameField = new EditBox(this.font, i + 19, j + 6, 151, 8, new TranslatableComponent("gui.reanme.waypoint.textbox"));
		this.nameField.setTextColor(10198015);
		this.nameField.setTextColorUneditable(0);
		this.nameField.setBordered(false);
		this.nameField.setMaxLength(30);
		this.nameField.setValue(rename.getName().getContents());
	}
	
	@Override
	public boolean keyPressed(int keycode, int p_keyPressed_2_, int p_keyPressed_3_) 
	{
		if(this.nameField.keyPressed(keycode, p_keyPressed_2_, p_keyPressed_3_))
		{
			rename.setName(nameField.getValue());
			FPPacketHandler.syncWithServer(container());
			return true;
		}
		else if(this.nameField.isFocused())
		{
			return true;
		}
		return super.keyPressed(keycode, p_keyPressed_2_, p_keyPressed_3_);
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int mouseButton)
	{
		this.nameField.mouseClicked(mouseX, mouseY, mouseButton);
		if(mouseButton == 0)
		{
			int k = (this.width - this.imageWidth) / 2;
			int l = (this.height - this.imageHeight) / 2;
			if(HelperComponent.isInBox(mouseX-k, mouseY-l, 6, 6, 14, 14))
			{
				this.minecraft.player.closeContainer();
				return true;
			}
		}
		return super.mouseClicked(mouseX, mouseY, mouseButton);
	}

	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
	{
		this.renderBackground(matrixStack);
		super.render(matrixStack, mouseX, mouseY, partialTicks);
		HelperRendering.disableLighting();
		GlStateManager._disableBlend();
		this.nameField.render(matrixStack, mouseX, mouseY, partialTicks);
		this.renderTooltip(matrixStack, mouseX, mouseY);
	}
	
	@Override
	public boolean charTyped(char symbol, int key_code) 
	{
		if(this.nameField.charTyped(symbol, key_code))
		{
			rename.setName(nameField.getValue());
			FPPacketHandler.syncWithServer(container());
			return true;
		}
		return super.charTyped(symbol, key_code);
	}
	
	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		RenderSystem.setShaderTexture(0, res);
		this.blit(matrixStack, leftPos, topPos, 0, 20, imageWidth, imageHeight);
		
	}
	
	public ContainerRenameWaypoint container()
	{
		return (ContainerRenameWaypoint) this.getMenu();
	}
	
	public static class ContainerRenameWaypoint extends ActuallyUseableContainer implements IGuiSyncronisedContainer
	{
		private ITileRenameable entity;
		
		public ContainerRenameWaypoint(Inventory pl, ITileRenameable tile)
		{
			entity = tile;
		}
		
		@Override
		public boolean stillValid(Player playerIn)
		{
			return HelperResearch.isUseable(playerIn, (BlockEntity) entity);
		}

		@Override
		public void writeToBuffer(FriendlyByteBuf buf)
		{
			buf.writeUtf(entity.getName().getString(), 255);
		}

		@Override
		public void readFromBuffer(FriendlyByteBuf buf)
		{
			entity.setName(buf.readUtf(255));
		}
		
	}


	
}
