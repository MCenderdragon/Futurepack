package futurepack.common.block.logistic;

import futurepack.common.FPTileEntitys;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityWireRedstone extends TileEntityWireBase 
{

	public TileEntityWireRedstone(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.WIRE_REDSTONE, pos, state);
	}

	@Override
	public int getMaxEnergy() 
	{
		return 500;
	}

}
