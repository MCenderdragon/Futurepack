package futurepack.common.block.logistic;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.CapabilitySupport;
import futurepack.api.capabilities.ISupportStorage;
import futurepack.common.FPTileEntitys;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityWireSupport extends TileEntityWireBase 
{
	public SupportWire support = new SupportWire();
	private int AI=0;
	private LazyOptional<ISupportStorage>[] supportOpt;
	
	public TileEntityWireSupport(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.WIRE_SUPPORT, pos, state);
	}
	
	@Override
	public int getMaxEnergy() 
	{
		return 500;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void configureLogistic(LogisticStorage log) 
	{
		supportOpt = new LazyOptional[6];
		super.configureLogistic(log);
		
		log.setDefaut(EnumLogisticIO.INOUT, EnumLogisticType.SUPPORT);
		log.removeState(EnumLogisticIO.IN, EnumLogisticType.SUPPORT);
		log.removeState(EnumLogisticIO.OUT, EnumLogisticType.SUPPORT);
	}
	
	@Override
	protected EnumLogisticType[] getTypes() 
	{
		return new EnumLogisticType[]{EnumLogisticType.ENERGIE, EnumLogisticType.SUPPORT};
	}
	
	@Override
	protected void onLogisticChange(Direction face, EnumLogisticType type)
	{
		if(type == EnumLogisticType.SUPPORT)
		{
			if(supportOpt[face.get3DDataValue()]!=null)
			{
				supportOpt[face.get3DDataValue()].invalidate();
				supportOpt[face.get3DDataValue()] = null;
			}
		}
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.put("support", support.serializeNBT());

		return nbt;
	}
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		support.deserializeNBT(nbt.getCompound("support"));
	}


	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(facing==null)
			return super.getCapability(capability, facing);
		
		if(capability == CapabilitySupport.cap_SUPPORT)
		{
			return HelperEnergyTransfer.getSupportCap(supportOpt, facing, this::getLogisticStorage, ()->support);
		}
		
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(supportOpt);
		super.setRemoved();
	}
	
	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState state)
	{
		super.tickServer(pLevel, pPos,state);

		if ( (this.AI>0) != state.getValue(BlockWireSupport.POWERED))
		{
			this.level.setBlockAndUpdate(worldPosition, state.setValue(BlockWireSupport.POWERED, this.AI>0));
		}
		if(this.AI>0)
		{
			AI--;
		}
		
		if( support.get() >= support.getMax() )
		{
			HelperEnergyTransfer.sendSupportPoints(this);
		}
	}
	
	public class SupportWire extends CapabilitySupport
	{
		public SupportWire()
		{
			super(1, EnumEnergyMode.WIRE, TileEntityWireSupport.this::setChanged);
		}
		
		@Override
		public int add(int added)
		{
			int d =  super.add(added);
			if(super.get() > 0)
			{
				HelperEnergyTransfer.sendSupportPoints(TileEntityWireSupport.this);
			}
			return d;
		}
		
		@Override
		public boolean canTransferTo(ISupportStorage other)
		{
			if(other.getType() == EnumEnergyMode.WIRE)
				return true;
			return super.canTransferTo(other);
		}
		
		@Override
		public void support()
		{
			AI = 10;
		}
		
		@Override
		public EnumEnergyMode getType() 
		{
			return super.getType();
		}
	}
}
