package futurepack.common.block.logistic;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.ILogisticInterface;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityPipeNeon extends TileEntityPipeBase 
{
	
	@SuppressWarnings("unchecked")
	public TileEntityPipeNeon(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.PIPE_NEON, pos, state);
		neonOpt = new LazyOptional[6];
		logOpt = new LazyOptional[6];
	}

	public NeonWire power = new NeonWire();
	private LazyOptional<INeonEnergyStorage>[] neonOpt;
	private LazyOptional<ILogisticInterface>[] logOpt;
		
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.put("energy", power.serializeNBT());
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		power.deserializeNBT(nbt.getCompound("energy"));
	}
	
	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState)
	{
		super.tickServer(pLevel, pPos, pState);
		
		if( (power.get()>= power.getMax() || (System.currentTimeMillis()%1000)/50 == 0))
		{
			HelperEnergyTransfer.powerLowestBlock(this);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(facing == null)
			return LazyOptional.empty();
		
		if(capability == CapabilityNeon.cap_NEON)
		{
			if(neonOpt[facing.get3DDataValue()]!=null)
			{
				return (LazyOptional<T>) neonOpt[facing.get3DDataValue()];
			}
			else
			{
				boolean neon = false;
				if(isSideLocked(facing))
				{
					if(isIgnoreLockSub(facing))
					{
						neon = true;
					}
					else
					{
						return LazyOptional.empty(); 
					}
				}
				else
					neon = true;
				
				if(neon)
				{
					neonOpt[facing.get3DDataValue()] = LazyOptional.of(() -> this.power);
					neonOpt[facing.get3DDataValue()].addListener(p -> neonOpt[facing.get3DDataValue()]= null);
					return (LazyOptional<T>) neonOpt[facing.get3DDataValue()];
				}
			}
		}
		else if(capability == CapabilityLogistic.cap_LOGISTIC)
		{
			if(logOpt[facing.get3DDataValue()]!=null)
			{
				return (LazyOptional<T>) logOpt[facing.get3DDataValue()];
			}
			else
			{
				 logOpt[facing.get3DDataValue()] =  LazyOptional.of(() -> new LogisticWrapper(facing));
				 logOpt[facing.get3DDataValue()].addListener(p -> logOpt[facing.get3DDataValue()] = null);
				 return (LazyOptional<T>) logOpt[facing.get3DDataValue()];
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(neonOpt);
		HelperEnergyTransfer.invalidateCaps(logOpt);
		super.setRemoved();
	}
	
	
	@Override
	public void toggelIgnoreLockSub(Direction side)
	{
		super.toggelIgnoreLockSub(side);
		if(neonOpt[side.get3DDataValue()]!=null)
		{
			neonOpt[side.get3DDataValue()].invalidate();
			neonOpt[side.get3DDataValue()] = null;
		}
	}
	
	@Override
	public void toggelLock(Direction side)
	{
		super.toggelLock(side);
		if(neonOpt[side.get3DDataValue()]!=null)
		{
			neonOpt[side.get3DDataValue()].invalidate();
			neonOpt[side.get3DDataValue()] = null;
		}
	}
	
	
	
	public class NeonWire extends CapabilityNeon
	{
		public NeonWire()
		{
			super(500, EnumEnergyMode.WIRE, TileEntityPipeNeon.this::setChanged);
		}
		
		@Override
		public int add(int added)
		{
			int a = super.add(added);
			if(super.get() > 0)
			{
				HelperEnergyTransfer.powerLowestBlock(TileEntityPipeNeon.this);
			}
			return a;
		}
		
		@Override
		public EnumEnergyMode getType()
		{
			BlockState state = level.getBlockState(worldPosition);
			if(state.getBlock()==Blocks.AIR)
			{
				//yes this can happen
				return EnumEnergyMode.NONE;
			}
			return EnumEnergyMode.WIRE;
		}
		
		@Override
		public boolean canTransferTo(INeonEnergyStorage other)
		{
			if(other.getType() == EnumEnergyMode.WIRE)
				return true;
			return super.canTransferTo(other);
		}
	}
	
	public class LogisticWrapper extends TileEntityPipeBase.LogisticWrapper
	{

		public LogisticWrapper(Direction face)
		{
			super(face);
		}
		
		@Override
		public EnumLogisticIO getMode(EnumLogisticType mode)
		{
			if(mode == EnumLogisticType.ENERGIE)
			{
				if(isSideLocked(face))
				{
					if(isIgnoreLockSub(face))
					{
						return EnumLogisticIO.INOUT;
					}
					else
					{
						return EnumLogisticIO.NONE;
					}
				}
				else
				{
					return EnumLogisticIO.INOUT;
				}
			}
			return super.getMode(mode);
		}

		@Override
		public boolean setMode(EnumLogisticIO log, EnumLogisticType mode)
		{
			if(mode == EnumLogisticType.ENERGIE)
			{
				if(isSideLocked(face))
				{
					if(log == EnumLogisticIO.INOUT && !isIgnoreLockSub(face))
					{
						toggelIgnoreLockSub(face);
						return true;
					}
					else if(log == EnumLogisticIO.NONE && isIgnoreLockSub(face))
					{
						toggelIgnoreLockSub(face);
						return true;
					}
				}
			}
			return super.setMode(log, mode);
		}

		@Override
		public boolean isTypeSupported(EnumLogisticType type)
		{
			return type==EnumLogisticType.ENERGIE || super.isTypeSupported(type);
		}
	}
}
