package futurepack.common.block.logistic;

import futurepack.common.FPTileEntitys;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityPipeNormal extends TileEntityPipeBase 
{

	public TileEntityPipeNormal(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.PIPE_NORMAL, pos, state);
	}

}
