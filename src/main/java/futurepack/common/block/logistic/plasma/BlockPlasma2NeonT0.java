package futurepack.common.block.logistic.plasma;

import java.util.Random;
import java.util.function.Supplier;

import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RedstoneLampBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.BlockHitResult;

public class BlockPlasma2NeonT0<T extends BlockEntity & ITileServerTickable> extends BlockPlasmaConverter<T>
{
	public static final BooleanProperty LIT = RedstoneLampBlock.LIT;
	

	public BlockPlasma2NeonT0(Properties properties, Supplier<BlockEntityType<T>> sup)
	{
		super(properties, sup);
	}

	@Override
	public InteractionResult use(BlockState state, Level worldIn, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.SOLAR_PANEL.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}
	
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) 
	{
		super.createBlockStateDefinition(builder);
		builder.add(LIT);
	}
	
	@Override
	public void tick(BlockState state, ServerLevel w, BlockPos pos, Random rand)
	{
		super.tick(state, w, pos, rand);
		w.setBlockAndUpdate(pos, state.setValue(LIT, ((TileEntityPlasma2NeonT0)w.getBlockEntity(pos)).plasma.getFluidAmount()>0));
	}
}
