package futurepack.common.block.logistic.light;

import com.google.common.base.Predicate;

import futurepack.api.capabilities.CapabilityOpticalBlock;
import futurepack.api.capabilities.IOpticalBlock;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.api.laser.LightProperties;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.Mth;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LightLayer;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityPrisma extends FPTileEntityBase implements ITileServerTickable
{
	public static final int CAPACITY = Integer.MAX_VALUE;
	protected LazyOptional<IOpticalBlock>[] opticalOpt;
	public int maxRange;
	protected int range = 0;
	protected CapabilityOpticalBlock lightStorage = new CapabilityOpticalBlock(CAPACITY);
	
	public int rayLength = 0;
	
	public int lastColor, time, lastLength;
	
	public TileEntityPrisma(BlockEntityType<?> pType, BlockPos pWorldPosition, BlockState pBlockState, int maxRange) 
	{
		super(pType, pWorldPosition, pBlockState);
		this.maxRange = maxRange;
		
		opticalOpt = new LazyOptional[6];
	}
	
	protected LazyOptional<IOpticalBlock> target = LazyOptional.empty();

	
	public static TileEntityPrisma range16(BlockPos pWorldPosition, BlockState pBlockState)
	{
		return new TileEntityPrisma(FPTileEntitys.PRISMA16, pWorldPosition, pBlockState, 16);
	}
	
	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState) 
	{
		Direction face = pState.getValue(BlockRotateableTile.FACING);
		
		range++;
		
		BlockPos other = pPos.relative(face, range);		
		
		BlockEntity tile = pLevel.getBlockEntity(other);
		LazyOptional<IOpticalBlock> t = LazyOptional.empty();
		if(tile!=null && (t = tile.getCapability(CapabilityOpticalBlock.cap_OPTICAL, face.getOpposite())).isPresent())
		{
			target = t;	
			rayLength = range;
			range = 0;
			
		}
		else if(pLevel.getBlockState(other).isViewBlocking(level, other))
		{
			target = LazyOptional.empty();
			rayLength = range;
			range = 0;
		}
			
		target.ifPresent(this::transferLight);
		
		if(!target.isPresent())
			lightStorage.remove(lightStorage.getLights().withIntensity(lightStorage.getLights().intensity() * 0.5), false);
		
		if(range >= maxRange)
		{
			rayLength = range;
			range = 0;
		}
		
		if(time<=0 && (lastColor != lightStorage.getLights().getRenderColorRGBA()) || lastLength != rayLength)
		{
			time=20;
			lastColor = lightStorage.getLights().getRenderColorRGBA();
			lastLength = rayLength;
			sendDataUpdatePackage(20);
		}
		if(time>0)
			time--;
	}
	
	public void transferLight(IOpticalBlock target)
	{
		double lux;
		if((lux = lightStorage.getLights().intensity()) > 1)
		{
			LightProperties ray = lightStorage.remove(lightStorage.getLights().withIntensity(lux * 0.5), true);//fake ray of 50% intensity
			
			LightProperties send = target.add(ray, false);//actually add it to target
			
			lightStorage.remove(send, false);//rmeove what was added
			
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(facing == null)
			return LazyOptional.empty();
		
		if(capability == CapabilityOpticalBlock.cap_OPTICAL)
		{
			if(opticalOpt[facing.get3DDataValue()] != null)
			{
				return (LazyOptional<T>) opticalOpt[facing.get3DDataValue()];
			}
			else
			{
				opticalOpt[facing.get3DDataValue()] = LazyOptional.of(() -> lightStorage);
				opticalOpt[facing.get3DDataValue()].addListener(p -> opticalOpt[facing.get3DDataValue()]=null);
				return (LazyOptional<T>) opticalOpt[facing.get3DDataValue()];
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(opticalOpt);
		super.setRemoved();
	}
	
	@Override
	public void writeDataSynced(CompoundTag pTag) 
	{
		super.writeDataSynced(pTag);
		pTag.put("light", lightStorage.serializeNBT());
		pTag.putInt("rayLength", rayLength);
	}
	
	@Override
	public void readDataSynced(CompoundTag nbt) 
	{
		super.readDataSynced(nbt);
		lightStorage.deserializeNBT(nbt.getCompound("light"));
		rayLength = nbt.getInt("rayLength");
	}

	
	public LightProperties getBeam()
	{
		return lightStorage.getLights();
	}
	
	@Override
	public AABB getRenderBoundingBox() 
	{
		AABB bb = new AABB(worldPosition, worldPosition.offset(1, 1, 1));
		bb = bb.expandTowards( Vec3.atCenterOf(worldPosition.relative(getBlockState().getValue(BlockRotateableTile.FACING), rayLength)));
		return bb;
	}
}
