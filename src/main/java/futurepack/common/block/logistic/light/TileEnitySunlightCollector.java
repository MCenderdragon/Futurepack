package futurepack.common.block.logistic.light;

import futurepack.api.laser.LightProperties;
import futurepack.common.FPTileEntitys;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LightLayer;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class TileEnitySunlightCollector extends TileEntityPrisma 
{

	public TileEnitySunlightCollector(BlockEntityType<?> pType, BlockPos pWorldPosition, BlockState pBlockState, int maxRange) 
	{
		super(pType, pWorldPosition, pBlockState, maxRange);
	}
	
	public TileEnitySunlightCollector(BlockPos pWorldPosition, BlockState pBlockState) 
	{
		this(FPTileEntitys.SUNLIGHT_COLLECTOR, pWorldPosition, pBlockState, 8);
	}

	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState) 
	{
		super.tickServer(pLevel, pPos, pState);
		
		if(pLevel.isDay())
		{
			double maxIntensity = 1361;
			
			double i = (pLevel.getBrightness(LightLayer.SKY, pPos.above()) - pLevel.getSkyDarken()) / 15;
			double f = pLevel.getSunAngle(1.0F);
			if (i > 0)
			{
				float f1 = f < (float)Math.PI ? 0.0F : ((float)Math.PI * 2F);
				f += (f1 - f) * 0.2F;
				i = i * Math.cos(f);
			}
			
			maxIntensity *= i;
			
			lightStorage.add(new LightProperties(0xe1e5b2, 0x7FCC, maxIntensity), false);
		}
	}
}
