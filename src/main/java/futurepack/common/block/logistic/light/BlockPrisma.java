package futurepack.common.block.logistic.light;

import java.util.function.Supplier;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockPrisma<T extends TileEntityPrisma> extends BlockRotateableTile implements IBlockServerOnlyTickingEntity<T>
{

	protected final Supplier<BlockEntityType<T>> sup;
	
	public BlockPrisma(Block.Properties props, Supplier<BlockEntityType<T>> sup) 
	{
		super(props);
		this.sup = sup;
	}
	
	@Override
	public BlockEntityType<T> getTileEntityType(BlockState pState) 
	{
		return sup.get();
	}


	
}
