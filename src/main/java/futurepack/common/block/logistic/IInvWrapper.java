package futurepack.common.block.logistic;

import futurepack.api.FacingUtil;
import net.minecraft.core.Direction;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;

public class IInvWrapper implements WorldlyContainer
{
	IItemHandler[] handlers = new IItemHandler[6];
	private int size;
	
	public IInvWrapper(ICapabilityProvider cap)
	{
		this(cap, 0);
		for(IItemHandler handler : handlers)
		{
			if(handler!=null)
			{
				size += handler.getSlots();
			}
		}
	}
	
	public IInvWrapper(ICapabilityProvider cap, int size)
	{
		for(Direction face : FacingUtil.VALUES)
		{
			LazyOptional<IItemHandler> handler = cap.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, face);
			if(handler.isPresent())
			{
				handlers[face.ordinal()] = handler.orElseThrow(NullPointerException::new);
			}
		}
		this.size = size;
	}
	
	public IInvWrapper(IItemHandler...handlers)
	{
		this.handlers = handlers;
		for(IItemHandler handler : handlers)
		{
			if(handler!=null)
			{
				size += handler.getSlots();
			}
		}
	}

	@Override
	public int getContainerSize()
	{	
		return size;
	}
	
	@Override
	public ItemStack getItem(int index)
	{
		for(IItemHandler handler : handlers)
		{
			if(handler!=null)
			{
				if(index>=handler.getSlots())
				{
					index-= handler.getSlots();
				}
				else
				{
					return handler.getStackInSlot(index);
				}
			}
		}
		return null;
	}

	@Override
	public ItemStack removeItem(int index, int count)
	{
		for(IItemHandler handler : handlers)
		{
			if(handler!=null)
			{
				if(index>=handler.getSlots())
				{
					index-= handler.getSlots();
				}
				else
				{
					return handler.extractItem(index, count, false);
				}
			}
		}
		return null;
	}

	@Override
	public ItemStack removeItemNoUpdate(int index)
	{
		for(IItemHandler handler : handlers)
		{
			if(handler!=null)
			{
				if(index>=handler.getSlots())
				{
					index-= handler.getSlots();
				}
				else
				{
					ItemStack item = handler.getStackInSlot(index);
					
					if(handler instanceof IItemHandlerModifiable)
					{
						((IItemHandlerModifiable) handler).setStackInSlot(index, null);
					}
						
					return item;
				}
			}
		}
		return null;
	}

	@Override
	public void setItem(int index, ItemStack stack)
	{
		for(IItemHandler handler : handlers)
		{
			if(handler!=null)
			{
				if(index>=handler.getSlots())
				{
					index-= handler.getSlots();
				}
				else
				{	
					if(handler instanceof IItemHandlerModifiable)
					{
						((IItemHandlerModifiable) handler).setStackInSlot(index, stack);
					}
						
				}
			}
		}
	}

	@Override
	public int getMaxStackSize()
	{
		return 64;
	}

	@Override
	public void setChanged() { }

	@Override
	public boolean stillValid(Player player)
	{
		return true;
	}
	
	@Override
	public boolean isEmpty()
	{
		return false;
	}

	@Override
	public void startOpen(Player player) { }

	@Override
	public void stopOpen(Player player) { }

	@Override
	public boolean canPlaceItem(int index, ItemStack stack)
	{
		for(IItemHandler handler : handlers)
		{
			if(handler!=null)
			{
				if(index>=handler.getSlots())
				{
					index-= handler.getSlots();
				}
				else
				{
					return handler.insertItem(index, stack, true) != stack;
				}
			}
		}
		return false;
	}

	@Override
	public void clearContent() { }

	@Override
	public int[] getSlotsForFace(Direction side)
	{
		int base = 0;
		for(int i=0;i<side.ordinal();i++)
		{
			if(handlers[i] !=null)
			{
				base += handlers[i].getSlots();
			}
		}
			
		if(handlers[side.ordinal()] !=null)
		{
			int[] slots = new int[handlers[side.ordinal()].getSlots()];
			for(int i=0;i<slots.length;i++)
			{
				slots[i] = base+i;
			}
			return slots;
		}
		return null;
	}

	@Override
	public boolean canPlaceItemThroughFace(int index, ItemStack stack, Direction side)
	{
		int base = 0;
		for(int i=0;i<side.ordinal();i++)
		{
			if(handlers[i] !=null)
			{
				base += handlers[i].getSlots();
			}
		}
		
		if(handlers[side.ordinal()] !=null)
		{
			return handlers[side.ordinal()].insertItem(index-base, stack, true) != stack;
		}
		return false;
	}

	@Override
	public boolean canTakeItemThroughFace(int index, ItemStack stack, Direction side)
	{
		int base = 0;
		for(int i=0;i<side.ordinal();i++)
		{
			if(handlers[i] !=null)
			{
				base += handlers[i].getSlots();
			}
		}
		
		if(handlers[side.ordinal()] !=null)
		{
			return handlers[side.ordinal()].extractItem(index-base, 64, true) != null;
		}
		return false;
	}
}
