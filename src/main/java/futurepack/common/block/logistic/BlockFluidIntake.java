package futurepack.common.block.logistic;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockHoldingTile;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockFluidIntake extends BlockHoldingTile implements IBlockServerOnlyTickingEntity<TileEntityFluidIntake>
{

	public BlockFluidIntake(Block.Properties props)
	{
		super(props);
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_maschiens);
	}


	@Override
	public BlockEntityType<TileEntityFluidIntake> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.FLUID_INTAKE;
	}
	
}
