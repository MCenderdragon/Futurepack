package futurepack.common.block.modification;

import java.util.function.Supplier;

import futurepack.api.interfaces.IBlockBothSidesTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockExternalCore extends BlockRotateableTile implements IBlockBothSidesTickingEntity<TileEntityExternalCore>
{
	private static final Supplier<double[][]> shape = () -> new double[][]{
		{0,0,0,16,3,16},
		{2,3,1,14,16,15},
	};
	public static final VoxelShape SHAPE_UP = HelperBoundingBoxes.createBlockShape(shape.get(), 0, 0);
	private final VoxelShape SHAPE_DOWN = HelperBoundingBoxes.createBlockShape(shape.get(), 180F, 0F);
	private final VoxelShape SHAPE_NORTH = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 0F);
	private final VoxelShape SHAPE_SOUTH = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 180F);
	private final VoxelShape SHAPE_WEST = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 90F);
	private final VoxelShape SHAPE_EAST = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 270F);
	
	
	
	public BlockExternalCore(Block.Properties props)
	{
		super(props);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) 
	{
		switch(state.getValue(FACING))
		{
		case DOWN:
			return SHAPE_DOWN;
		case NORTH:
			return SHAPE_NORTH;
		case SOUTH:
			return SHAPE_SOUTH;
		case WEST:
			return SHAPE_WEST;
		case EAST:
			return SHAPE_EAST;
		case UP:
		default:
			return SHAPE_UP;
		}
	}

	@Override
	public BlockEntityType<TileEntityExternalCore> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.EXTERNAL_CORE;
	}
}
