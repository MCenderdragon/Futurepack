package futurepack.common.block.modification.machines;

import java.util.function.Supplier;

import futurepack.api.Constants;
import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.IFluidTankInfo;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.inventory.ItemHandlerGuiOverride;
import futurepack.common.block.inventory.ItemStackHandlerGuis;
import futurepack.common.block.logistic.LogisticFluidWrapper;
import futurepack.common.fluids.FPFluids;
import futurepack.common.modification.EnumChipType;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.Connection;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.tags.FluidTags;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.AbstractFurnaceBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.material.Fluids;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.*;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.templates.FluidTank;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.ForgeRegistry;

public class TileEntityGasTurbine extends TileEntityAbstractMachine implements ITilePropertyStorage 
{
	private static final Supplier<Fluid> getBiogas = () -> FPFluids.biogasFluidStill;
	
	private LogisticStorage storage;
	private BioGasTank gas;
	private ItemContainer handler;
	
	private final LazyOptional<IFluidHandler>[] fluidHandlerOpt;
	
	private int speed;
	private boolean working; //For GUI
	
	@SuppressWarnings("unchecked")
	public TileEntityGasTurbine(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.GAS_TURBINE, pos, state);
		storage = new LogisticStorage(this, this::onLogisticChange, EnumLogisticType.ITEMS, EnumLogisticType.FLUIDS, EnumLogisticType.ENERGIE);
		storage.setDefaut(EnumLogisticIO.INOUT, EnumLogisticType.ITEMS);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.FLUIDS);
		storage.setDefaut(EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.FLUIDS);
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.FLUIDS);
		
		gas = new BioGasTank(FluidAttributes.BUCKET_VOLUME*8);
		
		handler = new ItemContainer();
		
		speed = 1;
		
		fluidHandlerOpt = new LazyOptional[6];
	}
	
	@Override
	protected void onLogisticChange(Direction face, EnumLogisticType type)
	{
		super.onLogisticChange(face, type);
		if(type == EnumLogisticType.FLUIDS)
		{
			if(fluidHandlerOpt[face.get3DDataValue()]!=null)
			{
				fluidHandlerOpt[face.get3DDataValue()].invalidate();
				fluidHandlerOpt[face.get3DDataValue()] = null;
			}
		}
	}
	
	@Override
	public IItemHandler getItemHandler(Direction face)
	{
		return handler;
	}
	
	@Override
	public LogisticStorage getLogisticStorage() 
	{
		return storage;
	}
	
	@Override
	public EnumEnergyMode getEnergyType() 
	{
		return EnumEnergyMode.PRODUCE;
	}
	
	
	
	@Override
	public void updateTile(int ticks) 
	{
		boolean lastWorking = working;
		boolean hasChanged = false;

		if(!level.isClientSide)
		{
			working = false;
			
			if(this.speed <= 0)
			{
				this.speed = 1;
			}
			
			int speed = this.speed * ticks;

			if(gas.getFluidAmount() >= speed)
			{
				float e = (getDefaultPowerUsage() / 10.0f) * speed;
				if(this.energy.get() + (int)e <= this.energy.getMax())
				{
					this.energy.add((int)e);
					gas.drainInternal(speed, true);
					working = true;
					hasChanged = true;
					if(level.random.nextInt(100) < 10)
					{
						this.speed = this.speed < 10 ? this.speed + 1 : this.speed;
						updateData();
					}
				}
				else
				{
					if(level.random.nextInt(100) < 20)
					{
						this.speed = this.speed > 1 ? this.speed - 1 : this.speed;
						updateData();
					}
				}
			}
			else
			{
				if(level.random.nextInt(100) < 20)
				{
					this.speed = this.speed > 1 ? this.speed - 1 : this.speed;
					updateData();
				}
			}
		}

		if (lastWorking != working) {
			BlockState newState = getBlockState().setValue(BlockGasTurbine.ACTIVATED, Boolean.valueOf(working));
			getLevel().setBlock(getBlockPos(), newState, 3);
			hasChanged = true;
		}

		if(hasChanged) {
			setChanged();
		}
	}
	
	@Override
	public void updateNaturally()
	{
		if(!level.isClientSide)
		{
			if(!handler.getStackInSlot(0).isEmpty())
			{
				FluidActionResult res = FluidUtil.tryEmptyContainer(handler.getStackInSlot(0), gas, 8000, null, true);
				if(res.isSuccess())
				{
					handler.setStackInSlot(0, res.getResult());
				}
			}
		}
	}
	
	
	@Override
	public int getDefaultPowerUsage() 
	{
		return (int) (20 * (2+getChipPower(EnumChipType.INDUSTRIE)));
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		gas.readFromNBT(nbt.getCompound("gas"));
		storage.read(nbt);
		handler.deserializeNBT(nbt.getCompound("items"));
		speed = nbt.getInt("speed");
		working = nbt.getBoolean("working");
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		CompoundTag tag = new CompoundTag();
		gas.writeToNBT(tag);
		nbt.put("gas", tag);
	
		storage.write(nbt);
		
		CompoundTag items = handler.serializeNBT();
		nbt.put("items", items);
		
		nbt.putInt("speed", speed);
		
		nbt.putBoolean("working", working);

		return nbt;
	}
	
	
	@Override
	public int getProperty(int id) 
	{
		switch (id)
		{
		case 0:
			return getCurrentFluidId(gas);
		case 1:
			return gas.getFluidAmount();
		case 2:
			return energy.get();
		case 3:
			return working ? speed : 0;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			Fluid fl = getSendFluid(value);
			if(fl!=null)
				gas.setFluid(new FluidStack(fl, Math.max(1, gas.getFluidAmount())));
			break;
		case 1:
			if(gas.getFluid()!=null && !gas.getFluid().isEmpty())
			{
				gas.getFluid().setAmount(value);
			}
			break;
		case 2:
			energy.set(value);
			break;
		case 3:
			speed = value;
		default:
			break;
		}
	}

	private int getCurrentFluidId(IFluidTank tank)
	{
		if(tank.getFluid()==null)
			return -1;

		ForgeRegistry<Fluid> reg = (ForgeRegistry<Fluid>) ForgeRegistries.FLUIDS;
		Fluid fl = tank.getFluid().getFluid();
		return reg.getID(fl);
	}

	public static Fluid getSendFluid(int id)
	{
		if(id==-1)
			return FluidStack.EMPTY.getFluid();

		ForgeRegistry<Fluid> reg = (ForgeRegistry<Fluid>) ForgeRegistries.FLUIDS;
		return reg.getValue(id);
	}

	@Override
	public int getPropertyCount() 
	{
		return 4;
	}

	@Override
	public boolean isWorking() 
	{
		return true;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(facing==null)
			return super.getCapability(capability, facing);
		
		if(capability==CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY)
		{
			if(fluidHandlerOpt[facing.get3DDataValue()] != null)
			{
				return (LazyOptional<T>) fluidHandlerOpt[facing.get3DDataValue()];
			}
			else
			{
				if(getLogisticStorage().getModeForFace(facing, EnumLogisticType.FLUIDS) == EnumLogisticIO.NONE)
				{
					LazyOptional.empty();
				}
				else
				{
					fluidHandlerOpt[facing.get3DDataValue()] = LazyOptional.of(() -> new LogisticFluidWrapper(getLogisticStorage().getInterfaceforSide(facing), gas));
					fluidHandlerOpt[facing.get3DDataValue()].addListener(p -> fluidHandlerOpt[facing.get3DDataValue()]=null);
					return (LazyOptional<T>) fluidHandlerOpt[facing.get3DDataValue()];
				}
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(fluidHandlerOpt);
		super.setRemoved();
	}

	private class ItemContainer extends ItemStackHandlerGuis
	{

		public ItemContainer()
		{
			super(1, TileEntityGasTurbine.this::setChanged);
		}
		
		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate, boolean gui)
		{
			validateSlotIndex(slot);
			
			if(isItemValid(slot, stack, gui))		
				return super.insertItem(slot, stack, simulate, gui);
			
			return stack;
		}
		
		
		@Override
		public boolean isItemValid(int slot, ItemStack stack, boolean gui)
		{
			if(slot == 0)
	    	{
				return FluidUtil.tryEmptyContainer(stack, new BioGasTank(8000), 8000, null, false).isSuccess();
	    	}
			
			return false;
		}
		
		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate, boolean gui)
		{
			validateSlotIndex(slot);
			
			if(slot==0)
			{
				if(gui || !isItemValid(slot, getStackInSlot(0), gui))
					return super.extractItem(slot, amount, simulate, gui);
			}
			return ItemStack.EMPTY;
		}
		
		@Override
		public int getSlotLimit(int slot)
		{
			return 1;
		}
		
	}

	public IItemHandler getGui()
	{
		return new ItemHandlerGuiOverride(handler);
	}

	
	class BioGasTank extends FluidTank
	{

		public BioGasTank(int capacity) 
		{
			super(capacity);
		}
		
		public void drainInternal(int amountDrained, boolean execute) 
		{
			super.drain(amountDrained, execute ? FluidAction.EXECUTE : FluidAction.SIMULATE);
		}

		@Override
	    public boolean isFluidValid(FluidStack fluid)
	    {

	        return fluid.getFluid().is(FPFluids.tagGasTurbineFuel);
	    }
		
		@Override
		public FluidStack drain(FluidStack resource, FluidAction action) 
		{
			return FluidStack.EMPTY;
		}
		
		@Override
		public FluidStack drain(int maxDrain, FluidAction action) 
		{
			return FluidStack.EMPTY;
		}
	}

	public IFluidTankInfo getGas() 
	{
		return new IFluidTankInfo.FluidTankInfo(gas, 0);
	}
	
	public int getSpeed()
	{
		return speed;
	}

	

	
	@Override
	public void onDataPacket(Connection net, ClientboundBlockEntityDataPacket pkt)
	{
		super.onDataPacket(net, pkt);
//		world.HelperChunks.renderUpdate(w, pos);(pos, pos);
		level.markAndNotifyBlock(worldPosition, level.getChunkAt(getBlockPos()), getBlockState(), getBlockState(), 3, Constants.RECURSIVE_BLOCKUPDATES);
	}
	

	
	private boolean last_state = false;
	
	public void updateData()
	{
		boolean now = working;
		if(now != last_state)
		{
			last_state = now;
		
			FPPacketHandler.sendTileEntityPacketToAllClients(this);
		}


		//markDirty();
	}
	
	public boolean isWorkingClient()
	{
		return working;
	}
	
}
