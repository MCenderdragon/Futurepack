package futurepack.common.block.modification.machines;

import futurepack.api.interfaces.IBlockBothSidesTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateable;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class BlockImproveComponents extends BlockRotateable implements IBlockBothSidesTickingEntity<TileEntityImproveComponents>
{

	public BlockImproveComponents(Block.Properties props)
	{
		super(props);
	}	
	
	@Override
	public InteractionResult use(BlockState state, Level worldIn, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(worldIn.isClientSide)
			return InteractionResult.SUCCESS;

		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.IMPROVE_COMPONENTS.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}

	@Override
	public BlockEntityType<TileEntityImproveComponents> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.IMPROVE_COMPONENTS;
	}
}
