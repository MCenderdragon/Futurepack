package futurepack.common.block.modification.machines;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.CapabilitySupport;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.IItemSupport;
import futurepack.api.interfaces.IRecyclerRecipe;
import futurepack.api.interfaces.IRecyclerTool;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileRecycler;
import futurepack.common.FPTileEntitys;
import futurepack.common.item.ComputerItems;
import futurepack.common.item.ItemChip;
import futurepack.common.item.ItemCore;
import futurepack.common.item.ItemRam;
import futurepack.common.modification.EnumChipType;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperInventory;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityRecycler extends TileEntityMachineSupport implements WorldlyContainer, ITilePropertyStorage, ITileRecycler
{
	private int progress=0;
	private int maxprogress=0;
	private final RecyclerRecipeCache cache = new RecyclerRecipeCache();
//	private ArrayList<ItemStack> rest = null;
	private Random rand = new Random();
	
	private EnumEnergyMode supportMode = EnumEnergyMode.NONE;
	
	/*Init*/
	public TileEntityRecycler(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.RECYCLER, f -> new SupportStorage(f), pos, state);
		((SupportStorage)support).rec = this;
	}
	
	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ITEMS);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
		storage.setDefaut(EnumLogisticIO.OUT, EnumLogisticType.SUPPORT);
		
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		
		storage.setModeForFace(Direction.DOWN, EnumLogisticIO.OUT, EnumLogisticType.ITEMS);
	}
	
	@Override
	protected EnumLogisticType[] getLogisticTypes()
	{
		return new EnumLogisticType[]{EnumLogisticType.ENERGIE, EnumLogisticType.ITEMS, EnumLogisticType.SUPPORT};
	}
	
	@Override
	public void updateTile(int ticks) 
	{
		
		if(!level.isClientSide)
		{
			if(!items.get(1).isEmpty() && items.get(1).getItem() instanceof IRecyclerTool)
			{
				IRecyclerTool tool = (IRecyclerTool)(items.get(1).getItem());
						
				handleSupport(tool.getSupportMode());
						
				if(!items.get(0).isEmpty())
				{
					//check / update chache
					if(!checkCache(tool, items.get(0)))
					{
						cache.recipe = tool.updateRecipe(this, items.get(1), items.get(0));
						
						if(cache.recipe != null)
						{
							maxprogress = tool.getMaxProgress(this, cache.recipe, items.get(1), items.get(0));
						}		
					}
								
					if(cache.recipe != null)
					{

						if(tool.canWork(this, cache.recipe, items.get(1), items.get(0), ticks))
						{
									
							if(progress >= maxprogress)
							{		
								
								if(mergeOutput(Arrays.asList(cache.recipe.getMaxOutput()), true))
								{
									
									ArrayList<ItemStack> its = tool.getOutput(this, cache.recipe, items.get(1), items.get(0), rand);
									mergeOutput(its, false);

									//update tool
									boolean flag = tool.craftComplete(this, cache.recipe, items.get(1), items.get(0));
											
									if(items.get(1).getCount() <= 0)
									{
										items.set(1, ItemStack.EMPTY);
									}
										
									//update Input
									if(flag)
									{
										items.get(0).shrink(1);
											
										if(items.get(0).isEmpty())
										{
											items.set(0, ItemStack.EMPTY);
										}
									}
									
									progress = 0;
								}
							}	
							else
							{
								tool.tick(this, cache.recipe, items.get(1), items.get(0));
								progress = Math.min(progress + ticks, maxprogress);
							}
						}				
					}
					else
					{							
						progress = 0;
					}	
				}
				else
				{
					progress = 0;
					cache.recipe = null;
					cache.in = ItemStack.EMPTY;
				}
			}
			else
			{	
				supportMode = EnumEnergyMode.NONE;
				progress = 0;
				cache.recipe = null;
				cache.in = ItemStack.EMPTY;
				cache.tool = null;
				
			}	
			
			if(getBlockState().getValue(BlockRecycler.ACTIVATED) != progress>0)
			{
				level.setBlockAndUpdate(worldPosition, getBlockState().setValue(BlockRecycler.ACTIVATED, progress>0));
				setChanged();
			}
		}		
	}
	
	private boolean checkCache(IRecyclerTool tool, ItemStack in) 
	{
		if(cache.tool != tool || !HelperInventory.areItemsEqualNoSize(cache.in, in))
		{
			cache.tool = tool;
			cache.in = in;
			cache.recipe = null;		
			return false;
		}		
		return true;
	}

	private void handleSupport(EnumLogisticIO support_mode)
	{
		if(support_mode == EnumLogisticIO.IN)
		{
			supportMode = EnumEnergyMode.USE;
			for(int i = 11; i <=12; i++)
			{
				if(!items.get(i).isEmpty() && items.get(i).getItem() instanceof IItemSupport)
				{
					IItemSupport ch = (IItemSupport) items.get(i).getItem();
					for(int k=0;k<29;k++)
					{
						if(ch.getSupport(items.get(i)) > 0 && support.get() < support.getMax())
						{
							support.add(1);
							ch.addSupport(items.get(i), -1);
						}
						else
						{
							break;
						}
					}
				}
			}
		}
		else if(support_mode == EnumLogisticIO.OUT)
		{
			for(int i = 11; i <=12; i++)
			{
				if(!items.get(i).isEmpty() && items.get(i).getItem() instanceof IItemSupport)
				{
					IItemSupport ch = (IItemSupport) items.get(i).getItem();
					for(int k=0;k<29;k++)
					{
						if(ch.isSupportable(items.get(i)) && support.get() > 0 && ch.getSupport(items.get(i)) < ch.getMaxSupport(items.get(i)))
						{
							ch.addSupport(items.get(i), 1);
							support.use(1);
						}		
						else
						{
							break;
						}
					}
				}
			}
			supportMode = EnumEnergyMode.PRODUCE;
			if(this.getChipPower(EnumChipType.SUPPORT)>0)
			{
				HelperEnergyTransfer.sendSupportPoints(this);
			}
		}	
	}
	
	
	private boolean mergeOutput(List<ItemStack> its, boolean simulate)
	{
//		ArrayList<ItemStack> arr = null;
		if(its != null)
		{
			for(int j = 0; j < its.size(); j++)
			{
				int count = its.get(j).getCount();
				for(int i = 2; i < 11; i++)
				{
					if(items.get(i).isEmpty())
					{
						if(!simulate)
						{
							items.set(i, its.get(j).copy());
							its.get(j).setCount(0);
						}
						count = 0;
						break;
					}
					else if(HelperInventory.canStack(items.get(i), its.get(j)))
					{
						if(items.get(i).getCount() + count <= its.get(j).getMaxStackSize())
						{
							if(!simulate)
							{
								items.get(i).grow(its.get(j).getCount());
								its.get(j).setCount(0);
							}
							count = 0;
							break;
						}
						else
						{
							int used = its.get(j).getMaxStackSize() - items.get(i).getCount();
							if(!simulate)
							{
								items.get(i).grow(used);
								its.get(j).shrink(used);
							}
							count -= used;
						}
					}	
				}
				
				if(count > 0)
				{
					return false;
				}
					
			}
		}
		return true;
	}
	
	/* Inventory */
	
	@Override
	public int[] getSlotsForFace(Direction side)
	{
		return new int[]{0,1,2,3,4,5,6,7,8,9,10,11,12};
	}

	@Override
	public boolean canPlaceItemThroughFace(int index, ItemStack item, Direction direction)
	{
		if(!storage.canInsert(direction, EnumLogisticType.ITEMS))
		{
			return false;
		}
		
		if(index < 2 || index > 10)
		{
			return canPlaceItem(index, item);
		}

		return false;
	}
	
	@Override
	public boolean canPlaceItem(int index, ItemStack item) 
	{
		if(index == 0)
		{
			if(!items.get(1).isEmpty() && items.get(1).getItem() instanceof IRecyclerTool)
			{
				IRecyclerTool ir = (IRecyclerTool) items.get(1).getItem();
				return (ir.updateRecipe(this, items.get(1), item) != null);
			}
			else
			{
				return false;
			}
		}
		
		if(index == 1)
		{
			return (item.getItem() instanceof IRecyclerTool);
		}
		
		if(index >= 11)
		{
			return (item.getItem() instanceof IItemSupport) && (((IItemSupport)item.getItem()).isSupportable(item)); 
		}
		
		return false;
	}

	@Override
	public boolean canTakeItemThroughFace(int index, ItemStack stack, Direction direction)
	{
		if(!storage.canExtract(direction, EnumLogisticType.ITEMS))
		{
			return false;
		}
		
		if(index >= 2 && index <= 10)
		{
			return true;
		}
		
		if(index >= 11)
		{
			return (stack.getItem() instanceof IItemSupport) && !(((IItemSupport)stack.getItem()).isSupportable(stack)); 
		}
		
		return false;
	}

	@Override
	protected int getInventorySize() 
	{
		return 13;
	}
	

	/** GENERIC **/
	

	
	/* NEON */
	@Override
	public EnumEnergyMode getEnergyType() 
	{
		return EnumEnergyMode.USE;
	}
	
	/* NBT */

	
	@Override
	public void writeDataSynced(CompoundTag nbt)
	{
		super.writeDataSynced(nbt);
		nbt.putInt("progress", progress);
	}
	
	@Override
	public void readDataSynced(CompoundTag nbt)
	{
		super.readDataSynced(nbt);
		progress = nbt.getInt("progress");
	}
	
	/* Property */

	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return energy.get();
		case 1:
			return progress;
		case 2:
			return support.get();
		case 3:
			return maxprogress;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		//TODO
		switch (id)
		{
		case 0:
			setEnergy(value);
			break;
		case 1:
			progress = value;
			break;
		case 2:
			support.set(value);
			break;
		case 3:
			maxprogress = value;
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 4;
	}

	/**/
	@Override
	public boolean isWorking() 
	{
		return !(progress == 0 || progress >= maxprogress);
	}

	public int getProgress() 
	{
		return progress;
	}

	public float getMaxProgress() 
	{
		return maxprogress;
	}
	
	public static ItemStack getUntoastedItemStack(ItemStack in)
	{
		ItemStack conv = null;
		
		if(in.getItem() == ComputerItems.toasted_chip)
		{
			conv = ItemChip.getFromToasted(in);
		}
		else if(in.getItem() == ComputerItems.toasted_ram)
		{
			conv = ItemRam.getFromToasted(in);
		}
		else if(in.getItem() == ComputerItems.toasted_core)
		{
			conv = ItemCore.getFromToasted(in);
		}
		
		if(conv == null)
		{
			conv = in;
		}	
		
		return conv;
	}
	
	public static class RecyclerRecipeCache
	{
		public IRecyclerRecipe recipe; //nullable
		public IRecyclerTool tool;
		public ItemStack in;
		
		RecyclerRecipeCache()
		{
			recipe = null;
			tool = null;
			in = ItemStack.EMPTY;
		}
		
	}

	@Override
	public void setChanged()
	{
		super.setChanged();
	}
	
	/* SUPPORT */
	
	public static class SupportStorage extends CapabilitySupport
	{
		TileEntityRecycler rec;
		
		public SupportStorage(TileEntityMachineSupport tile)
		{
			super(512, EnumEnergyMode.PRODUCE, tile::setChanged);
		}
		
		@Override
		public EnumEnergyMode getType()
		{
			return rec.supportMode;
		}
	}
	
}
