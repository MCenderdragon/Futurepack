package futurepack.common.block.misc;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.CapProviderRFSupport;
import futurepack.common.FPTileEntitys;
import futurepack.common.FuturepackMain;
import futurepack.common.block.BlockRotateable;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.potions.FPPotions;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperEntities;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LightningBolt;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.monster.Creeper;
import net.minecraft.world.level.Explosion.BlockInteraction;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

public class TileEntityRFtoNEConverter extends FPTileEntityBase implements ITilePropertyStorage, ITileServerTickable
{
	public static final int PROPERTY_CONV = 1;
	public static final int PROPERTY_WORKING = 2;
	private CapabilityNeon power = new CapabilityNeon(10000, EnumEnergyMode.PRODUCE, this::setChanged);
	private EnumConversation conv = EnumConversation.S5;
	private boolean working = false;
	
	private List<WeakReference<Creeper>> creeper = null;
	
	public TileEntityRFtoNEConverter(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.RF2NE_CONVERTER, pos, state);
	}
	
	// 4RF = 1NE
	@Override
	public void tickServer(Level level, BlockPos pos, BlockState pState)
	{
		if(working && conv.rate>=100)
		{
			if(level.random.nextInt(10) == 0)
			{
				List<Creeper> l = getActiveCreeper();
				for(Creeper c : l)
				{
					if(HelperEntities.navigateEntity(c, pos, false))
					{
						break;
					}
				}
			}
		}
		if(power.get() > 0)
		{
			HelperEnergyTransfer.powerLowestBlock(this);
		}
	}
	
	@Nullable
	private List<Creeper> getActiveCreeper()
	{
		if(creeper == null || creeper.isEmpty())
		{
			AABB bb = new AABB(worldPosition.offset(-50, -50, -50), worldPosition.offset(50, 50, 50));
			Iterator<Creeper> iter =level.getEntitiesOfClass(Creeper.class, bb, new Predicate<Creeper>()
			{
				@Override
				public boolean test(Creeper c) 
				{
					if(c.isAlive() && !c.isPowered()) //isCharged = isPowered
					{
						return c.getNavigation().isDone();
					}
					return false;
				}
			}).iterator();
			creeper = new ArrayList<>(16);
			ArrayList<Creeper> list = new ArrayList<>(16);
			while(iter.hasNext())
			{
				Creeper c = iter.next();
				if(c!=null)
				{
					creeper.add(new WeakReference<Creeper>(c));
					list.add(c);
				}
			}
			return list;
		}
		else
		{
			ArrayList<Creeper> list = new ArrayList<>(creeper.size());
			Iterator<WeakReference<Creeper>> iter = creeper.iterator();
			while(iter.hasNext())
			{
				WeakReference<Creeper> ref = iter.next();
				Creeper c = ref.get();
				if(ref.get()==null || ref.get().isAlive()==false || ref.get().isPowered())
					iter.remove();
				else
					list.add(c);
					
			}
			return list;
		}
	}

	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.put("energy", power.serializeNBT());
		nbt.putByte("speed", (byte)conv.ordinal());
		nbt.putBoolean("working", working);
		
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		power.deserializeNBT(nbt.getCompound("energy"));
		conv = EnumConversation.values()[nbt.getByte("speed")];
		working = nbt.getBoolean("working");
	}
	
	private LazyOptional<IEnergyStorage> optRF;
	private LazyOptional<INeonEnergyStorage> optNeon; 
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityEnergy.ENERGY && facing == getRFInput())
		{
			if(optRF!=null)
			{
				return (LazyOptional<T>) optRF;
			}
			else
			{
				optRF = LazyOptional.of(() -> new SaveableStorage(this));
				optRF.addListener(p -> optRF = null);
				return (LazyOptional<T>) optRF;
			}
		}			
		if(capability == CapabilityNeon.cap_NEON)
		{
			if(optNeon!=null)
			{
				return (LazyOptional<T>) optNeon;
			}
			else
			{
				optNeon = LazyOptional.of(() -> power);
				optNeon.addListener(p -> optNeon = null);
				return (LazyOptional<T>) optNeon;
			}
		}
		return super.getCapability(capability, facing);
	}

	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(optNeon, optRF);
		super.setRemoved();
	}
	
	
	private void doDamage(int transfer)
	{
		EnumConversation.getCoversationFromNeon(transfer).applyEffect(this);
	}
	
	public  EnumConversation getConv()
	{
		return conv;
	}
	
	public Direction getRFInput()
	{
		BlockState state = level.getBlockState(worldPosition);
		if(state.isAir())
			return Direction.UP;
		
		return state.getValue(BlockRotateable.HORIZONTAL_FACING);
	}
	
	private static class SaveableStorage extends CapProviderRFSupport.RFNeonWrapper
	{
		private final TileEntityRFtoNEConverter parent;
		
		SaveableStorage(TileEntityRFtoNEConverter parent) 
		{
			super(parent.power, () -> parent.conv.rate, () -> parent.working);
			this.parent = parent;
		}

		@Override
		public int receiveEnergy(int maxReceive, boolean simulate)
		{
			int t = super.receiveEnergy(maxReceive, simulate);
			if(!simulate && t>= 100)
			{
				parent.doDamage(t);
			}
			return t;
		}
	}

	@Override
	public int getProperty(int id)
	{
		switch(id)
		{
		case 0:
			return power.get();
		case PROPERTY_CONV: 
			return conv.ordinal();
		case PROPERTY_WORKING:
			return working ? 1 : 0;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch(id)
		{
		case 0:
			int d = value - power.get();
			if(d>0)
				power.add(d);
			else
				power.use(-d);
			break;
		case PROPERTY_CONV: 
			conv = EnumConversation.values()[value];
			break;
		case PROPERTY_WORKING:
			working = value == 1;
			break;
		default:
			break;
		}
	}


	@Override
	public int getPropertyCount()
	{
		return 3;
	}
	
	public static enum EnumConversation
	{
		S1(10),
		S2(25),
		S3(50),
		S4(75),
		S5(100),//Charged Creeper r=8
		S6(250),//Paralized, r=16
		S7(500),//Instand Damage, r=16
		S8(750),//Blitze, r=24
		S9(1000);//explode, mit block damage, 
		
		private final int rate;
		public final int xPos, yPos;
		
		EnumConversation(int rate)
		{
			this.rate = rate;
			xPos = 122 + (ordinal()/3) * 41;
			yPos = 0 + (ordinal()%3) * 21;
		}
		
		public static EnumConversation getCoversationFromNeon(int transfer)
		{
			EnumConversation[] ec = EnumConversation.values();
			int i=0;
			while(ec[i].rate < transfer)
				i++;
			
			return ec[i];
		}
		
		public void applyEffect(TileEntityRFtoNEConverter tile)
		{
			List<LivingEntity> list2 = null;
			List<Creeper> list1;
			switch(this)
			{
			case S9:
				if(tile.level.random.nextInt(10 * 60 * 20) == 0)
				{
					tile.level.explode(null, tile.worldPosition.getX(), tile.worldPosition.getY(), tile.worldPosition.getZ(), 16F, true, BlockInteraction.BREAK);
					return;
				}
			case S8:
				list2 = tile.level.getEntitiesOfClass(LivingEntity.class, new AABB(tile.worldPosition.offset(-24, -24, -24), tile.worldPosition.offset(24, 24, 24)));
				list2.forEach(e -> {
					if(e.level.random.nextInt(40)==0)
					{
						LightningBolt bolt = EntityType.LIGHTNING_BOLT.create(e.level);
						bolt.setPos(e.getX(), e.getY(), e.getZ());
						bolt.setVisualOnly(false);
						e.level.addFreshEntity(bolt);
					}
						
				});
			case S7:
				if(list2==null)
					list2 = tile.level.getEntitiesOfClass(LivingEntity.class, new AABB(tile.worldPosition.offset(-16, -16, -16), tile.worldPosition.offset(16, 16, 16)));
				list2.forEach(e -> {
					if(e.level.random.nextInt(20)==0)
						e.hurt(FuturepackMain.NEON_DAMAGE, 1);
				});
			case S6:
				if(list2==null)
					list2 = tile.level.getEntitiesOfClass(LivingEntity.class, new AABB(tile.worldPosition.offset(-16, -16, -16), tile.worldPosition.offset(16, 16, 16)));
				list2.forEach(e -> {
					if(e.level.random.nextInt(10)==0)
						e.addEffect(new MobEffectInstance(FPPotions.PARALYZED, 20*2));
				});	
			case S5:
				list1 = tile.level.getEntitiesOfClass(Creeper.class, new AABB(tile.worldPosition.offset(-8, -8, -8), tile.worldPosition.offset(8, 8, 8)), e -> {return !e.isPowered();});
				list1.forEach(e -> {
					CompoundTag nbt = new CompoundTag();
					e.addAdditionalSaveData(nbt);
					nbt.putBoolean("powered", true);
					e.readAdditionalSaveData(nbt);
				});
			default:
				break;
			}
		}
		
		public int getLimit()
		{
			return rate;
		}
	}

	public boolean isWorking()
	{
		return working;
	}
}
