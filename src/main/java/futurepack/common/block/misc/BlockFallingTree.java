package futurepack.common.block.misc;

import futurepack.api.interfaces.IBlockBothSidesTickingEntity;
import futurepack.common.FPTileEntitys;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockFallingTree extends Block implements IBlockBothSidesTickingEntity<TileEntityFallingTree>
{

	public BlockFallingTree(Block.Properties props)
	{
		super(props);
//		super(Material.WOOD);
	}

//	@Override
//	public boolean isFullBlock(IBlockState state)
//	{
//		return false;
//	}
//	
//	@Override
//	public boolean isOpaqueCube(IBlockState state)
//	{
//		return false;
//	}
	
	@Override
	public RenderShape getRenderShape(BlockState state)
	{
		return RenderShape.ENTITYBLOCK_ANIMATED;
	}
	
	@Override
	public void onRemove(BlockState state, Level world, BlockPos pos, BlockState newState, boolean isMoving) 
	{
		super.onRemove(state, world, pos, newState, isMoving);
		if(newState.getBlock()!=this)
		{
			TileEntityFallingTree tree = (TileEntityFallingTree) world.getBlockEntity(pos);
			if(tree!=null)
			{
				tree.onBlockBreak();
			}
		}
	}

	@Override
	public BlockEntityType<TileEntityFallingTree> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.FALLING_TREE;
	}
}
