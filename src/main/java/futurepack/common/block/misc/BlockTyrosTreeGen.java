package futurepack.common.block.misc;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockTyrosTreeGen extends Block implements IBlockServerOnlyTickingEntity<TileEntityTyrosTreeGen>
{

	public BlockTyrosTreeGen() 
	{
		super(Block.Properties.copy(Blocks.BEDROCK).noCollission().randomTicks().noDrops().lightLevel(state -> 15));
	}

	@Override
	public BlockEntityType<TileEntityTyrosTreeGen> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.TYROS_TREE_GEN;
	}
}
