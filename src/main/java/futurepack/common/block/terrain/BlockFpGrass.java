package futurepack.common.block.terrain;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.GrassBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.PlantType;

public class BlockFpGrass extends GrassBlock
{
	public BlockFpGrass(Block.Properties properties)
	{
		super(properties);
	}
	
	@Override
	public boolean canSustainPlant(BlockState state, BlockGetter world, BlockPos pos, Direction facing, IPlantable plantable)
	{
		PlantType plantType = plantable.getPlantType(world, pos.relative(facing));
		//fast accept
		if(plantType == PlantType.PLAINS)
		{
			return true;
		}
		return super.canSustainPlant(state, world, pos, facing, plantable);
	}
	
}
