package futurepack.common.block.terrain;

import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.level.block.OreBlock;

public class BlockErze extends OreBlock
{
	public BlockErze(Properties builder)
	{
		super(builder, UniformInt.of(2, 5));
	}
}
