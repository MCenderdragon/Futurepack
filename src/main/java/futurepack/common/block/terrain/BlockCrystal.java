package futurepack.common.block.terrain;

import java.util.Random;
import java.util.function.Predicate;

import com.mojang.math.Vector3f;

import futurepack.common.FuturepackMain;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import futurepack.extensions.albedo.LightList;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.DustParticleOptions;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class BlockCrystal extends Block
{
	public static final EnumProperty<EnumGrowthState> GROWTH = EnumProperty.create("growth", EnumGrowthState.class);
	private final Predicate<BlockState> canStay;
	private final MobEffect effect;
	private final float r,g,b;
	private final DustParticleOptions crystalParticle;

	protected static final VoxelShape CRYSTAL_SMALL = Block.box(2.0D, 0.0D, 2.0D, 14.0D, 13.0D, 14.0D);
	protected static final VoxelShape CRYSTAL_BOTTOM = Block.box(1.5D, 0.0D, 1.5D, 14.5D, 16.0D, 14.5D);
	protected static final VoxelShape CRYSTAL_TOP = Block.box(5.0D, 0.0D, 5.0D, 11.0D, 13.0D, 11.0D);

	protected BlockCrystal(Properties props, Predicate<BlockState> canStay, int color)
	{
		this(props, canStay, color, null);
	}
	
	protected BlockCrystal(Properties props, Predicate<BlockState> canStay, int color, MobEffect effect)
	{
		super(props.noCollission().randomTicks());
		this.canStay = canStay;
		this.effect = effect;
		b = ( (color & 0x0000FF ) >> 0 ) / 255F;
		g = ( (color & 0x00FF00 ) >> 8 ) / 255F;
		r = ( (color & 0xFF0000 ) >> 16) / 255F;
		crystalParticle = new DustParticleOptions(new Vector3f(r + 0.01F, g + 0.01F, b + 0.01F), 0.5F);
	}

	@Override
	@Nonnull
	@SuppressWarnings(value = { "deprecation" })
	public VoxelShape getShape(BlockState pState, @NotNull BlockGetter pLevel, @NotNull BlockPos pPos, @NotNull CollisionContext pContext) {
		return switch (pState.getValue(GROWTH)) {
			case SMALL -> CRYSTAL_SMALL;
			case BOTTOM -> CRYSTAL_BOTTOM;
			case TOP -> CRYSTAL_TOP;
		};
	}

	@Nullable
	@Override
	public BlockPathTypes getAiPathNodeType(BlockState state, BlockGetter level, BlockPos pos, @Nullable Mob entity)
	{
		return BlockPathTypes.DAMAGE_OTHER;
	}
	
	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(GROWTH);
	}

	@Nullable
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext pContext) {
		//Disallow placement in air etc. (not on valid block)
		if(!this.canStay.test(pContext.getLevel().getBlockState(pContext.getClickedPos().below())))
			return null;
		return this.defaultBlockState();
	}

	//@Override
	public boolean canBlockStay(LevelReader w, BlockPos pos, BlockState state)
	{
		BlockState stateUp = w.getBlockState(pos.above());
		BlockState stateDown = w.getBlockState(pos.below());
		
		if(state.getValue(GROWTH) == EnumGrowthState.BOTTOM)
		{
			if(stateUp.getBlock() != this)
				return false;
			if(stateUp.getValue(GROWTH) != EnumGrowthState.TOP)
				return false;
		}
		if(stateDown.getBlock() == this)
		{
			return state.getValue(GROWTH) == EnumGrowthState.TOP && stateDown.getValue(GROWTH) == EnumGrowthState.BOTTOM;
		}
		else if(state.getValue(GROWTH) == EnumGrowthState.SMALL || state.getValue(GROWTH) == EnumGrowthState.BOTTOM)
		{
			if(HelperBoundingBoxes.isSideSolid(w, pos.below(), Direction.UP))
			{
				return canStay.test(stateDown);
			}
		}				
		return false;
	}
	
	@Override
	public void tick(BlockState state, ServerLevel w, BlockPos pos, Random r)
	{
		neighborChanged(state, w, pos, Blocks.AIR, null, false);
		if(r.nextInt(20)==0 && !w.isClientSide)
		{					
			if(w.isEmptyBlock(pos.above()) && state.getValue(GROWTH) == EnumGrowthState.SMALL)
			{
				w.setBlock(pos, state.setValue(GROWTH, EnumGrowthState.BOTTOM), 2);
				w.setBlock(pos.above(), state.setValue(GROWTH, EnumGrowthState.TOP), 3);
			}	
		}
	}
	
	@SuppressWarnings(value = { "deprecation" })
	@Override
	public void neighborChanged(BlockState state, Level w, BlockPos pos, Block bl, BlockPos nBlock, boolean isMoving)
	{
		if(!canBlockStay(w, pos, state) && !w.isClientSide)
		{
			w.destroyBlock(pos, true);
		}
		super.neighborChanged(state, w, pos, bl, nBlock, isMoving);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onPlace(BlockState state, Level w, BlockPos pos, BlockState oldState, boolean isMoving)
	{
		if(oldState.getBlock()!=this)
			neighborChanged(state, w, pos, Blocks.AIR, null, isMoving);
		
		super.onPlace(state, w, pos, oldState, isMoving);
	}
	
	@Override
	public void entityInside(BlockState state, Level worldIn, BlockPos pos, Entity entityIn)
	{
		if(entityIn instanceof LivingEntity)
		{
			entityIn.hurt(FuturepackMain.NEON_DAMAGE, 1);
			if(effect != null)
			{
				MobEffectInstance eff = new MobEffectInstance(effect, 20 * 1, 0, false, false);
				((LivingEntity) entityIn).addEffect(eff);
			}
		}
	}
	
	@OnlyIn(Dist.CLIENT)
	@Override
	public void animateTick(BlockState stateIn, Level w, BlockPos pos, Random rand)
	{
		float x = pos.getX() + rand.nextFloat();
		float y = pos.getY() + rand.nextFloat();
		float z = pos.getZ() + rand.nextFloat();
		
		w.addParticle(crystalParticle, x, y, z, 0F, 0F, 0F);
		if(rand.nextFloat() <= 0.02)
			LightList.addStaticLight(x, y, z, r, g, b, 0.75F, 7.0F, 0.5f);
	}
	public enum EnumGrowthState implements StringRepresentable
	{
		SMALL, BOTTOM, TOP;
		
		@Override
		public String getSerializedName()
		{
			return name().toLowerCase();
		}
		
		public int getMeta()
		{
			return ordinal();
		}
		
		public static EnumGrowthState fromMeta(int i)
		{
			EnumGrowthState[] val = values();
			if(i<val.length)
				return values()[i];
			
			return EnumGrowthState.SMALL;
		}
	}
}
