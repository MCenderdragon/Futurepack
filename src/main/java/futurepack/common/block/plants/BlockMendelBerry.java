package futurepack.common.block.plants;

import java.util.Random;

import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.common.item.FoodItems;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.common.PlantType;

public class BlockMendelBerry extends CropBlock
{

	//todo
	private static final VoxelShape[] box = new VoxelShape[] {
			Block.box(6, 0, 6, 10, 3, 10),
			Block.box(4, 0, 4, 12, 5, 12),
			Block.box(4, 0, 4, 12, 6, 12),
			Block.box(3, 0, 3, 13, 10, 13),
			Block.box(3, 0, 3, 13, 15, 13),
			Block.box(3, 0, 3, 13, 16, 13),
			Block.box(3, 0, 3, 13, 16, 13),
			Block.box(3, 0, 3, 13, 16, 13)
			};

	public BlockMendelBerry(Block.Properties props)
	{
		super(props);
	}

	@Override
	public void onRemove(BlockState state, Level w, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if(newState.isAir())
		{
			//Block.dropResources(state, w, pos);

			boolean canStay = canSurvive(state, w, pos);

			int age = getAge(state);
			if(age == 5)
			{
				Block.dropResources(state, w, pos);
				w.setBlock(pos, state.setValue(AGE, 2), 2);
				//if(canStay)
				//	dropBlockAsItem(w, pos, state, 0);
			}
			else if (age >= getMaxAge())
			{
				Block.dropResources(state, w, pos);
				w.setBlock(pos, state.setValue(AGE, 2), 2);
				//if(canStay)
				//	dropBlockAsItem(w, pos, state, 0);
			}
			//ground is gone - full destroy
			if(!canStay)
			{
				//add an Unraw drop
				//if(age == 5 || age >= 7)
				//	dropBlockAsItem(w, pos, state.withProperty(AGE, 2), 0);

				w.removeBlock(pos, false);
			}
		}
		else
		{
			super.onRemove(state, w, pos, newState, isMoving);
		}
	}

	@Override
	public PlantType getPlantType(BlockGetter world, BlockPos pos)
	{
		return PlantType.DESERT;
	}

	@Override
	protected boolean mayPlaceOn(BlockState state, BlockGetter worldIn, BlockPos pos)
	{
		return super.mayPlaceOn(state, worldIn, pos) || state.getBlock() == TerrainBlocks.dirt_m.get();
	}

	@Override
	protected ItemLike getBaseSeedId()
	{
		return FoodItems.mendel_seed;
	}


	@Override
	public Item asItem()
	{
		return FoodItems.mendel_seed;
	}



	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext sel)
	{
		return box[getAge(state)];
	}

//	@Override
//	public boolean shouldSideBeRendered(IBlockState blockState, IWorldReader blockAccess, BlockPos pos, EnumFacing side)
//	{
//		return true;
//	}

	private boolean willSeedHarvest(BlockState state)
	{
		 int age = getAge(state);
		 return (age == 5 || age >= getMaxAge());
	}

	@Override
	public void growCrops(Level worldIn, BlockPos pos, BlockState state)
	{
		super.growCrops(worldIn, pos, state);
		state = worldIn.getBlockState(pos);
		purifyNeonSand(worldIn, pos, state);
	}

	@Override
	public void tick(BlockState state, ServerLevel worldIn, BlockPos pos, Random rand)
	{
		super.tick(state, worldIn, pos, rand);
		state = worldIn.getBlockState(pos);
		purifyNeonSand(worldIn, pos, state);
	}

	public void purifyNeonSand(Level w, BlockPos pos, BlockState state)
	{
		if(state.getBlock()!=this)
			return;

		if(getAge(state) < getMaxAge())
		{
			return;
		}

		BlockState down = w.getBlockState(pos.below());
		Block sand = down.getBlock();
		if(sand == TerrainBlocks.sand_alutin.get() || sand == TerrainBlocks.sand_bioterium.get() || sand == TerrainBlocks.sand_glowtite.get() || sand == TerrainBlocks.sand_retium.get())
		{
			w.setBlockAndUpdate(pos.below(), TerrainBlocks.sand_neon.get().defaultBlockState());
			w.removeBlock(pos, false); //this is needed because of the break block mechanic
			w.setBlockAndUpdate(pos, Blocks.DEAD_BUSH.defaultBlockState());
		}
	}
}
