package futurepack.common.block.inventory;

import java.util.Collection;
import java.util.UUID;

import futurepack.api.EnumStateSpaceship;
import futurepack.api.ParentCoords;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.api.interfaces.IPlanet;
import futurepack.api.interfaces.ISelector;
import futurepack.api.interfaces.ISpaceshipUpgrade;
import futurepack.api.interfaces.IStatisticsManager;
import futurepack.api.interfaces.tilentity.ITileBoardComputer;
import futurepack.api.interfaces.tilentity.ITileClientTickable;
import futurepack.api.interfaces.tilentity.ITileFuelProvider;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.api.interfaces.tilentity.ITileWithOwner;
import futurepack.client.particle.ParticlePathSearch;
import futurepack.common.FPBlockSelector;
import futurepack.common.FPConfig;
import futurepack.common.FPLog;
import futurepack.common.FPSounds;
import futurepack.common.FPTileEntitys;
import futurepack.common.SelectInterface;
import futurepack.common.block.BlockRotateable;
import futurepack.common.item.misc.MiscItems;
import futurepack.common.spaceships.FPPlanetRegistry;
import futurepack.common.spaceships.FPSpaceShipSelector;
import futurepack.common.spaceships.moving.SpaceShipSelecterUtil;
import futurepack.depend.api.StableConstants;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.world.dimensions.atmosphere.FullBlockCache;
import net.minecraft.Util;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Registry;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fml.DistExecutor;

public class TileEntityBoardComputer extends TileEntityInventoryBase implements ITilePropertyStorage, ITileWithOwner, WorldlyContainer, ITileBoardComputer, ITileServerTickable, ITileClientTickable
{
	public CapabilityNeon power = new CapabilityNeon(getMaxNE(), EnumEnergyMode.USE, this::setChanged);
	int sy=-1,ey=-1;
	public Component chat;
	
	private int errorCode = 0;
	
	private UUID owner = new UUID(-1, -1);
	private ResourceLocation home = null;

	public Player currentUser;
	private IPlanet willJump = null;
	
	private FPSpaceShipSelector base;
	private ParentCoords air;
	
	protected int thrusterCount, blockCount, fuelCount;
	protected int transportingBlocks;
	private int extraEnergy;
	public float client_jump_progress = 0F;
	private BlockPos moveTarget = null;
	
	private LazyOptional<INeonEnergyStorage> neonOpt;

	public TileEntityBoardComputer(BlockEntityType<?> tileEntityTypeIn, BlockPos pos, BlockState state) 
	{
		super(tileEntityTypeIn, pos, state);
	}
	
	public TileEntityBoardComputer(BlockPos pos, BlockState state) 
	{
		this(FPTileEntitys.BOARD_COMPUTER, pos, state);
	}
	
	public int getMaxNE()
	{
		return 1000;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityNeon.cap_NEON)
		{
			if(neonOpt == null)
			{
				neonOpt = LazyOptional.of(() -> power);
				neonOpt.addListener(p -> neonOpt = null);
			}
			return (LazyOptional<T>) neonOpt;
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(neonOpt);
		super.setRemoved();
	}
	
	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState)
	{
		tick();
	}
	
	@Override
	public void tickClient(Level pLevel, BlockPos pPos, BlockState pState)
	{
		tick();
	}
	
	public void tick()
	{
		if(level.isClientSide && getState(EnumStateSpaceship.open) && isAdvancedBoardComputer())
		{
			if(air==null)
			{
				try
				{
					if(base==null)
					{
						base = SpaceShipSelecterUtil.selectShip(level, worldPosition);
					}
					Direction face = getBlockState().getValue(BlockRotateable.HORIZONTAL_FACING);
					BlockPos xyz = worldPosition.relative(face,-1);
					
					if(!base.isInArea(xyz))
					{
						return;
					}
					FPBlockSelector airSel = new FPBlockSelector(level, new selAir(base));
					airSel.getBlocks(xyz, Material.AIR);
				}
				catch(IllegalArgumentException e)
				{
					//THis mean succses
				}
				
			}
			if(air!=null)
				spawnParticleTypes();
		}
		extraEnergy -= power.add(extraEnergy);
		if(willJump!=null)
		{
			jump();
		}
		else
		{
			if(transportingBlocks < blockCount)
			{
				extraEnergy += transportingBlocks * 10;
				transportingBlocks = 0;
			}
		}
	}
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	private void spawnParticleTypes()
	{
		DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable()
		{
			@Override
			public void run() 
			{
				if(level.random.nextInt(20)==0)
				{
					ParticlePathSearch serach = new ParticlePathSearch((ClientLevel) level, air);
					
					Minecraft.getInstance().particleEngine.add(serach);
				}
			}
		});
	}
	
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		if(home==null)
		{
			home = this.level.dimension().location();
		}
	}
	
	@Override
	public void setOwner(Player pl)
	{
		owner = pl.getGameProfile().getId();
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.put("energy", power.serializeNBT());
		nbt.putInt("sy", sy);
		nbt.putInt("ey", ey);
		nbt.putLong("UUID1", owner.getLeastSignificantBits());
		nbt.putLong("UUID2", owner.getMostSignificantBits());
		if(home!=null)
			nbt.putString("homeDim", home.toString());
		nbt.putInt("poweredBlocks", transportingBlocks);
		if(moveTarget!=null)
			nbt.putIntArray("target", new int[]{moveTarget.getX(),moveTarget.getY(),moveTarget.getZ()});
		
		nbt.putInt("extraEnergy", extraEnergy);
		nbt.putInt("blocks", blockCount);
		nbt.putInt("thrusters", thrusterCount);
		nbt.putInt("fuel", fuelCount);
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		power.deserializeNBT(nbt.getCompound("energy"));
		sy = nbt.getInt("sy");
		ey = nbt.getInt("ey");
		owner = new UUID(nbt.getLong("UUID2"), nbt.getLong("UUID1"));
		if(nbt.contains("homeDim"))
			home = new ResourceLocation(nbt.getString("homeDim"));
		transportingBlocks = nbt.getInt("poweredBlocks");
		if(nbt.contains("target"))
		{
			int[] ints = nbt.getIntArray("target");
			moveTarget = new BlockPos(ints[0],ints[1],ints[2]);
		}
		extraEnergy = nbt.getInt("extraEnergy");
		blockCount = nbt.getInt("blocks");
		thrusterCount = nbt.getInt("thrusters");
		fuelCount = nbt.getInt("fuel");
	}
	

	

	public void setOldPos(int sy, int ey)
	{
		this.sy = sy;
		this.ey = ey;
	}
	
	public int getOldSy()
	{
		return sy;
	}
	
	public int getOldEy() 
	{
		return ey;
	}
	
	public void searchForShip()
	{
		resetState();
		chat = null;
		thrusterCount = -1;
		blockCount = -1;
		fuelCount = -1;

		if(power.get() < power.getMax())
		{
			setState(EnumStateSpaceship.lowEnergie, true);
			float f = power.get() / power.getMax()*1F;
			chat = EnumStateSpaceship.lowEnergie.getChatFormated(f);
			return;
		}
		else
		{
			base = SpaceShipSelecterUtil.selectShip(level, worldPosition);
			ISelector blocks = base.getSelector();
			IStatisticsManager stats = blocks.getStatisticsManager();
			try
			{
				Direction face = getBlockState().getValue(BlockRotateable.HORIZONTAL_FACING);
				BlockPos xyz = worldPosition.relative(face,-1);
				
				thrusterCount = stats.getBlockCountFromPredicate(FPSpaceShipSelector.thrusters);
				blockCount = base.getBlockTotal();
				
				if(!base.isInArea(xyz))
				{
					throw new IllegalArgumentException();
				}						
				FPBlockSelector airSel = new FPBlockSelector(level, new selAir(base));
				airSel.getBlocks(xyz, Material.AIR);
				
				int thrustersStrength = FPConfig.COMMON.thrusterBlockMoveCount.get();
				
				if(thrusterCount*thrustersStrength <blockCount)
				{
					setState(EnumStateSpaceship.missingThruster, true);		
					int t3 = blockCount/thrustersStrength;
					if(blockCount%thrustersStrength>0)
					{
						t3++;
					}
					t3-=thrusterCount;
					
					if(chat==null)
						chat = EnumStateSpaceship.missingThruster.getChatFormated(blockCount,thrusterCount, t3);
//					return;
				}
				int producers = stats.getBlockCountFromPredicate(FPSpaceShipSelector.neon_producer);
				if(producers == 0)
				{
					setState(EnumStateSpaceship.missingEngine, true);
					if(chat==null)
						chat = EnumStateSpaceship.missingEngine.getChatFormated();
				}
				int teleporters = stats.getBlockCountFromPredicate(FPSpaceShipSelector.teleporter);
				if(teleporters<=0)
				{
					setState(EnumStateSpaceship.missingBeam, true);
					if(chat==null)
						chat = EnumStateSpaceship.missingBeam.getChatFormated();
//					return;
				}
				Collection<ParentCoords> col = blocks.getValidBlocks(new SelectInterface(ITileFuelProvider.class));
				if(col.size()<=0)
				{
					setState(EnumStateSpaceship.missingFuel, true);
					if(chat==null)
						chat = EnumStateSpaceship.missingFuel.getChatFormated(thrusterCount);
//					return;
				}
				else
				{
					int triebwerke = thrusterCount;
					fuelCount = 0;
					for(ParentCoords pc : col)
					{
						ITileFuelProvider fuel = (ITileFuelProvider) level.getBlockEntity(pc);
						fuelCount += fuel.getFuel();					
					}
					
					triebwerke -= fuelCount/1000;
					
					if(triebwerke>0)
					{
						setState(EnumStateSpaceship.missingFuel, true);
						if(chat==null)
							chat = EnumStateSpaceship.missingFuel.getChatFormated(triebwerke);
//						return;
					}
				}
			}
			catch(IllegalArgumentException e)
			{
				setState(EnumStateSpaceship.open, true);
				if(chat==null)
					chat = EnumStateSpaceship.open.getChatFormated();
				return;
			}	
		}
		if(errorCode==0)
		{
			setState(EnumStateSpaceship.GO, true);
			if(chat==null)
				chat = EnumStateSpaceship.GO.getChatFormated();
		}
		if(!level.isClientSide)
		{
			BlockPos pos = this.getBlockPos();
			BlockState state = this.getBlockState();
			level.markAndNotifyBlock(pos, level.getChunkAt(pos), state, state, StableConstants.BlockFlags.BLOCK_UPDATE | StableConstants.BlockFlags.NO_RERENDER | StableConstants.BlockFlags.NOTIFY_NEIGHBORS, 128);
		}
	}
	
	
	@Override
	public boolean getState(EnumStateSpaceship state)
	{
		int core = errorCode >> state.ordinal();
		
		return (core & 1) == 1;
	}
	
	public void resetState()
	{
		errorCode = 0;
	}
	
	public void setState(EnumStateSpaceship state, boolean bool)
	{
		errorCode |= (bool ? 1 : 0) << state.ordinal();
	}
	
	@Override
	public boolean isAdvancedBoardComputer()
	{
		return false;
	}


	private class selAir implements IBlockSelector
	{
		FPSpaceShipSelector highMap;
		
		public selAir(FPSpaceShipSelector sel)
		{
			highMap = sel;
		}
		
		@Override
		public boolean isValidBlock(Level w, BlockPos pos, Material m, boolean dia, ParentCoords parent)
		{
			if(dia)
				return false;
			
			if(!highMap.isInArea(pos))
			{
//				WorldServer s = (WorldServer) w;
//				while(parent!=null)
//				{
//					
//					s.spawnParticle(ParticleTypes.NOTE, false, parent.getX()+0.5, parent.getY()+0.5, parent.getZ()+0.5, 1,  0D, 0D, 0D ,0D, 0);
//					parent = parent.getParent();
//				}
				System.out.println("Open at "+pos);
				air = parent;
				throw new IllegalArgumentException();
			}
			else	
			{
				BlockState state = w.getBlockState(pos);
				if(state.getMaterial()==Material.AIR || state.isAir())
				{
					return true;
				}
				else
				{
					return !FullBlockCache.isFullBlock(pos, w);
				}
			}
		}
		
		@Override
		public boolean canContinue(Level w, BlockPos pos, Material m, boolean dia, ParentCoords parent) 
		{
			return true;
		}
	}

	@Override
	public int getProperty(int id)
	{
		switch(id)
		{
		case 0:
			return this.errorCode;
		case 1:
			return power.get();
		case 2:
			
			return (int)(10000 * (float)transportingBlocks/blockCount);
		default:
			return 0;
		}
	}
	
	@Override
	public void setProperty(int id, int value)
	{		
		switch (id)
		{
		case 0:
			errorCode = value;
			break;
		case 1:
			int d = value - power.get();
			if(d>0)
				power.add(d);
			else
				power.use(-d);
			break;
		case 2:
			client_jump_progress = value / 10000F;
		default:
			break;
		}
	}
	
	@Override
	public int getPropertyCount()
	{
		return 3;
	}

	@Override
	public boolean isOwner(Player pl)
	{
		if(owner==null)
			return true;
		return isOwner(pl.getGameProfile().getId());
	}
	
	@Override
	public boolean isOwner(UUID pl)
	{
		if(owner==null)
			return true;
		return this.owner.equals(pl);
	}

	@Override
	public void prepareJump(IPlanet pl, BlockPos target)
	{
		//tile.JUMP((EntityPlayerMP) pl, planet.getDimenion());
		willJump  = pl;
		this.moveTarget = target;
	}
	
	private void jump()
	{
		if(base != null && willJump!=null && !level.isClientSide && getState(EnumStateSpaceship.GO))
		{	
			if(canJump())
			{
				
				ResourceKey<Level> dim = ResourceKey.create(Registry.DIMENSION_REGISTRY, willJump.getDimenionId());
				ResourceKey<Level> current = level.dimension();
				if(current == dim)
				{
					if(moveTarget==null || worldPosition.distSqr(moveTarget)<2)
					{
						power.add(transportingBlocks *10);
						transportingBlocks = 0;
						
						level.playLocalSound(worldPosition.getX(), worldPosition.getY(), worldPosition.getZ(), new SoundEvent( new ResourceLocation("random.fizz")), SoundSource.BLOCKS, 1.1F, 1.75F, true);
					}	
					else
					{
						if(transportingBlocks>=blockCount)
						{
							int fuel = FPSpaceShipSelector.getFuel(worldPosition, moveTarget) * thrusterCount;
							if(fuel<= fuelCount)
							{
								BlockPos finish = moveTarget.subtract(worldPosition.subtract(base.getStart()));
								if(base.isSpaceAvailable(finish))
								{
									fuel = FPSpaceShipSelector.getFuel(worldPosition, moveTarget);
									base.useFuel(fuel);
									transportingBlocks = 0;
									try
									{
										base.moveShip(finish);
										if(currentUser != null)
											currentUser.closeContainer();
									}
									catch (IllegalArgumentException e)
									{
										e.printStackTrace();
										if(currentUser != null)
											currentUser.sendMessage(EnumStateSpaceship.WRONG_COORDS.getChatFormated(), Util.NIL_UUID);
									}
								}
								else
								{
									willJump = null;
									power.add(transportingBlocks *10)	;
									transportingBlocks = 0;
									if(currentUser != null)
										currentUser.sendMessage(EnumStateSpaceship.DESTINATION_OBSTRUCTED.getChatFormated(), Util.NIL_UUID);
									level.playLocalSound(worldPosition.getX(), worldPosition.getY(), worldPosition.getZ(), SoundEvents.GENERIC_HURT, SoundSource.BLOCKS, 1.1F, 1.75F, true);
								}
							}
							else
							{
								level.playLocalSound(worldPosition.getX(), worldPosition.getY(), worldPosition.getZ(), new SoundEvent( new ResourceLocation("random.fizz")), SoundSource.BLOCKS, 1.1F, 1.75F, true);
							}
						}
						else
						{
							while(power.get()>10 && transportingBlocks<blockCount)
							{
								power.use(10);
								transportingBlocks++;
							}
							
							if(transportingBlocks>=blockCount)
							{
								willJump = null; //you must confirm the jump again!
							}
							return;
						}
					}
				}
				else if(getState(EnumStateSpaceship.GO))
				{
					base.setComputerPos(worldPosition);		//setting computer Pos
					
					ServerLevel serv = (ServerLevel) level;
					MinecraftServer server = serv.getServer();
					serv = server.getLevel(dim);					
					
					if(serv == null)
					{
						FPLog.logger.error("Did not find '" + dim + "' (got null for it)");
					}
					else if(base.preloadWorld(serv))
					{
						power.use(power.getMax());	
						base.useFuel(1000);		
						if(items.get(0).getItem() == Items.FILLED_MAP && moveTarget!=null)
						{
							base.transferShips(serv, moveTarget.getX(), moveTarget.getZ());
						}
						else
						{
							base.transferShips(serv);
						}
						level.playLocalSound(worldPosition.getX(),worldPosition.getY(),worldPosition.getZ(), FPSounds.JUMP, SoundSource.BLOCKS, 2.0F, 1.0F, true);
					}
					
				}
			}
			else
			{
				level.playLocalSound(worldPosition.getX(), worldPosition.getY(), worldPosition.getZ(), new SoundEvent( new ResourceLocation("random.fizz")), SoundSource.BLOCKS, 1.1F, 1.75F, true);
			}
		}
		else if(base!=null && !level.isClientSide && willJump!=null && transportingBlocks>0)
		{
			return;	//this prevents reseting of willJump, wich results in abortion if this.			
		}

		willJump = null;
	}
	
	@Override
	public void setHome()
	{
		if(home!=null)
		{
			willJump = FPPlanetRegistry.instance.getPlanetSafe(home);		
		}	
	}
	
	@Override
	public boolean canJump()
	{
		IPlanet thisP = FPPlanetRegistry.instance.getPlanetSafe(level);
		return FPPlanetRegistry.instance.canJump(this, thisP, willJump);
	}

	@Override
	protected int getInventorySize()
	{
		return 1;
	}
	
	@Override
	public boolean isUpgradeinstalled(ISpaceshipUpgrade up)
	{
		if(base==null)
		{
			return false;
		}
		return up.isBoardComputerValid(this) && base.hasUpgrade(up);
	}

	public ISpaceshipUpgrade[] getMissingUpgrades()
	{
		if(base==null)
		{
			return new ISpaceshipUpgrade[0];
		}
		return base.getMissingUpgrades(); 
	}

	
	@Override
	public int[] getSlotsForFace(Direction side)
	{
		return new int[]{};
	}

	@Override
	public boolean canPlaceItemThroughFace(int index, ItemStack itemStackIn, Direction direction)
	{
		return false;
	}

	@Override
	public boolean canTakeItemThroughFace(int index, ItemStack stack, Direction direction)
	{
		return false;
	}
	
	@Override
	public boolean canPlaceItem(int var1, ItemStack var2)
	{
		return var2!=null && (var2.getItem() == MiscItems.spacecoordinats || var2.getItem() == Items.FILLED_MAP);
	}

	public BlockPos getJumpTargetPosition()
	{
		return this.moveTarget;
	}
	
	@Override
	public String getGUITitle() {
		return "gui.futurepack.boardcomputer.title";
	}

	@Override
	public int getThrusterCount() 
	{
		return thrusterCount;
	}

	@Override
	public int getFuelCount() 
	{
		return fuelCount;
	}

	@Override
	public int getBlockCount() 
	{
		return blockCount;
	}

	@Override
	public int getTransportingBlockCount() 
	{
		return transportingBlocks;
	}
	
	@Override
	public BlockPos getComputerPosition() 
	{
		return worldPosition;
	}
}
