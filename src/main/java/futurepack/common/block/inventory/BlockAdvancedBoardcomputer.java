package futurepack.common.block.inventory;

import futurepack.common.FPTileEntitys;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class BlockAdvancedBoardcomputer extends BlockBoardComputer
{


	public BlockAdvancedBoardcomputer(Properties props)
	{
		super(props);
	}

//	@Override
//	public String getMetaName(int meta)
//	{
//		if(meta>=8)
//			meta=2;
//		else if(meta>=4)
//			meta=1;
//		else
//			meta=0;
//		return "advanced_boardcomputer_"+meta;
//	}

	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(w.isClientSide)
		{
			return InteractionResult.SUCCESS;
		}

		TileEntityBoardComputer comp = (TileEntityBoardComputer) w.getBlockEntity(pos);
		comp.searchForShip();

		if(HelperResearch.canOpen(pl, state))
		{
			if(comp.isOwner(pl))
			{
				FPGuiHandler gui = FPGuiHandler.BOARD_COMPUTER;
				if(this == InventoryBlocks.advanced_board_computer_s)
					gui = FPGuiHandler.BOARD_COMPUTER_BLACK;
				else if(this == InventoryBlocks.advanced_board_computer_g)
					gui = FPGuiHandler.BOARD_COMPUTER_LIGHT_GRAY;
				else if(this == InventoryBlocks.advanced_board_computer_w)
					gui = FPGuiHandler.BOARD_COMPUTER_WHITE;
				gui.openGui(pl, pos);
			}
		}
		return InteractionResult.SUCCESS;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public BlockEntityType<TileEntityBoardComputer> getTileEntityType(BlockState pState)
	{
		return (BlockEntityType)FPTileEntitys.ADVANCED_BOARD_COMPUTER;
	}
}
