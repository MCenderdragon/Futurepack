package futurepack.common.block.inventory;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateable;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockFlashServer extends BlockRotateable implements IBlockServerOnlyTickingEntity<TileEntityFlashServer>
{
	public static final BooleanProperty slot1 = BooleanProperty.create("slot1");
	public static final BooleanProperty slot2 = BooleanProperty.create("slot2");
	public static final BooleanProperty slot3 = BooleanProperty.create("slot3");
	public static final BooleanProperty slot4 = BooleanProperty.create("slot4");
	
	public static VoxelShape shapeNorth = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,1,16,16,16}}, 0, 0);
	public static VoxelShape shapeEast = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,1,16,16,16}}, 0, 270);
	public static VoxelShape shapeSouth = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,1,16,16,16}}, 0, 180);
	public static VoxelShape shapeWest = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,1,16,16,16}}, 0, 90);
	
	public BlockFlashServer(Block.Properties props)
	{
		super(props);
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_maschiens);
		registerDefaultState(this.stateDefinition.any().setValue(slot1, false).setValue(slot2, false).setValue(slot3, false).setValue(slot4, false));
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) 
	{
		switch(state.getValue(HORIZONTAL_FACING))
		{
		case NORTH:
			return shapeNorth;
		case EAST:
			return shapeEast;
		case SOUTH:
			return shapeSouth;
		case WEST:
			return shapeWest;
		default:
			return super.getShape(state, worldIn, pos, context);
		}
	}

	
	@Override
	public InteractionResult use(BlockState state, Level worldIn, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.FLASH_SERVER.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(slot1, slot2, slot3, slot4);
	}	
	
	@Override
	public BlockEntityType<TileEntityFlashServer> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.FLASH_SERVER;
	}
}
