package futurepack.common.block.inventory;

import java.util.function.IntConsumer;

import net.minecraftforge.items.ItemStackHandler;

public class ListenableItemStackHandler extends ItemStackHandler
{
	public IntConsumer onChanged;
	
	public ListenableItemStackHandler(int size)
    {
        super(size);
    }
	
	@Override
	protected void onContentsChanged(int slot) 
	{
		onChanged.accept(slot);
		
		super.onContentsChanged(slot);
	}
	
}
