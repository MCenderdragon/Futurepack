package futurepack.common;

import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLDedicatedServerSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;

public class DistExecutorBase 
{
	public void earlySetup()
	{
		
	}
	
	public void postInitClient(FMLClientSetupEvent event)
	{
		System.out.println("FuturepackMain.postInitClient()");
	}
	
	public void postInitServer(FMLDedicatedServerSetupEvent event)
	{
		System.out.println("FuturepackMain.postInitServer()");
	}
	
	public void setupFinished(FMLLoadCompleteEvent event)
	{
		
	}
}
