package futurepack.common.recipes.crafting;

import java.util.UUID;

import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.CraftingContainer;

public class InventoryCraftingForResearch extends CraftingContainer
{
	private Object user;
	
	public InventoryCraftingForResearch(AbstractContainerMenu p_i1807_1_, int width, int height, Player user)
	{
		super(p_i1807_1_, width, height);
		this.user = user;
	}

	public InventoryCraftingForResearch(AbstractContainerMenu p_i1807_1_, int width, int height, UUID user)
	{
		super(p_i1807_1_, width, height);
		this.user = user;
	}
	
	public Object getUser()
	{
		return user;
	}

}
