package futurepack.common.recipes.multiblock;

import futurepack.common.FuturepackTags;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.block.modification.ModifiableBlocks;
import futurepack.common.block.multiblock.BlockDeepCoreMiner;
import futurepack.common.block.multiblock.BlockDeepCoreMiner.EnumDeepCoreMiner;
import futurepack.common.block.multiblock.MultiblockBlocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.pattern.BlockInWorld;
import net.minecraft.world.level.block.state.pattern.BlockPattern;
import net.minecraft.world.level.block.state.pattern.BlockPatternBuilder;
import net.minecraft.world.level.block.state.predicate.BlockMaterialPredicate;
import net.minecraft.world.level.block.state.predicate.BlockStatePredicate;
import net.minecraft.world.level.material.Material;

public class MultiblockPatterns
{	
//	private static boolean isMetalFence(IBlockState state)
//	{
//		return state.getBlock() == FPBlocks.colorIronFence || (state.getBlock() == FPBlocks.metalFence && state.getValue(FPBlocks.META(1)) == 0);
//	}
//	
//	private static boolean isMetalBlock(IBlockState state)
//	{
//		return (state.getBlock() == FPBlocks.erzBlocke && (state.getValue(BlockErzeBlocke.types) == EnumOreBlockTypes.METAL || state.getValue(BlockErzeBlocke.types) == EnumOreBlockTypes.LUFTSCHACHT)) 
//				|| state.getBlock() == FPBlocks.colorIron
//				|| state.getBlock() == FPBlocks.colorLuftung;
//	}
	
	private static boolean isMainBlock(BlockState state)
	{
		return state.getBlock() == MultiblockBlocks.deepcore_miner && state.getValue(BlockDeepCoreMiner.variants) == EnumDeepCoreMiner.maschine;
	}
	
	private static boolean isLaser(BlockState state)
	{
		return state.getBlock() == ModifiableBlocks.entity_eater || state.getBlock() == ModifiableBlocks.entity_healer || state.getBlock() == ModifiableBlocks.entity_killer;
	}
	
	public static BlockPattern getDeepMinerPattern()
	{
		return BlockPatternBuilder.start().aisle("FBC", "FLC", "M~C")
				.where('~', BlockInWorld.hasState(BlockMaterialPredicate.forMaterial(Material.AIR)))
				.where('L', BlockInWorld.hasState(MultiblockPatterns::isLaser))
				.where('C', BlockInWorld.hasState(BlockStatePredicate.forBlock(InventoryBlocks.composite_chest)))
				.where('F', BlockInWorld.hasState(s -> s.is(FuturepackTags.METAL_FENCE)))
				.where('B', BlockInWorld.hasState(s -> s.is(FuturepackTags.METAL_BLOCK)))
				.where('M', BlockInWorld.hasState(MultiblockPatterns::isMainBlock))
				.build();
		
	}
	
//	public static BlockPattern getDeepMinerPatternActive()
//	{
//		return FactoryBlockPattern.start().aisle("FBC", "FLC", "M~C")
//				.where('~', BlockWorldState.hasState(BlockMaterialMatcher.forMaterial(Material.AIR)))
//				.where('L', BlockWorldState.hasState(BlockStateMatcher.forBlock(FPBlocks.entityEater)))
//				.where('C', BlockWorldState.hasState(BlockStateMatcher.forBlock(FPBlocks.composite_chest)))
//				.where('F', BlockWorldState.hasState(MultiblockPatterns::isMetalFence))
//				.where('B', BlockWorldState.hasState(MultiblockPatterns::isMetalBlock))
//				.where('M', BlockWorldState.hasState(MultiblockPatterns::isMainBlock))
//				.build();
//		
//	}
}
