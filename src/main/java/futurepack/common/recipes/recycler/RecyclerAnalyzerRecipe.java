package futurepack.common.recipes.recycler;
import java.util.Collections;
import java.util.List;

import futurepack.api.ItemPredicateBase;
import futurepack.api.interfaces.IRecyclerRecipe;
import futurepack.common.item.misc.MiscItems;
import futurepack.common.recipes.EnumRecipeSync;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;

public class RecyclerAnalyzerRecipe implements IRecyclerRecipe 
{
	private ItemPredicateBase input;
	private int support;
	
	@Override
	public ItemStack[] getMaxOutput() 
	{
		return new ItemStack[0];
	}

	@Override
	public ItemPredicateBase getInput()
	{
		return input;
	}
	
	public RecyclerAnalyzerRecipe(ItemPredicateBase i, int sp)
	{
		input = i;
		support = sp;
	}
	
	public int getSupport() 
	{
		return support;
	}
		
	public boolean match(ItemStack in) 
	{
		if(in!=null)
		{
			return input.apply(in, false);
		}
		return false;
	}

	@Override
	public List<ItemStack> getToolItemStacks() 
	{
		return Collections.singletonList(new ItemStack(MiscItems.analyzer));
	}

	@Override
	public float[] getChances() 
	{
		return null;
	}

	@Override
	public String getJeiInfoText() 
	{
		return I18n.get("jei.recycler.analyser.makesupport", support);
	}	
	
	public void write(FriendlyByteBuf buf)
	{
		EnumRecipeSync.writeUnknown(input, buf);
		buf.writeVarInt(support);
	}
	
	public static RecyclerAnalyzerRecipe read(FriendlyByteBuf buf)
	{
		return new RecyclerAnalyzerRecipe((ItemPredicateBase) EnumRecipeSync.readUnknown(buf), buf.readVarInt());
	}
}
