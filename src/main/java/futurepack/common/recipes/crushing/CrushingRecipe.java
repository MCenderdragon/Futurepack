package futurepack.common.recipes.crushing;

import futurepack.api.ItemPredicateBase;
import futurepack.api.interfaces.IOptimizeable;
import futurepack.common.recipes.EnumRecipeSync;
import futurepack.common.research.CustomPlayerData;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;

/**
 * This Recipes are getCount() sensitive
 */
public class CrushingRecipe implements IOptimizeable
{
	private ItemStack out;
	private ItemPredicateBase in;
	
	public CrushingRecipe(ItemPredicateBase in, ItemStack out)
	{
		this.in = in;
		this.out = out;
	}
	
	public boolean matches(ItemStack it)
	{
		if(it!=null)
		{
			return in.apply(it, false);
		}
		return false;
	}
	
	public ItemStack getOutput()
	{
		return out.copy();
	}
	
	@Override
	public String toString()
	{
		return "Crusher:" + in + "->" + out+ ";";
	}

	public ItemPredicateBase getInput()
	{
		return in;
	}
	
	public boolean isLocalResearched()
	{
		return CustomPlayerData.createForLocalPlayer().canProduce(out);
	}
	
	private int points = 0;
	
	@Override
	public int getPoints()
	{
		return points;
	}

	@Override
	public void addPoint()
	{
		points++;
	}

	@Override
	public void resetPoints()
	{
		points++;
	}
	
	public void write(FriendlyByteBuf buf)
	{
		EnumRecipeSync.writeUnknown(in, buf);
		buf.writeItem(out);
	}
	
	public static CrushingRecipe read(FriendlyByteBuf buf)
	{
		return new CrushingRecipe((ItemPredicateBase) EnumRecipeSync.readUnknown(buf), buf.readItem());
	}
}
