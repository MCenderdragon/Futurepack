package futurepack.common;

import java.util.EnumMap;
import java.util.function.BooleanSupplier;
import java.util.function.IntSupplier;

import futurepack.api.Constants;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.event.AttachCapabilitiesEvent;

public class CapProviderRFSupport 
{
	

	public static void addProvider(AttachCapabilitiesEvent<BlockEntity> event) 
	{
		if(Constants.MOD_ID.equals(HelperItems.getRegistryName(event.getObject().getType()).getNamespace()))
		{
			RFCapabilityProvider prov = new RFCapabilityProvider(event.getObject());
			event.addCapability(new ResourceLocation(Constants.MOD_ID, "rf2neon"), prov);
			event.addListener(prov::invalidate);
		}
	}
	
	public static class RFCapabilityProvider implements ICapabilityProvider
	{
		private final BlockEntity tile;
		private final EnumMap<Direction, LazyOptional<IEnergyStorage>> map = new EnumMap<>(Direction.class);

		 RFCapabilityProvider(BlockEntity tile)
		 {
			 this.tile = tile;
		 }
		
		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) 
		{
			if(cap == CapabilityEnergy.ENERGY)
			{
				return map.computeIfAbsent(side, this::compute).cast();
			}
			return LazyOptional.empty();
		}
		
		private LazyOptional<IEnergyStorage> compute(Direction side)
		{
			LazyOptional<INeonEnergyStorage> optNeon = tile.getCapability(CapabilityNeon.cap_NEON, side);
			if(optNeon.isPresent())
			{
				LazyOptional<IEnergyStorage> opt = LazyOptional.of(() -> new RFNeonWrapper(optNeon.orElseThrow(NullPointerException::new), () -> 100, () -> true));
				optNeon.addListener(p -> opt.invalidate());
				opt.addListener(p -> map.remove(side));
				
				return opt;
			}
			else
			{
				return LazyOptional.empty();
			}
		}
		
		public void invalidate()
		{
			HelperEnergyTransfer.invalidateCaps(map.values().toArray(LazyOptional[]::new));
		}
	}
	
	public static class RFNeonWrapper implements IEnergyStorage
	{
		final INeonEnergyStorage neon;
		final IntSupplier maxConverSationRate;
		final BooleanSupplier isWorking;
		
		public RFNeonWrapper(INeonEnergyStorage neon, IntSupplier maxConverSationRate, BooleanSupplier isWorking) 
		{
			super();
			this.neon = neon;
			this.maxConverSationRate = maxConverSationRate;
			this.isWorking = isWorking;
		}

		@Override
		public int receiveEnergy(int maxReceive, boolean simulate)
		{
			if(!isWorking.getAsBoolean())
				return 0;
			
			maxReceive = Math.min(maxReceive, maxConverSationRate.getAsInt()*4);
			int ne = maxReceive /4;
			int space = neon.getMax() - neon.get();
			int transfer = Math.min(ne, space);
			
			if(!simulate)
			{
				neon.add(transfer);
			}
			
			return transfer *4;
		}

		@Override
		public int extractEnergy(int maxExtract, boolean simulate)
		{
			return 0;
		}

		@Override
		public int getEnergyStored()
		{
			return neon.get() * 2;
		}

		@Override
		public int getMaxEnergyStored()
		{
			return neon.getMax() * 2;
		}

		@Override
		public boolean canExtract()
		{
			return false;
		}

		@Override
		public boolean canReceive()
		{
			return isWorking.getAsBoolean();
		}
		
	}
	
}
