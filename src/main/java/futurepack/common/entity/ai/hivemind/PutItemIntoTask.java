package futurepack.common.entity.ai.hivemind;

import futurepack.common.entity.living.EntityDungeonSpider;
import futurepack.depend.api.helper.HelperInventory;
import futurepack.depend.api.helper.HelperSerialisation;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;

public class PutItemIntoTask extends HVTask 
{
	private BlockPos pos;
	private Direction dir;
	
	
	public PutItemIntoTask(BlockPos pos, Direction dir) 
	{
		super();
		this.pos = pos;
		this.dir = dir;
	}

	public PutItemIntoTask(CompoundTag nbt) 
	{
		super(nbt);
	}
	
	@Override
	public CompoundTag serializeNBT() 
	{
		CompoundTag nbt = new CompoundTag();
		HelperSerialisation.putBlockPos(nbt, "pos", pos);
		nbt.putInt("direction", dir.get3DDataValue());
		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundTag nbt) 
	{
		pos = HelperSerialisation.getBlockPos(nbt, "pos");
		dir = Direction.from3DDataValue(nbt.getInt("direction"));
	}

	@Override
	public BlockPos getSpiderPosition() 
	{
		return pos;
	}

	@Override
	public boolean execute(EntityDungeonSpider spider) 
	{
		if(!spider.getMainHandItem().isEmpty())
		{
			BlockPos invntory = pos.relative(dir);
			BlockEntity tile = spider.getCommandSenderWorld().getBlockEntity(invntory);
			if(tile!=null)
			{
				IItemHandler handler = HelperInventory.getHandler(tile, dir.getOpposite());
				if(handler != null)
				{
					ItemStack stack = spider.getMainHandItem();
					spider.setItemInHand(InteractionHand.MAIN_HAND, ItemHandlerHelper.insertItem(handler, stack, false));
				}
				else
				{
					return isDone = true;
				}
			}
			else
			{
				return isDone = true;
			}
		}
		return isDone = spider.getMainHandItem().isEmpty();
	}
	
	@Override
	public boolean canExecute(EntityDungeonSpider spider, AssignedTask assignedTask) 
	{
		return !spider.getMainHandItem().isEmpty();
	}

}
