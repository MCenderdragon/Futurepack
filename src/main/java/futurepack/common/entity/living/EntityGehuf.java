package futurepack.common.entity.living;

import javax.annotation.Nullable;

import futurepack.api.Constants;
import futurepack.common.FPEntitys;
import futurepack.common.block.plants.PlantBlocks;
import futurepack.common.entity.ai.EntityAIEatCroops;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.Mth;
import net.minecraft.world.Container;
import net.minecraft.world.ContainerHelper;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.animal.Cow;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ChestMenu;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;

public class EntityGehuf extends Cow implements Container, MenuProvider
{
	private static EntityDataAccessor<Boolean> chest = SynchedEntityData.defineId(EntityGehuf.class, EntityDataSerializers.BOOLEAN);

	public EntityGehuf(EntityType<EntityGehuf> type, Level w)
	{
		super(type, w);
	}
	
	private int eatTimer = 0;
	
	@Override
	protected void registerGoals()
	{
		super.registerGoals();
		this.goalSelector.addGoal(4, new EntityAIEatCroops(this, PlantBlocks.mendel_berry, 1.0));
	}
	
	@Override
	public EntityType<?> getType() 
	{
		return FPEntitys.GEHUF;
	}
	
	@Override
	public EntityGehuf getBreedOffspring(ServerLevel world, AgeableMob p_90011_1_)
	{
		return new EntityGehuf(FPEntitys.GEHUF, world);
	}
	
	@Override
	protected void defineSynchedData() 
	{
		super.defineSynchedData();		
		this.entityData.define(chest, false);
	}
	
	@Override
	public void handleEntityEvent(byte id)
	{
		if(id==12)
		{
			eatTimer = 40;
		}
		super.handleEntityEvent(id);
	}
	
	@Override
	public void aiStep()
	{
		if (this.level.isClientSide)
		{
			eatTimer = eatTimer>0?eatTimer-1:0;
		}
		
		super.aiStep();
	}
	
	private void setChest(boolean b)
	{
		this.entityData.set(chest, b);
	}
	
	public boolean hasChest()
	{
		return this.entityData.get(chest);
	}
	
	@Override
	public InteractionResult mobInteract(Player pl, InteractionHand hand)
    {
		InteractionResult b = super.mobInteract(pl, hand);
		if(!isBaby())
		{
			if(hasChest())
			{
//				pl.openContainer(p_213829_1_)
				
				if(pl instanceof ServerPlayer)
				{
					((ServerPlayer)pl).openMenu(this);
				}
				
				return InteractionResult.SUCCESS;
			}
			ItemStack itemstack = pl.getInventory().getSelected();
	
			if (!itemstack.isEmpty() && itemstack.getItem() == Blocks.CHEST.asItem())
			{
				if(!pl.isCreative())
					itemstack.shrink(1);
				 
				setChest(true);
				return InteractionResult.SUCCESS;
			}
		}
		return b;
	}

	private NonNullList<ItemStack> chestContents = NonNullList.withSize(27, ItemStack.EMPTY);
	
	@Override
	public int getContainerSize() 
	{
		return chestContents.size();
	}

	@Override
    public ItemStack getItem(int slot)
    {
        return this.chestContents.get(slot);
    }

    @Override
    public ItemStack removeItem(int index, int count)
    {
    	 ItemStack itemstack = ContainerHelper.removeItem(chestContents, index, count);

         if (!itemstack.isEmpty())
         {
             this.setChanged();
         }

         return itemstack;
    }

    @Override
    public ItemStack removeItemNoUpdate(int index)
    {
    	 return ContainerHelper.takeItem(chestContents, index);
    }

    @Override
    public void setItem(int index, ItemStack stack)
    {
    	 chestContents.set(index, stack);

         if (stack.getCount() > this.getMaxStackSize())
         {
             stack.setCount(this.getMaxStackSize());
         }

         this.setChanged();
    }

//    @Override
//    public String getInventoryName()
//    {
//        return "container.chest";
//    }
//    
//    @Override
//    public boolean hasCustomName()
//    {
//        return false;
//    }
    
    @Override
    public int getMaxStackSize()
    {
        return 64;
    }
    
//    @Override
//    public boolean isUseableByPlayer(EntityPlayer p_70300_1_)
//    {
//        return getDistanceToEntity(p_70300_1_) <= 64.0D;
//    }
    
    @Override
    public boolean stillValid(Player player)
    {
    	return distanceToSqr(player) <= 64.0D;
    }
    
    @Override
    public boolean canPlaceItem(int p_94041_1_, ItemStack p_94041_2_)
    {
        return true;
    }

	@Override
	public void setChanged() {}


	@Override
	public void readAdditionalSaveData(CompoundTag nbt)
	{
		super.readAdditionalSaveData(nbt);
		setChest(nbt.getBoolean("Chest"));
		ListTag nbttaglist = nbt.getList("Items", 10);
        for (int i = 0; i < nbttaglist.size(); ++i)
        {
            CompoundTag nbttagcompound1 = nbttaglist.getCompound(i);
            int j = nbttagcompound1.getByte("Slot") & 255;

            if (j >= 0 && j < this.chestContents.size())
            {
            	setItem(j, ItemStack.of(nbttagcompound1));
            }
        }
	}
	
	@Override
	public void addAdditionalSaveData(CompoundTag nbt)
	{
		super.addAdditionalSaveData(nbt);
		nbt.putBoolean("Chest", hasChest());
	
		ListTag nbttaglist = new ListTag();

        for (int i = 0; i < this.chestContents.size(); ++i)
        {
            if (!this.chestContents.get(i).isEmpty())
            {
                CompoundTag nbttagcompound1 = new CompoundTag();
                nbttagcompound1.putByte("Slot", (byte)i);
                this.chestContents.get(i).save(nbttagcompound1);
                nbttaglist.add(nbttagcompound1);
            }
        }

        nbt.put("Items", nbttaglist);

	}
   
	@Override
	public void die(DamageSource p_70645_1_)
	{
		if(hasChest())
		{
			for(ItemStack it : chestContents)
			{
				if(it==null || it.isEmpty())
					continue;
				
				ItemEntity item = new ItemEntity(level, getX(), getY(), getZ(), it);
				level.addFreshEntity(item);
			}
		}
			
		super.die(p_70645_1_);
	}

	@Override
	public void startOpen(Player player) { }

	@Override
	public void stopOpen(Player player) { }

	@Override
	public void clearContent() {}

	@Override
	public boolean isEmpty()
	{
		return !hasChest();
	}
	
	//@ TODO: OnlyIn(Dist.CLIENT)
    public float getHeadRotationPointY(float p_70894_1_)
    {
        return this.eatTimer <= 0 ? 0.0F : (this.eatTimer >= 4 && this.eatTimer <= 36 ? 1.0F : (this.eatTimer < 4 ? (this.eatTimer - p_70894_1_) / 4.0F : -(this.eatTimer - 40 - p_70894_1_) / 4.0F));
    }

    //@ TODO: OnlyIn(Dist.CLIENT)
    public float getHeadRotationAngleX(float p_70890_1_)
    {
        if (this.eatTimer > 4 && this.eatTimer <= 36)
        {
            float f = (this.eatTimer - 4 - p_70890_1_) / 32.0F;
            return ((float)Math.PI / 5F) + ((float)Math.PI * 7F / 100F) * Mth.sin(f * 28.7F);
        }
        else
        {
            return this.eatTimer > 0 ? ((float)Math.PI / 5F) : this.getXRot() * 0.017453292F;
        }
    }
    
    @Nullable
    @Override
    protected ResourceLocation getDefaultLootTable()
    {
    	return new ResourceLocation(Constants.MOD_ID, "entities/gehuf");
    }

	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inv, Player pl) 
	{
		return ChestMenu.threeRows(id, inv, this);
	}
}
