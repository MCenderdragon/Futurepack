package futurepack.common.entity;

import futurepack.api.Constants;
import futurepack.common.FPLog;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;

@Mod.EventBusSubscriber(modid = Constants.MOD_ID, bus = Bus.FORGE)
public class CapabilityPlayerData implements IPlayerData, INBTSerializable<CompoundTag>
{
	
	public static final Capability<IPlayerData> cap_PLAYERDATA = CapabilityManager.get(new CapabilityToken<>(){});
	
//	public static class Storage implements IStorage<IPlayerData>
//	{
//		@Override
//		public Tag writeNBT(Capability<IPlayerData> capability, IPlayerData instance, Direction side)
//		{
//			return instance.getTag();
//		}
//
//		@Override
//		public void readNBT(Capability<IPlayerData> capability, IPlayerData instance, Direction side, Tag nbt)
//		{
//			instance.addAll((CompoundTag)nbt);
//		}
//	}
	
	private CompoundTag nbt;
	
	public CapabilityPlayerData() 
	{
		nbt = new CompoundTag();
	}
	
	
	@Override
	public CompoundTag serializeNBT() 
	{
		return getTag();
	}

	@Override
	public void deserializeNBT(CompoundTag nbt) 
	{
		addAll(getTag());
	}

	@Override
	public CompoundTag getTag() 
	{
		return nbt;
	}

	@Override
	public void addAll(CompoundTag nbt) 
	{
		this.nbt.merge(nbt);
	}
	
	
	public static CompoundTag getPlayerdata(Player pl)
	{
		if(pl.isAlive())
			return pl.getCapability(cap_PLAYERDATA).map(IPlayerData::getTag).orElseThrow(NullPointerException::new);
		else
			return new CompoundTag();
	}
	
	@SubscribeEvent
	public static void addDataCapsToPlayer(AttachCapabilitiesEvent<Entity> e)
	{
		if(e.getGenericType()==Entity.class)
		{
			if(e.getObject() instanceof Player)
			{	
				e.addCapability(new ResourceLocation(Constants.MOD_ID, "additionaldata"), new PlayerDataProvider());
			}
		}
	}
	
	@SubscribeEvent
	public static void copyData(PlayerEvent.Clone e)
	{
		CompoundTag original = e.getOriginal().getCapability(cap_PLAYERDATA).map(IPlayerData::getTag).orElse(null);
		if(original!=null)
		{
			CompoundTag newPlayer = e.getPlayer().getCapability(cap_PLAYERDATA).map(IPlayerData::getTag).orElseThrow(NullPointerException::new);
			newPlayer.merge(original);
		}
		else
		{
			FPLog.logger.warn("Old player %s did not have futurepack-data capability", e.getOriginal());
		}
	}
	
	private static class PlayerDataProvider implements ICapabilityProvider, INBTSerializable<CompoundTag>
	{
		LazyOptional<IPlayerData> opt = LazyOptional.of(CapabilityPlayerData::new);
		
		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
		{
			if(capability==cap_PLAYERDATA)
			{
				return opt.cast();
			}
			return LazyOptional.empty();
		}

		@Override
		public CompoundTag serializeNBT()
		{
			return opt.map(d -> d.getTag()).orElseGet(CompoundTag::new);
		}

		@Override
		public void deserializeNBT(CompoundTag nbt)
		{
			opt.ifPresent(d -> d.addAll(nbt));
		}
		
	}
	
}
