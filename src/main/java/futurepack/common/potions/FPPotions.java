package futurepack.common.potions;

import java.util.WeakHashMap;

import futurepack.api.Constants;
import futurepack.common.FPConfig;
import futurepack.common.block.misc.TileEntityForcefieldGenerator;
import futurepack.common.item.tools.compositearmor.CompositeArmorInventory;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.damagesource.EntityDamageSource;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.entity.projectile.ThrowableProjectile;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.TickEvent.Phase;
import net.minecraftforge.event.TickEvent.WorldTickEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.living.PotionEvent;
import net.minecraftforge.eventbus.api.Event.Result;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = Constants.MOD_ID)
public class FPPotions
{
	public static final MobEffect PARALYZED = new PotionParalyze().setRegistryName(new ResourceLocation(Constants.MOD_ID, "paralyze"));
	public static final MobEffect CHARGED = new PotionCharged().setRegistryName(new ResourceLocation(Constants.MOD_ID, "charged"));
	
	
	public static void register(RegistryEvent.Register<MobEffect> w)
	{
		w.getRegistry().register(PARALYZED);
		w.getRegistry().register(CHARGED);
	}
	
	@SubscribeEvent
	public static void onLivingTick(LivingUpdateEvent event)
	{
//		BlockPos pos = new BlockPos(event.getEntityLiving());
//		IBlockState state = event.getEntityLiving().world.getBlockState(pos);
//		if(state.getBlock() == FPBlocks.neonLiquid)
//		{
//			if(!CompositeArmorInventory.hasSetBonus(event.getEntityLiving()))
//			{
//				event.getEntityLiving().addPotionEffect(new PotionEffect(FPPotions.paralyze, 20*2, 1));
//			}	
//		}
		
		//still needed as for tome reason tick doesn twork
		MobEffect[] effects = {PARALYZED, CHARGED};
		for(MobEffect effect : effects)
		{
			MobEffectInstance eff = event.getEntityLiving().getEffect(effect);
			if(eff!=null)
			{
				effect.applyEffectTick(event.getEntityLiving(), eff.getAmplifier());
			}
		}
	}
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	@SubscribeEvent
	public static void onClientTick(TickEvent.ClientTickEvent event)
	{
		if(event.phase==Phase.END)
		{
			Minecraft mc = Minecraft.getInstance();
			if(mc.player!=null)
			{
				GameRenderer render = mc.gameRenderer;
				if(mc.player.hasEffect(PARALYZED) && render.currentEffect() == null)
				{
					render.loadEffect(new ResourceLocation(Constants.MOD_ID, "shader/post/paralyze.json"));
				}
				else if(!mc.player.hasEffect(PARALYZED) && render.currentEffect() != null)
				{
					render.shutdownEffect();
				}
			}
		}
	}
	
	@SubscribeEvent
	public static void onLivingAttack(LivingAttackEvent event)
	{
		if(event.getSource() instanceof EntityDamageSource)
		{
			if(event.getSource().getEntity() instanceof LivingEntity)
			{
				LivingEntity base = (LivingEntity) event.getSource().getEntity();
				if(base.hasEffect(PARALYZED))
				{
					event.setCanceled(true);
				}
			}
		}
	}
	
	@SubscribeEvent
	public static void onNewPotionEffect(PotionEvent.PotionAddedEvent event)
	{
		
	}
	
	@SubscribeEvent
	public static void isPotionEffectApplicable(PotionEvent.PotionApplicableEvent event)
	{
		if(!event.getEntity().level.isClientSide)
		{
			if(allowed.get(event.getEntityLiving()) != Boolean.TRUE)
			{
				if(CompositeArmorInventory.hasAirTightSetBonus(event.getEntityLiving()))
				{
					if(event.getPotionEffect().getEffect() == CHARGED)
					{
						event.setResult(Result.ALLOW);
					}
					else if(event.getPotionEffect().getEffect() == MobEffects.JUMP && CompositeArmorInventory.hasDungeonSetBonus(event.getEntityLiving()))
					{
						event.setResult(Result.ALLOW);
					}
					else
					{
						Exception e = new Exception();
						StackTraceElement[] stack = e.getStackTrace();
						
						if(stack.length >= 9 && stack[6]!=null)
						{
							if(stack[6].getClassName().equals(LivingEntity.class.getName()) && ("addEffect".equals(stack[6].getMethodName()) || "m_147207_".equals(stack[6].getMethodName())))
							{
								String src = stack[7].getClassName() + "#" + stack[7].getMethodName();
								if(src.equals("net.minecraft.world.entity.LivingEntity#m_7292_") || src.equals("net.minecraft.world.entity.LivingEntity#addEffect"))
								{
									src = stack[8].getClassName() + "#" + stack[8].getMethodName();
								}
								
								
								Boolean b = FPConfig.SERVER.getCompositeArmorPotionMap().computeIfAbsent(src, s -> {
									
									String[] ss = s.split("#", 2);
									
									if(ss.length==2)
									{
										if("addEatEffect".equals(ss[1]) || "m_21063_".equals(ss[1]))
										{
											return false;
										}
									}
									
									try
									{
										Class c = Class.forName(ss[0]);
										if(Block.class.isAssignableFrom(c))
										{
											return true;
										}
										else if(BlockEntity.class.isAssignableFrom(c))
										{
											return true;
										}
										else if(Projectile.class.isAssignableFrom(c))
										{
											return true;
										}
									}
									catch(ClassNotFoundException ee)
									{
										
									}
									
									
									return true;
								});
								if(b==true)
								{
									event.setResult(Result.DENY);
								}
	
								return;
							}
						}
							
						
						event.setResult(Result.DENY);
					}
					
				}
			}
		}
	}
	
	@SubscribeEvent
	public static void onRemovePotionEffect(PotionEvent.PotionRemoveEvent event)
	{
		
	}
	
	@SubscribeEvent
	public static void onItemUseFinsih(LivingEntityUseItemEvent.Tick event)
	{
		if(!event.getEntity().level.isClientSide)
		{
			if(event.getDuration() <= 1 && CompositeArmorInventory.hasAirTightSetBonus(event.getEntityLiving()))
			{
				allowed.put(event.getEntityLiving(), Boolean.TRUE);
			}
		}
	}
	
	private static WeakHashMap<LivingEntity, Boolean> allowed = new WeakHashMap<>();
	
	@SubscribeEvent
	public static void onWorldTick(WorldTickEvent event)
	{
		if(event.phase == Phase.START)
		{
			allowed.clear();
		}
	}
}
