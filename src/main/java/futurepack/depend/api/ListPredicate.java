package futurepack.depend.api;

import java.util.Arrays;
import java.util.List;

import com.google.common.base.Predicate;

import futurepack.api.ItemPredicateBase;
import futurepack.common.recipes.EnumRecipeSync;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;

public class ListPredicate extends ItemPredicateBase
{
	/**
	 * true:  OR;
	 * false: AND;
	 */
	private final boolean mode;
	private ItemPredicateBase[] preds;
	
	public ListPredicate(boolean mode, ItemPredicateBase...predicates)
	{
		this.mode = mode;
		preds = predicates;
	}
	
	@Override
	public boolean apply(ItemStack input) 
	{
		if(mode)
		{
			for(Predicate<ItemStack> it : preds)
			{
				if(it.apply(input))
				{
					return true;
				}
			}
			return false;
		}
		else
		{
			for(Predicate<ItemStack> it : preds)
			{
				if(!it.apply(input))
				{
					return false;
				}
			}
			return true;
		}
	}

	@Override
	public ItemStack getRepresentItem()
	{
		for(ItemPredicateBase it : preds)
		{		
			return it.getRepresentItem();
		}
		return null;
	}

	@Override
	public int getMinimalItemCount(ItemStack item)
	{
		for(ItemPredicateBase it : preds)
		{
			if(it.apply(item))
			{
				return it.getMinimalItemCount(item);
			}
		}
		return 0;
	}
	
	@Override
	public List<ItemStack> collectAcceptedItems(List<ItemStack> list)
	{
		for(ItemPredicateBase it : preds)
		{		
			it.collectAcceptedItems(list);
		}
		return list;
	}
	
	@Override
	public String toString()
	{
		return (mode?"OR":"AND") + Arrays.deepToString(preds);
	}
	
	public void write(FriendlyByteBuf buf)
	{
		buf.writeBoolean(mode);
		EnumRecipeSync.writePredicates(preds, buf);
	}
	
	public static ListPredicate read(FriendlyByteBuf buf)
	{
		return new ListPredicate(buf.readBoolean(), EnumRecipeSync.readPredicates(buf));
	}
}
