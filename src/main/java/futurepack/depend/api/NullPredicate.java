package futurepack.depend.api;

import java.util.List;

import futurepack.api.ItemPredicateBase;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;

public class NullPredicate extends ItemPredicateBase
{

	@Override
	public boolean apply(ItemStack input)
	{
		return false;
	}

	@Override
	public ItemStack getRepresentItem()
	{
		return null;
	}

	@Override
	public int getMinimalItemCount(ItemStack input)
	{
		return 0;
	}
	
	@Override
	public List<ItemStack> collectAcceptedItems(List<ItemStack> list)
	{
		return list;
	}
	
	@Override
	public String toString()
	{
		return "nullPredicate";
	}
	
	public void write(FriendlyByteBuf buf)
	{
		
	}
	
	public static NullPredicate read(FriendlyByteBuf buf)
	{
		return new NullPredicate();
	}
}
