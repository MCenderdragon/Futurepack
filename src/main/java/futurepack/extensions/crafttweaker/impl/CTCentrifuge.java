package futurepack.extensions.crafttweaker.impl;

import org.openzen.zencode.java.ZenCodeType;

import com.blamejared.crafttweaker.api.CraftTweakerAPI;
import com.blamejared.crafttweaker.api.annotation.ZenRegister;
import com.blamejared.crafttweaker.api.ingredient.IIngredient;
import com.blamejared.crafttweaker.api.item.IItemStack;

import futurepack.common.recipes.centrifuge.FPZentrifugeManager;
import futurepack.common.recipes.centrifuge.ZentrifugeRecipe;
import futurepack.extensions.crafttweaker.CrafttweakerExtension;
import futurepack.extensions.crafttweaker.RecipeActionBase;

@ZenRegister
@ZenCodeType.Name("mods.futurepack.centrifuge")
public class CTCentrifuge
{

	static class ZentrifugeRecipeAddAction extends RecipeActionBase<ZentrifugeRecipe>
	{
		private ZentrifugeRecipe r;

		public ZentrifugeRecipeAddAction(String id, IItemStack[] output, IIngredient input, int support)
		{
			super(Type.ADD, () -> FPZentrifugeManager.instance);

			r = new ZentrifugeRecipe(CrafttweakerExtension.convert(input), CrafttweakerExtension.convert(output), support);
		}

		@Override
		public ZentrifugeRecipe createRecipe()
		{
			return r;
		}

	}

	static class ZentrifugeRecipeRemoveAction extends RecipeActionBase<ZentrifugeRecipe>
	{
		private IItemStack input;

		public ZentrifugeRecipeRemoveAction(IItemStack input)
		{
			super(Type.REMOVE, () -> FPZentrifugeManager.instance);

			this.input = input;
		}

		@Override
		public ZentrifugeRecipe createRecipe()
		{
			return ((FPZentrifugeManager)recipeManager.get()).getMatchingRecipe(input.getInternal(), true);
		}

	}

	@ZenCodeType.Method
	public static void add(String id, IItemStack[] output, IIngredient input, int support)
	{
		CraftTweakerAPI.apply(new ZentrifugeRecipeAddAction(id, output, input, support));
	}

	@ZenCodeType.Method
	public static void remove(IItemStack input)
	{
		CraftTweakerAPI.apply(new ZentrifugeRecipeRemoveAction(input));
	}
}
