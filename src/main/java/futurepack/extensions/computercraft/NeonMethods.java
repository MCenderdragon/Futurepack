package futurepack.extensions.computercraft;

import dan200.computercraft.api.lua.LuaFunction;
import dan200.computercraft.api.peripheral.GenericPeripheral;
import dan200.computercraft.api.peripheral.PeripheralType;
import futurepack.api.Constants;
import futurepack.api.capabilities.INeonEnergyStorage;
import net.minecraft.resources.ResourceLocation;
import javax.annotation.Nonnull;

public class NeonMethods implements GenericPeripheral 
{
	@Nonnull
	@Override
	public PeripheralType getType() 
	{
		return PeripheralType.ofAdditional("neon_storage");
	}

	@Nonnull
	@Override
	public ResourceLocation id() 
	{
		return new ResourceLocation(Constants.MOD_ID, "neon");
	}
	
    /**
     * Get the energy of this block.
     *
     * @param energy The current energy storage.
     * @return The energy stored in this block, in FE.
     */
    @LuaFunction( mainThread = true )
    public static int getEnergy( INeonEnergyStorage energy )
    {
        return energy.get();
    }

    /**
     * Get the maximum amount of energy this block can store.
     *
     * @param energy The current energy storage.
     * @return The energy capacity of this block.
     */
    @LuaFunction( mainThread = true )
    public static int getEnergyCapacity( INeonEnergyStorage energy )
    {
        return energy.getMax();
    }

}
