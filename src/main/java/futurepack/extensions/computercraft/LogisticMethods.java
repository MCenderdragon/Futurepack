package futurepack.extensions.computercraft;

import javax.annotation.Nonnull;

import dan200.computercraft.api.lua.LuaFunction;
import dan200.computercraft.api.peripheral.GenericPeripheral;
import dan200.computercraft.api.peripheral.PeripheralType;
import futurepack.api.Constants;
import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.ILogisticInterface;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;

public class LogisticMethods implements GenericPeripheral 
{
	@Nonnull
	@Override
	public PeripheralType getType() 
	{
		return PeripheralType.ofAdditional("logistic");
	}

	@Nonnull
	@Override
	public ResourceLocation id() 
	{
		return new ResourceLocation(Constants.MOD_ID, "logistic");
	}
	
    /**
     * Gets the IO mode for the given logistic type
     *
     * @param log The configurable block
     * @param side the side of the block you which to edit [down up, west, east, north, south]
     * @param type the Logistic type (ENERGIE, SUPPORT, NETWORK, ITEMS, FLUIDS)
     * @return The io configuration (NONE; IN, OUT; INOUT)
     */
    @LuaFunction( mainThread = true )
    public static String getMode( ILogisticInterface log, String side , String type)
    {
        return getInterfaceForOtherSide(log, side).getMode(EnumLogisticType.valueOf(type)).toString();
    }
    
    /**
     * Sets the IO mode for the given logistic type
     *
     * @param log The configurable block
     * @param side the side of the block you which to edit [down up, west, east, north, south]
     * @param type the Logistic type (ENERGIE, SUPPORT, NETWORK, ITEMS, FLUIDS)
     * @param mode The io configuration (NONE; IN, OUT; INOUT)
     */
    @LuaFunction( mainThread = true )
    public static void setMode( ILogisticInterface log , String side, String type, String mode)
    {
    	getInterfaceForOtherSide(log, side).setMode(EnumLogisticIO.valueOf(mode), EnumLogisticType.valueOf(type));
    }

    /**
     * Gets the IO mode for the given logistic type
     *
     * @param log The configurable block
     * @param side the side of the block you which to edit [down up, west, east, north, south]
     * @param type the Logistic type (ENERGIE, SUPPORT, NETWORK, ITEMS, FLUIDS)
     * @return true if the type is suported, false otherwise
     */
    @LuaFunction( mainThread = true )
    public static boolean isTypeSupported( ILogisticInterface log , String side, String type)
    {
        return getInterfaceForOtherSide(log, side).isTypeSupported(EnumLogisticType.valueOf(type));
    }
    
    /**
     * Gets the IO mode for the given logistic type
     *
     * @param log The configurable block
     * @param side the side of the block you which to edit [down up, west, east, north, south]
     * @return the ILogisticInterface for that side or log if its not supported
     */
    public static ILogisticInterface getInterfaceForOtherSide( ILogisticInterface log , String side)
    {
        if(log instanceof LogisticStorage.LogInf)
        {
        	return ((LogisticStorage.LogInf) log).getForOtherSide(Direction.byName(side.toLowerCase()));
        }
        return log;
    }

    
}
