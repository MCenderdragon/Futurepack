package futurepack.extensions.computercraft;

import futurepack.depend.api.interfaces.ITileInventoryProvider;
import net.minecraft.world.item.ItemStack;

public class PusherFilterAccess extends FilterAccessBase<ITileInventoryProvider>
{
	public PusherFilterAccess(ITileInventoryProvider block) 
	{
		super(block);
	}

	@Override
	public String[] getFilterGroups() 
	{
		return new String[] {"main"};
	}

	@Override
	public int getSlots(String group) 
	{
		return filter.getInventory().getContainerSize();
	}

	@Override
	public ItemStack getItem(String group, int slot) 
	{
		return filter.getInventory().getItem(slot);
	}

	@Override
	public void setItem(String group, int slot, ItemStack stack) 
	{
		filter.getInventory().setItem(slot, stack);
	}

}
