package futurepack.extensions.jei.assembly;

import java.util.ArrayList;
import java.util.List;

import futurepack.api.Constants;
import futurepack.api.ItemPredicateBase;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.recipes.assembly.AssemblyRecipe;
import futurepack.extensions.jei.BaseRecipeCategory;
import futurepack.extensions.jei.FuturepackUids;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

public class AssemblyCategory extends BaseRecipeCategory<AssemblyRecipe>
{
	public AssemblyCategory(IGuiHelper gui)
	{
		super(gui, InventoryBlocks.assembly_table_w, FuturepackUids.ASSEMBLY, 0, 32);
	}
	
	@Override
	protected IDrawable createBackground(IGuiHelper gui) 
	{
		ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/assembler.png");
		return gui.createDrawable(res, 47, 21, 80, 54);
	}

	@Override
	public Class<? extends AssemblyRecipe> getRecipeClass() 
	{
		return AssemblyRecipe.class;
	}
	
	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, AssemblyRecipe recipe, IFocusGroup focuses) 
	{
		List<List<ItemStack>> inputs = new ArrayList<List<ItemStack>>();
		for(ItemPredicateBase in : recipe.getInput())
		{
			inputs.add(in.collectAcceptedItems(new ArrayList<ItemStack>()));
		}
		while(inputs.size()<3)
		{
			inputs.add(new ArrayList<>());
		}
		builder.addSlot(RecipeIngredientRole.INPUT, 15, 1).addItemStacks(inputs.get(0));//old x y +1px
		builder.addSlot(RecipeIngredientRole.INPUT, 33, 1).addItemStacks(inputs.get(1));
		builder.addSlot(RecipeIngredientRole.INPUT, 51, 1).addItemStacks(inputs.get(2));
		
		builder.addSlot(RecipeIngredientRole.OUTPUT, 33, 33).addItemStack(recipe.getOutput());
	}

	@Override
	public boolean isResearched(AssemblyRecipe t) 
	{
		return t.isLocalResearched();
	}

}
