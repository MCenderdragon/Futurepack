package futurepack.world.gen.feature;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import com.mojang.serialization.Codec;

import futurepack.common.dim.structures.ManagerDungeonStructures;
import futurepack.common.dim.structures.StructureBase;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;

public class DungeonFeatureConfig implements FeatureConfiguration 
{
	public static final Codec<DungeonFeatureConfig> CODEC = Codec.list(Codec.STRING).fieldOf("structures")
			.xmap(DungeonFeatureConfig::new, DungeonFeatureConfig::getAsStringList)
			.codec();
	
	public final StructureEntry[] structures;

	private DungeonFeatureConfig(StructureEntry[] structures) 
	{
		super();
		this.structures = structures;
	}
	
	private DungeonFeatureConfig(List<String> structures) 
	{
		this(structures.stream().map(DungeonFeatureConfig::loadEntry).toArray(StructureEntry[]::new));
	}
	
	private List<String> getAsStringList()
	{
		return Arrays.stream(structures).map(StructureEntry::toString).collect(Collectors.toList());
	}	

	private static StructureEntry loadEntry(String combined)
	{
		String ss[] = combined.split("#");
		String path = ss[0];
		int rot = Integer.valueOf(ss[1]);
		return new StructureEntry(path, rot);
	}
	
	public static class Builder
	{
		private Vector<StructureEntry> structs = new Vector<StructureEntry>(10, 4);
		
		public DungeonFeatureConfig build()
		{
			return new DungeonFeatureConfig(structs.toArray(new StructureEntry[structs.size()]));
		}		
		
		public Builder addEntryManual(int rotations, String path)
		{
			structs.ensureCapacity(rotations);
			for(int i=0; i<rotations; i++)
			{
				structs.add(new StructureEntry(path+"_R"+i, 0));
			}	
			return this;
		}
		
		public Builder addEntryAuto(int rotations, String path)
		{
			structs.ensureCapacity(rotations);
			for(int i=0; i<rotations; i++)
			{
				structs.add(new StructureEntry(path, i));
			}
			return this;
		}

		
		
//		public Builder addEntryAuto(int rotations, String path, Function<StructureBase, StructureBase> transform)
//		{
//			structs.ensureCapacity(rotations);
//			for(int i=0; i<rotations; i++)
//			{
//				structs.add(transform.apply(ManagerDungeonStructures.get(path, i)));
//			}
//			return this;
//		}
	}
	
	public static class StructureEntry
	{
		public final StructureBase base;
		public final String path;
		public final int rot;
		
		public StructureEntry(String path, int rot) 
		{
			super();
			this.path = path;
			this.rot = rot;
			
			this.base = ManagerDungeonStructures.get(path, rot);
		}
		
		@Override
		public String toString() 
		{
			return path + "#" + rot;
		}
	}
}
