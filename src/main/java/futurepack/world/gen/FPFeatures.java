package futurepack.world.gen;

import java.util.function.Consumer;

import futurepack.api.Constants;
import futurepack.depend.api.helper.HelperItems;
import futurepack.world.gen.feature.AbstractDungeonFeature;
import futurepack.world.gen.feature.BedrockRiftFeature;
import futurepack.world.gen.feature.BendsFeature;
import futurepack.world.gen.feature.BendsFeatureConfig;
import futurepack.world.gen.feature.BigMushroomFeature;
import futurepack.world.gen.feature.BigMushroomFeatureConfig;
import futurepack.world.gen.feature.CrystalBubbleFeature;
import futurepack.world.gen.feature.MycelFeature;
import futurepack.world.gen.feature.PalirieTreeFeature;
import futurepack.world.gen.feature.SmallCraterFeature;
import futurepack.world.gen.feature.SpecialDirtFeature;
import futurepack.world.gen.feature.TyrosDeadTreeFeature;
import futurepack.world.gen.feature.TyrosTreeAsync;
import futurepack.world.gen.feature.TyrosTreeFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.BlockStateConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;

public class FPFeatures {

	public static final CrystalBubbleFeature CRYSTAL_BUBBLE = new CrystalBubbleFeature();
	public static final AbstractDungeonFeature DUNGEON = new AbstractDungeonFeature();
	public static final BedrockRiftFeature BEDROCK_RIFT = new BedrockRiftFeature();
	public static final PalirieTreeFeature PALIRIE_TREE = new PalirieTreeFeature(NoneFeatureConfiguration.CODEC);
	public static final BigMushroomFeature MUSHROOM_TREE = new BigMushroomFeature(BigMushroomFeatureConfig.CODEC);
	public static final TyrosTreeFeature LARGE_TYROS_TREE = new TyrosTreeFeature(TreeConfiguration.CODEC);
	
	public static void registerFeatures(Consumer<Feature<?>> register)
	{
		register.accept( HelperItems.setRegistryName(new SpecialDirtFeature(BlockStateConfiguration.CODEC), Constants.MOD_ID, "menelaus_dirt"));
		register.accept( HelperItems.setRegistryName(new SmallCraterFeature(), Constants.MOD_ID, "crater"));
		register.accept( HelperItems.setRegistryName(new MycelFeature(BlockStateConfiguration.CODEC), Constants.MOD_ID, "mycel"));
		register.accept( HelperItems.setRegistryName(new BendsFeature(BendsFeatureConfig.CODEC), Constants.MOD_ID, "bend"));
		register.accept( HelperItems.setRegistryName(MUSHROOM_TREE, Constants.MOD_ID, "big_mushroom"));
		register.accept( HelperItems.setRegistryName(new TyrosDeadTreeFeature(NoneFeatureConfiguration.CODEC), Constants.MOD_ID, "dead_tree"));
		register.accept( HelperItems.setRegistryName(CRYSTAL_BUBBLE, Constants.MOD_ID, "crystal_bubble"));
		register.accept( HelperItems.setRegistryName(DUNGEON, Constants.MOD_ID, "dungeon"));
		register.accept( HelperItems.setRegistryName(PALIRIE_TREE, Constants.MOD_ID, "palirie_tree"));
		register.accept(HelperItems.setRegistryName(new TyrosTreeAsync(), Constants.MOD_ID, "large_tyros_tree_async"));
		register.accept(HelperItems.setRegistryName(BEDROCK_RIFT, Constants.MOD_ID, "bedrock_rift"));
		register.accept(HelperItems.setRegistryName(LARGE_TYROS_TREE, Constants.MOD_ID, "large_tyros_tree"));
	}

	

	

}
