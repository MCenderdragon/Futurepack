package futurepack.world.protection;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.WeakHashMap;
import java.util.concurrent.Callable;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import futurepack.api.Constants;
import futurepack.common.AsyncTaskManager;
import futurepack.common.FPLog;
import futurepack.depend.api.helper.HelperChunks;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.ByteArrayTag;
import net.minecraft.nbt.ByteTag;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.HitResult.Type;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.ChunkDataEvent;
import net.minecraftforge.event.world.ExplosionEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.registries.ForgeRegistries;

public class FPDungeonProtection 
{
	public static final Set<Block> whitelist;
	
	static
	{
		whitelist = new TreeSet<>(FPDungeonProtection::order);
		addBlockToWhiteList(new ResourceLocation("gravestone:gravestone"));
		addBlockToWhiteList(new ResourceLocation("erebus:block_of_bones"));
	}

	public static final Capability<IChunkProtection> cap_PROTECTION = CapabilityManager.get(new CapabilityToken<>(){});
	
	public static void init()
	{
		FPDungeonProtection instance  = new FPDungeonProtection();
		MinecraftForge.EVENT_BUS.register(instance);
	}
	
	private static WeakHashMap<Level, DungeonProtection> worlds = new WeakHashMap<Level, DungeonProtection>();
	
	@Deprecated
	public static DungeonProtection getInstance(Level w)
	{
		if(w.isClientSide)
		{
			FPLog.logger.error("Someone tried to get a DungeonProtecctor for a ClientWorld!");
			return null;
		}
		DungeonProtection pro = worlds.get(w);
		if(pro==null)
		{
			pro = new DungeonProtection();
			worlds.put(w, pro);
		}
		return pro;
	}

	public static void addProtection(Level w, BoundingBox box)
	{
		box.inflatedBy(1);
		
		int cx1 = box.minX() >>4;
		int cx2 = box.maxX() >>4;
		int cz1 = box.minZ() >>4;
		int cz2 = box.maxZ() >>4;
		
		LazyOptional<IChunkProtection>[][] chunks = new LazyOptional[cx2 - cx1 +1][cz2 - cz1 +1];
		for(int cx=cx1;cx<=cx2;cx++)
		{
			for(int cz=cz1;cz<=cz2;cz++)
			{
				chunks[cx-cx1][cz-cz1] = w.getChunk(cx, cz).getCapability(cap_PROTECTION);
			}
		}
		
		for(int x=box.minX();x<=box.maxX();x++)
		{
			for(int z=box.minZ();z<=box.maxZ();z++)
			{
				LazyOptional<IChunkProtection> c = chunks[(x>>4)-cx1][(z>>4)-cz1];
				final int x2=x,z2=z;
				c.ifPresent(pro -> {
					for(int y=box.minY();y<=box.maxY();y++)
					{
						BlockPos pos = new BlockPos(x2,y,z2);
						pro.setRawProtectionState(pos, (byte) (IChunkProtection.BLOCK_NOT_BREAKABLE | IChunkProtection.BLOCK_NOT_PLACABLE));
					}
				});

			}
		}
	}
	
	public static boolean isUnbreakable(Level w, BlockPos pos)
	{
		if(w.isClientSide)
			return false;
		Block bl = w.getBlockState(pos).getBlock();
		
		if(whitelist.contains(bl))
			return false;
		
		LevelChunk c = (LevelChunk) w.getChunk(pos);
		LazyOptional<Boolean> bool = c.getCapability(cap_PROTECTION, null).lazyMap(proc -> 
		{
			byte s = proc.getRawProtectionState(pos);
			return IChunkProtection.check(s, IChunkProtection.BLOCK_NOT_BREAKABLE);
		});
		
		return bool.orElse(false);
	}
	
	public static boolean isUnplaceable(Level w, BlockPos pos)
	{
		if(w.isClientSide)
			return false;
		
		LevelChunk c = (LevelChunk) w.getChunk(pos);
		LazyOptional<Boolean> bool = c.getCapability(cap_PROTECTION, null).lazyMap(proc -> 
		{
			byte s = proc.getRawProtectionState(pos);
			return IChunkProtection.check(s, IChunkProtection.BLOCK_NOT_PLACABLE);
		});
		return bool.orElse(false);
	}
	
	@SubscribeEvent
	public void addChunkCaps(AttachCapabilitiesEvent<LevelChunk> e)
	{
		LevelChunk c = e.getObject();
		if(c.getLevel() == null)
		{
			return; // most likely optifine is installed and doing some shit... 
		}
		else if(!c.getLevel().isClientSide)
		{
			e.addCapability(new ResourceLocation(Constants.MOD_ID, "protection"), new ProtectionProvider());
		}
	}


	@SubscribeEvent
	public void onChunkOpen(ChunkDataEvent.Load event)
	{
		if(event.getWorld() == null)
			return; //this is weird that this can happen
		if(event.getWorld().isClientSide())
			return;
		
		DungeonProtection dun = getInstance((Level) event.getWorld());		
		
		ChunkPos pcp = event.getChunk().getPos();
		CompoundTag nbt = event.getData().getCompound(Constants.MOD_ID);
		Tag base = nbt.get("protection");
		
		if(base!=null)
		{
			LevelChunk c = (LevelChunk) event.getChunk();
			c.getCapability(cap_PROTECTION, null).ifPresent(p ->
			{
				CapabilityChunkProtection protection = (CapabilityChunkProtection) p;
				protection.deserializeNBT((ByteArrayTag) base);
				
				Map<Integer, Byte> map = dun.queed.remove(pcp);
				if(map!=null)
				{
					protection.addProtection(map);
					FPLog.logger.info("Applied queed protection to %s (todo:%s)", pcp, dun.queed.size());
				}	
			});
		}				
	}
	
	@SubscribeEvent
	public void onPlayerBreakBlock(BlockEvent.BreakEvent event)
	{
		if(event.getWorld().isClientSide())
			return;
			
		if(isUnbreakable((Level) event.getWorld(), event.getPos()))
		{
			event.setCanceled(true);
		}
	}
	
	@SubscribeEvent
	public void onPlayerPlaceBlock(BlockEvent.EntityPlaceEvent event)
	{
		if(event.getWorld().isClientSide())
			return;

		if(isUnplaceable((Level) event.getWorld(), event.getPos()))
		{
			event.setCanceled(true);
		}
	}
	
	@SubscribeEvent
	public void onExplosion(ExplosionEvent.Detonate event)
	{
		if(event.getWorld().isClientSide)
			return;
		
		Iterator<BlockPos> iter = event.getAffectedBlocks().iterator();
		while(iter.hasNext())
		{
			BlockPos pos = iter.next();
			LevelChunk c = (LevelChunk) event.getWorld().getChunk(pos);
			c.getCapability(cap_PROTECTION, null).ifPresent(prot ->
			{
				byte state = prot.getRawProtectionState(pos);
				if(IChunkProtection.check(state, IChunkProtection.BLOCK_NOT_BREAKABLE))
				{
					iter.remove();
				}
			});
		}
	}
	
	@SubscribeEvent
	public void onBucketUsed(FillBucketEvent event)
	{
		if(event.getWorld().isClientSide)
			return;
		
		if(event.getTarget() == null)
			return;
		if(event.getTarget().getType() != Type.BLOCK)
			return;
		
		
		BlockPos pos = ((BlockHitResult)event.getTarget()).getBlockPos();
		LevelChunk c = (LevelChunk) event.getWorld().getChunk(pos);
		c.getCapability(cap_PROTECTION, null).ifPresent(prot ->
		{
			byte state = prot.getRawProtectionState(pos);
			
			if(event.getEmptyBucket().getItem() == Items.BUCKET && IChunkProtection.check(state, IChunkProtection.BLOCK_NOT_BREAKABLE))
			{
				event.setCanceled(true);
				return;
			}
			if(event.getEmptyBucket().getItem() != Items.BUCKET && IChunkProtection.check(state, IChunkProtection.BLOCK_NOT_PLACABLE))
			{
				event.setCanceled(true);
				return;
			}
		});
	}
	
	@SubscribeEvent
	public void onWorldLoad(WorldEvent.Load event)
	{
		if(event.getWorld().isClientSide())
			return;
	
		Level w = (Level) event.getWorld();
		File dir = HelperChunks.getDimensionDir(w);
		dir.mkdirs();
		File file = new File(dir, "FPDungeons.dat");
		
		try
		{
			FileInputStream in = new FileInputStream(file);
			GZIPInputStream gin = new GZIPInputStream(in);
			
			getInstance((Level) event.getWorld()).loadQueed(gin);
			
			gin.close();
		}
		catch (FileNotFoundException e) {
			//this is ok, because it can (new word creation)
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@SubscribeEvent
	public void onWorldSave(WorldEvent.Save event)
	{
		if(event.getWorld().isClientSide())
			return;
		Level w = (Level) event.getWorld();
		File dir = HelperChunks.getDimensionDir(w);
		dir.mkdirs();
		File file = new File(dir, "FPDungeons.dat");	
		AsyncTaskManager.addTask(AsyncTaskManager.FILE_IO, new Callable<Boolean>()
		{
			@Override
			public Boolean call() throws Exception 
			{
				try
				{
					FileOutputStream out = new FileOutputStream(file);
					GZIPOutputStream gout = new GZIPOutputStream(out);
					
					getInstance((Level) event.getWorld()).saveQueed(gout);
					
					gout.close();
				}
				catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				return true;
			}
		});
	}
	
	private static class DungeonProtection
	{
		private HashMap<ChunkPos, Map<Integer, Byte>> queed = new HashMap<ChunkPos, Map<Integer, Byte>>();
		
//		public final World w;
		
		
//		public DungeonProtection(World w)
//		{
//			this.w = w;
//		}

		private void loadQueed(InputStream s) throws IOException
		{
			DataInputStream in = new DataInputStream(s);
			int c = in.readInt();
			
			for(int i=0;i<c;i++)
			{
				long l = in.readLong();
				int length = in.readInt();
				if(length<0)
					throw new IOException("File is corrupted, tried to read negative amount of bytes");
				byte[] data = new byte[length];
				in.readFully(data);
				
				int x = (int)(  l & 4294967295L);
				int z = (int)( (l>>32) & 4294967295L);
				ChunkPos pos = new ChunkPos(x, z);
				CapabilityChunkProtection protection = new CapabilityChunkProtection();
				ByteArrayTag arr = new ByteArrayTag(data);
				protection.deserializeNBT( arr);
				
				if(queed.containsKey(pos))
				{
					Map<Integer, Byte> map = queed.get(pos);
					protection.addProtection(map);
					queed.put(pos, protection.map);
				}
				else
				{
					queed.put(pos, protection.map);
				}	
			}
			in.close();
		}
		
		private void saveQueed(OutputStream o) throws IOException
		{
			Entry<ChunkPos, HashMap<Integer, Byte>>[] set = queed.entrySet().toArray(new Entry[queed.entrySet().size()]);			
			DataOutputStream out = new DataOutputStream(o);
			out.writeInt(set.length);
			for(Entry<ChunkPos, HashMap<Integer, Byte>> e : set)
			{
				CapabilityChunkProtection protection = new CapabilityChunkProtection();
				protection.addProtection(e.getValue());
				ByteArrayTag arr = protection.serializeNBT();
				
				byte[] bb = arr.getAsByteArray();	
				long l = ChunkPos.asLong(e.getKey().x, e.getKey().z);
				
				out.writeLong(l);
				out.writeInt(bb.length);
				out.write(bb);
			}
			out.flush();
			out.close();
		}
		
	}
	
	public static void addBlockToWhiteList(ResourceLocation block)
	{
		Block bl = ForgeRegistries.BLOCKS.getValue(block);
		if(bl!=null && bl!= Blocks.AIR)
		{
			whitelist.add(bl);
		}
	} 
	
	public static int order(Block b1, Block b2)
	{
		return b1.hashCode() - b2.hashCode();
	}

	private static class ProtectionProvider implements ICapabilityProvider, INBTSerializable<Tag>
	{
		CapabilityChunkProtection cap = new CapabilityChunkProtection();
		LazyOptional<CapabilityChunkProtection> opt = LazyOptional.of(() -> cap);
		
		private final int LENGTH = 16*16 * 256;
		
		@Override
		public Tag serializeNBT()
		{
			return writeNBT(cap, null);
		}

		@Override
		public void deserializeNBT(Tag nbt)
		{
			readNBT(cap, null, nbt);
		}

		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
		{
			if(capability == cap_PROTECTION)
			{
				return opt.cast();
			}
			return LazyOptional.empty();
		}
		
		public Tag writeNBT(IChunkProtection instance, Direction side)
		{
			if(instance.hasChunkProtection())
			{
				byte[] data = new byte[LENGTH];
				int i = 0;
				for(BlockPos pos : BlockPos.betweenClosed(0, 0, 0, 15, 255, 15))
				{
					i = (pos.getX()<<12) | (pos.getZ()<<8) | (pos.getY());
					
					if(i >= LENGTH)
						System.out.println(pos);
					data[i] = instance.getRawProtectionState(pos);
					i++;
				}
				return new ByteArrayTag(data);
			}
			else
			{
				return ByteTag.valueOf((byte)1);
			}
		}

		public void readNBT(IChunkProtection instance, Direction side, Tag nbt)
		{
			if(nbt instanceof ByteArrayTag)
			{
				byte[] data = ((ByteArrayTag) nbt).getAsByteArray();
				if(data.length!=LENGTH)
					throw new IllegalArgumentException("ArraySizes do not match! Got " + data.length + ", but expected " + LENGTH);
				
				int i = 0;
				for(BlockPos pos : BlockPos.betweenClosed(0, 0, 0, 15, 255, 15))
				{
					i = (pos.getX()<<12) | (pos.getZ()<<8) | (pos.getY());
					instance.setRawProtectionState(pos, data[i]);
				}
			}
		}
	}
	
}
