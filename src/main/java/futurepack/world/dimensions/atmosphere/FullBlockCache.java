package futurepack.world.dimensions.atmosphere;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Predicate;

import it.unimi.dsi.fastutil.longs.Long2ObjectRBTreeMap;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.shapes.VoxelShape;

public class FullBlockCache implements Comparable<FullBlockCache>
{
	/**
	 * is this a full block
	 */
	private BitSet full_set;
	/**
	 * is this state up to date?
	 */
	private BitSet test_set;
	
	public final Predicate<BlockPos> test_isFullBlock;
	public final ChunkPos position;
	private final ReadWriteLock rwLock;
	private final List<Integer> dirtyBlockPosses = Collections.synchronizedList(new ArrayList<>(16));
	
	public FullBlockCache(ChunkPos position, Predicate<BlockPos> isFullBlock) 
	{
		full_set = new BitSet(16*16 * 256);
		test_set = new BitSet(16*16 * 256);
		test_isFullBlock = isFullBlock;
		this.position = position;
		
		rwLock = new ReentrantReadWriteLock();
	}
	
	public static int getIndex(BlockPos pos)
	{
		return (pos.getX()&15) + (pos.getZ()&15)*16 + (pos.getY()&255)*256;
	}
	
	public boolean isFullBlock(BlockPos pos)
	{
		final int index = getIndex(pos);
		Lock rLock = rwLock.readLock();
		Lock wLock = rwLock.writeLock();
		
		if(wLock.tryLock())
		{
			try
			{
				dirtyBlockPosses.forEach(p -> test_set.set(p, false));
				dirtyBlockPosses.clear();
			}
			finally 
			{
				wLock.unlock();
			}
		}
		
		rLock.lock();
		
		if(test_set.get(index) && Collections.binarySearch(dirtyBlockPosses, index)<0) // checks if dirtyBlockPosses does not contain the current index
		{
			boolean state = full_set.get(index);
			rLock.unlock();
			return state;
		}
		else
		{
			rLock.unlock();
			
			boolean full = this.test_isFullBlock.test(pos);//this is outside the lock blocks, because it gets the block state from the main thread and that could lock the server otherwise.
			wLock.lock();
			
			dirtyBlockPosses.forEach(p -> test_set.set(p, false));
			dirtyBlockPosses.clear();
			full_set.set(index, full);
			test_set.set(index, true);
			wLock.unlock();
			return full;
		}
	}
	
	public void notifyBlockChange(BlockPos pos)
	{
		final int index = getIndex(pos);
		Lock l = rwLock.writeLock();
		
		if(l.tryLock())
		{
			try
			{
				test_set.set(index, false);
				dirtyBlockPosses.forEach(p -> test_set.set(p, false));
				dirtyBlockPosses.clear();
			}
			finally 
			{
				l.unlock();
			}
		}
		else
		{
			dirtyBlockPosses.add(index);
			Collections.sort(dirtyBlockPosses);
		}
		
	}
	
	public void markAllDirty()
	{
		test_set.clear();
	}
	
	@Override
	public int compareTo(FullBlockCache o) 
	{
		return (int) (o.position.toLong() - this.position.toLong());
	}
	
	/////////////////////////////////////////////////////////////////////
	// Now begins the static Part for smooth interaction ////////////////
	/////////////////////////////////////////////////////////////////////
	
	private static Map<Level, SoftReference<Map<Long, FullBlockCache>>> map;
	
	static
	{
		map = Collections.synchronizedMap(new WeakHashMap<Level, SoftReference<Map<Long, FullBlockCache>>>());
		
		
	}
	
	private static boolean isFullBlockBase(VoxelShape voxelShape)
	{
		if(voxelShape==null || voxelShape.isEmpty())
			return false;
		
		AABB bb = voxelShape.bounds();
		
		double x = bb.maxX - bb.minX;
		double y = bb.maxY - bb.minY;
		double z = bb.maxZ - bb.minZ;
		return x>=1D && y>=1D && z>=1D;
	}
	
	private static boolean isFullBlock_impl(BlockPos pos, LevelAccessor w)
	{
		BlockState state = w.getBlockState(pos);
		return isFullBlock(pos, w, state);
	}
	
	private static boolean isFullBlock(BlockPos pos, LevelAccessor w, BlockState state)
	{
		if(state.getBlock()== Blocks.AIR)
			return false;
		if(state.getMaterial().isLiquid())
			return true;
		if(!state.getFluidState().isEmpty())
			return true;
		
		if(state.isRedstoneConductor(w, pos))
		{
			return true;
		}
		else
		{
			return isFullBlockBase(state.getShape(w, pos));
		}
	}
	
	public static boolean isFullBlock(BlockPos pos, Level w)
	{
		//also thread save
		
		SoftReference<Map<Long, FullBlockCache>> ref = map.computeIfAbsent(w, ww -> new SoftReference(new Long2ObjectRBTreeMap<FullBlockCache>()));
		Map<Long, FullBlockCache> chunks = ref.get();
		if(chunks!=null)
		{
			long position = ChunkPos.asLong(pos.getX(), pos.getZ());
			FullBlockCache fb_cache;
			synchronized (chunks) 
			{
				fb_cache = chunks.computeIfAbsent(position, l -> new FullBlockCache(new ChunkPos(l), pp -> isFullBlock_impl(pp, w)));				
			}
			return fb_cache.isFullBlock(pos);
		}
		else
		{
			map.remove(w);
		}
		return isFullBlock_impl(pos, w);
	}
	
	public static FullBlockCache getChunkCache(BlockPos pos, Level w)
	{
		SoftReference<Map<Long, FullBlockCache>> ref = map.computeIfAbsent(w, ww -> new SoftReference(new Long2ObjectRBTreeMap<FullBlockCache>()));
		Map<Long, FullBlockCache> chunks = ref.get();
		if(chunks!=null)
		{
			long position = ChunkPos.asLong(pos.getX(), pos.getZ());
			FullBlockCache fb_cache;
			synchronized (chunks) 
			{
				fb_cache = chunks.computeIfAbsent(position, l -> new FullBlockCache(new ChunkPos(l), pp -> isFullBlock_impl(pp, w)));				
			}
			return fb_cache;
		}
		return null;
	}
	
	public static void notifyBlockChange(BlockPos pos, Level w)
	{
		SoftReference<Map<Long, FullBlockCache>> ref = map.get(w);
		if(ref!=null)
		{
			Map<Long, FullBlockCache> chunks = ref.get();
			if(chunks!=null)
			{
				long position = ChunkPos.asLong(pos.getX(), pos.getZ());
				FullBlockCache fb_cache;
				synchronized (chunks) 
				{
					fb_cache = chunks.get(position);				
				}
				if(fb_cache!=null)
				{
					fb_cache.notifyBlockChange(pos);
				}
			}
		}
	}
	
	private static int tick = 0;

	public static void onWorldTick(Level w) 
	{
		if(tick++ >= 20*60*10) //every 10 minutes a full cache clear
		{
			tick = 0;
			SoftReference<Map<Long, FullBlockCache>> ref = map.get(w);
			if(ref!=null)
			{
				Map<Long, FullBlockCache> chunks = ref.get();
				if(chunks!=null)
				{
					synchronized (chunks) 
					{
						chunks.entrySet().stream().map(m -> m.getValue()).forEach(f -> f.markAllDirty());
					}
					
				}
			}
		}
	}
}
