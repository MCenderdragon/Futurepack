package futurepack.generating.connected;

import java.util.HashMap;
import java.util.Map;

public class GenBlockState {
	public Map<String, GenBlockStateModel> variants;
	
	public GenBlockState() {
		variants = new HashMap<>();
	}
	
	public void addVariant(String variant, GenBlockStateModel variantModel) {
		this.variants.put(variant, variantModel);
	}
}
