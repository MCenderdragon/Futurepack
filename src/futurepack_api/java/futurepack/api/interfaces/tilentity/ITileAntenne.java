package futurepack.api.interfaces.tilentity;

public interface ITileAntenne extends ITileNetwork
{
	public int getRange();
	
	public int getFrequenz();
	
	public void setFrequenz(int integer);
}
