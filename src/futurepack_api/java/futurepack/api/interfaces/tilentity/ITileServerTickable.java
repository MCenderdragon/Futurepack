package futurepack.api.interfaces.tilentity;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;

public interface ITileServerTickable 
{
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState);
}
