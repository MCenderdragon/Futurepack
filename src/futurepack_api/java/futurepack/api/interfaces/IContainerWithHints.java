package futurepack.api.interfaces;

import java.util.List;
import java.util.Map;

import net.minecraft.network.chat.Component;
import net.minecraft.world.inventory.Slot;

public interface IContainerWithHints
{
	public void addHints(Map<Slot, List<Component>> slot2hints);
}
