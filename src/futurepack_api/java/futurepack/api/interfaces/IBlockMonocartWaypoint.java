package futurepack.api.interfaces;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;

/**
 * Implement this in a Block to make it a monorail waypoint
 */
public interface IBlockMonocartWaypoint
{
	/**
	 * @param w the World
	 * @param pos the position of the b lock
	 * @param state the current blockstate
	 * @return THe name of this waypoint
	 */
	public Component getName(Level w, BlockPos pos, BlockState state);
	
	/**
	 * @param w the World
	 * @param pos the position of the b lock
	 * @param state the current blockstate
	 * @return if this is realy a waypoint
	 */
	public boolean isWaypoint(Level w, BlockPos pos, BlockState state);
}
