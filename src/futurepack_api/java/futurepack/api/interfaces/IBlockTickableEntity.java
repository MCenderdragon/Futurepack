package futurepack.api.interfaces;

import javax.annotation.Nullable;

import futurepack.api.helper.HelperTileEntities;
import futurepack.api.interfaces.tilentity.ITileClientTickable;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public interface IBlockTickableEntity extends EntityBlock
{
	@Nullable
	BlockEntity newBlockEntity(BlockPos pPos, BlockState pState);

	@Nullable
	default <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) 
	{
		return pLevel.isClientSide() ? HelperTileEntities.createClientTickerHelper(pBlockEntityType, getClientEnityType(pLevel, pState)) : HelperTileEntities.createServerTickerHelper(pBlockEntityType, getServerEntityType(pLevel, pState));
	}
	
	public BlockEntityType<? extends ITileClientTickable> getClientEnityType(Level pLevel, BlockState pState);
	
	public BlockEntityType<? extends ITileServerTickable> getServerEntityType(Level pLevel, BlockState pState);
}
