package futurepack.api.capabilities;

import javax.annotation.Nonnull;

import futurepack.api.laser.LightProperties;

public interface IOpticalBlock 
{
	/**
	 * 
	 * @param light the light to add
	 * @param simulate if this simulated or not
	 * @return the light that was accepted
	 */
	@Nonnull
	public LightProperties add(LightProperties light, boolean simulate);
	
	
	/**
	 * 
	 * @param light the light to remove
	 * @param simulate if this simulated or not
	 * @return the light that was removed
	 */
	@Nonnull
	public LightProperties remove(LightProperties light, boolean simulate);
	
	
	/**
	 * @param light
	 * @return the part of the light in the right color/spectrum/wavelength
	 */
	public LightProperties getRightWavelength(LightProperties light);
	
	/**
	 * @return the lights this Block has
	 */
	@Nonnull
	public LightProperties getLights();
	
}
