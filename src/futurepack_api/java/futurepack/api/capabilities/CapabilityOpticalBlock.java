package futurepack.api.capabilities;

import futurepack.api.laser.LightProperties;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.commands.TagCommand;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.util.INBTSerializable;

public class CapabilityOpticalBlock implements IOpticalBlock, INBTSerializable<CompoundTag>
{
	public static final Capability<IOpticalBlock> cap_OPTICAL = CapabilityManager.get(new CapabilityToken<>(){});
	
	
	protected double capacity;
	protected LightProperties light = LightProperties.EMPTY;
	
	public CapabilityOpticalBlock(double capacity) 
	{
		this.capacity = capacity;
	}
	
	@Override
	public LightProperties add(LightProperties light, boolean simulate) 
	{
		LightProperties part = getRightWavelength(light);
		
		double toAdd = Math.min(capacity - light.intensity(), part.intensity());
		
		part = part.withIntensity(toAdd);
		
		if(!simulate)
		{
			this.light = this.light.add(part);
		}
		
		return part;
	}

	@Override
	public LightProperties remove(LightProperties l, boolean simulate) 
	{
		LightProperties toRemove = light.min(l);
		
		if(!simulate)
		{
			this.light = light.substract(toRemove);
		}
	
		return toRemove;
	}

	@Override
	public LightProperties getRightWavelength(LightProperties light) 
	{
		return light;
	}

	@Override
	public LightProperties getLights() 
	{
		return light;
	}

	public void clear() 
	{
		light = LightProperties.EMPTY;
	}

	@Override
	public CompoundTag serializeNBT() 
	{
		CompoundTag tag = new CompoundTag();
		
		tag.putDouble("capacity", capacity);
		tag.putInt("rgb", light.color());
		tag.putInt("uvi", light.infraRed_UV());
		tag.putDouble("intensity", light.intensity());
		
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundTag nbt) 
	{
		capacity = nbt.getDouble("capacity");
		
		int rgb = nbt.getInt("rgb");
		int uvi = nbt.getInt("uvi");
		double intensity = nbt.getDouble("intensity");
		
		light = new LightProperties(rgb, uvi, intensity);
	}

}
