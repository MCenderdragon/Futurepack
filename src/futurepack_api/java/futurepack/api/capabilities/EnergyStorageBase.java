package futurepack.api.capabilities;

import javax.annotation.Nullable;

public abstract class EnergyStorageBase implements IEnergyStorageBase
{
	protected int energy = 0;
	private final int maxenergy;
	private final EnumEnergyMode mode;
	@Nullable
	private final Runnable changedCallback;
	
	public EnergyStorageBase(int maxpower, EnumEnergyMode type, Runnable changedCallback)
	{
		this.maxenergy = maxpower;
		this.energy = 0;
		this.mode = type;
		this.changedCallback = changedCallback;
	}

	@Override
	public int get()
	{
		return energy;
	}

	@Override
	public int getMax()
	{
		return maxenergy;
	}

	@Override
	public int use(int used)
	{
		if(energy >= used)
		{
			energy -= used;
			changedCallback.run();
			return used;
		}
		else
		{
			int p = energy;
			energy = 0;
			changedCallback.run();
			return p;
		}
	}

	@Override
	public int add(int added)
	{
		if(energy + added <= maxenergy)
		{
			energy += added;
			changedCallback.run();
			return added; 
		}
		else
		{
			int p = maxenergy - energy;
			energy = maxenergy;
			changedCallback.run();
			return p;
		}
	}
	
	@Override
	public EnumEnergyMode getType()
	{
		return mode;
	}
	
	public void set(int e)
	{
		energy = e;
		changedCallback.run();
	}
}
