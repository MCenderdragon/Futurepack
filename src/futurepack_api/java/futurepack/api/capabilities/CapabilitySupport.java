package futurepack.api.capabilities;

import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.util.INBTSerializable;


public class CapabilitySupport extends EnergyStorageBase implements ISupportStorage, INBTSerializable<CompoundTag>
{
	public static final Capability<ISupportStorage> cap_SUPPORT = CapabilityManager.get(new CapabilityToken<>(){});
	
//	public static class Storage implements IStorage<ISupportStorage>
//	{
//
//		@Override
//		public Tag writeNBT(Capability<ISupportStorage> capability, ISupportStorage instance, Direction side)
//		{
//			return IntTag.valueOf(instance.get());
//		}
//
//		@Override
//		public void readNBT(Capability<ISupportStorage> capability, ISupportStorage instance, Direction side, Tag nbt)
//		{
//			int sup = ((IntTag)nbt).getAsInt();
//			if(sup > instance.get())
//			{
//				instance.add(sup - instance.get());
//			}
//			else if(sup < instance.get())
//			{
//				instance.add(instance.get() - sup);
//			}
//		}
//		
//	}}
	
	public CapabilitySupport(int max, EnumEnergyMode mode, Runnable changedCallback)
	{
		super(max, mode, changedCallback);
	}

	@Override
	public void support() { }

	
	@Override
	public boolean canTransferTo(ISupportStorage other)
	{
		return getType().getPriority() < other.getType().getPriority();
	}

	@Override
	public boolean canAcceptFrom(ISupportStorage other)
	{
		return get() < getMax();
	}
	
	@Override
	public CompoundTag serializeNBT()
	{
		CompoundTag nbt = new CompoundTag();
		nbt.putInt("s", get());
		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundTag nbt)
	{
		energy = nbt.getInt("s");
	}
}
