package futurepack.api.laser;

public record LaserProperties(int wavelength, double intensity) {

}
