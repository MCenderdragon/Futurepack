package futurepack.api.laser;

public record LightProperties(int color, int infraRed_UV, double intensity) 
{
	public static LightProperties EMPTY = new LightProperties(0, 0 ,0);
	
	public double getRed()
	{
		return (color & 0xFF) / 255.0 * intensity;
	}
	
	public double getGreen()
	{
		return ((color>>8) & 0xFF) / 255.0 * intensity;
	}
	
	public double getBlue()
	{
		return ((color>>16) & 0xFF) / 255.0 * intensity;
	}
	
	public double getUV()
	{
		return ((infraRed_UV>>8) & 0xFF) / 255.0 * intensity;
	}
	
	public double getInfrared()
	{
		return ((infraRed_UV) & 0xFF) / 255.0 * intensity;
	}
	
	public boolean isEmpty()
	{
		return this == EMPTY || intensity <= 0;
	}
	
	public LightProperties withIntensity(double intensity) 
	{
		return new LightProperties(color, infraRed_UV, intensity);
	}
	
	public LightProperties substract(LightProperties other)
	{
		return substract(this, other);
	}
	
	public LightProperties add(LightProperties other) 
	{
		return mix(this, other);
	}
	
	public LightProperties min(LightProperties other)
	{
		double r = Math.min(this.getRed(), other.getRed());
		double g = Math.min(this.getGreen(), other.getGreen());
		double b = Math.min(this.getBlue(), other.getBlue());
		double uv = Math.min(this.getUV(), other.getUV());
		double infra = Math.min(this.getInfrared(), other.getInfrared());
		
		return fromIntensities(r, g, b, infra, uv);
	}
	
	public int getRenderColorRGBA()
	{
		//assuming intensity of 1361W is maximum bightness;
		
		int r = (int) (Math.min(getRed(), 1361) / 1361D * 0xFF);
		int g = (int) (Math.min(getGreen(), 1361) / 1361D * 0xFF);
		int b = (int) (Math.min(getBlue(), 1361) / 1361D * 0xFF);
		
		return r | (g<<8) | (b<<16) | (0xFF<<24);
	}
	
	
	public static LightProperties mix(LightProperties...lights)
	{
		double r=0,g=0,b=0,uv=0,infra=0;
		
		for(var l : lights)
		{
			r += l.getRed();
			g += l.getGreen();
			b += l.getBlue();
			uv += l.getUV();
			infra += l.getInfrared();
		}
		
		return fromIntensities(r, g, b, infra, uv);
	}
	
	/**
	 * @param a
	 * @param b
	 * @return a - b
	 */
	public static LightProperties substract(LightProperties a, LightProperties b)
	{
		return fromIntensities(a.getRed() - b.getRed(), a.getGreen() - b.getGreen(), a.getBlue() - b.getBlue(), a.getInfrared() - b .getInfrared(), a.getUV() - b.getUV());
	}
	
	public static LightProperties fromIntensities(double red, double green, double blue, double infraRed, double uv)
	{
		if(red < 0 || green < 0 || blue < 0 || infraRed < 0 || uv < 0)
			throw new IllegalArgumentException("intensity smaller 0");
		
		int r,g,b,infra,uv2;
		
		double intensity = red+green+blue+infraRed+uv;
		
		r = (int)(255.0 * red/intensity);
		g = (int)(255.0 * green/intensity);
		b = (int)(255.0 * blue/intensity);
		infra = (int)(255.0 * infraRed/intensity);
		uv2 = (int)(255.0 * uv/intensity);
		
		r &= 0xFF;
		g &= 0xFF;
		b &= 0xFF;
		infra &= 0xFF;
		uv2 &= 0xFF;
		
		int color = r | (g<<8) | (b<<16);
		int inf_uv = infra | (uv2<<8);
		
		return new LightProperties(color, inf_uv, intensity);
	}
}
