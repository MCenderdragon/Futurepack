package futurepack.extensions.jei;

public class RecipeRuntimeEditor
{
	protected static IRecipeEditor editor;
	
	
	public static void addRecipe(Object o)
	{
		if(editor!=null)
			editor.addRecipe(o);
	}
	
	public static void removeRecipe(Object o)
	{
		if(editor!=null)
			editor.removeRecipe(o);
	}

	
	public static interface IRecipeEditor
	{
		void addRecipe(Object o);
			
		void removeRecipe(Object o);
	}
}
