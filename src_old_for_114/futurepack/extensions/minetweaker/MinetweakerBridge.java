//package futurepack.extensions.minetweaker;
//
//import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Method;
//
//public class MinetweakerBridge 
//{
//	/**
//	 * Called from FPMain#preInit(FMLPreInitializationEvent event)
//	 */
//	public static void init()
//	{
//		try
//		{
//			Class<?> cls = Class.forName("minetweaker.MineTweakerAPI");
//			Class<?> zensymbol = Class.forName("stanhebben.zenscript.symbols.IZenSymbol");
//			
//			Method m_regClasses = cls.getMethod("registerClassRegistry", String.class);
//			Method m_global = cls.getMethod("registerGlobalSymbol", String.class, zensymbol);
//			Method m_getSymbol = cls.getMethod("getJavaStaticFieldSymbol", Class.class, String.class);
//			
//			m_regClasses.invoke(null, "futurepack.minetweaker.ClassRegistry");
//			
//			Class<?> reg = Class.forName("futurepack.minetweaker.ClassRegistry");
//			Object o = m_getSymbol.invoke(null, reg, "bridge");
//			m_global.invoke(null, "futurepack", o);
//			
//		} 
//		catch (ClassNotFoundException e) {
//			//e.printStackTrace(); - since this is pretty normal I wont catch it
//		} catch (NoSuchMethodException e) {
//			e.printStackTrace();
//		} catch (SecurityException e) {
//			e.printStackTrace();
//		} catch (IllegalAccessException e) {
//			e.printStackTrace();
//		} catch (IllegalArgumentException e) {
//			e.printStackTrace();
//		} catch (InvocationTargetException e) {
//			e.printStackTrace();
//		}
//	}
//	
//	
//	
//}
