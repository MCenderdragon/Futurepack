// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class wolba<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "wolba"), "main");
	private final ModelPart wolba;

	public wolba(ModelPart root) {
		this.wolba = root.getChild("wolba");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition wolba = partdefinition.addOrReplaceChild("wolba", CubeListBuilder.create().texOffs(46, 0).addBox(-3.0F, -12.0F, -4.5F, 6.0F, 5.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(18, 8).addBox(-2.5F, -11.5F, -1.5F, 5.0F, 4.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(46, 0).addBox(-3.0F, -12.0F, 1.5F, 6.0F, 5.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-2.5F, -11.5F, 4.5F, 5.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.5F));

		PartDefinition neck = wolba.addOrReplaceChild("neck", CubeListBuilder.create(), PartPose.offset(0.0F, -10.0F, -4.5F));

		PartDefinition neck_r1 = neck.addOrReplaceChild("neck_r1", CubeListBuilder.create().texOffs(20, 0).addBox(-1.5F, -1.5F, -1.0F, 3.0F, 3.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -1.0F, -2.0F, -0.4461F, 0.0F, 0.0F));

		PartDefinition head = neck.addOrReplaceChild("head", CubeListBuilder.create().texOffs(16, 33).addBox(-3.0F, -1.0F, -1.0F, 4.0F, 4.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(0, 17).addBox(-2.5F, 1.0F, -2.0F, 3.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(1.0F, -2.0F, -5.0F));

		PartDefinition mouse = head.addOrReplaceChild("mouse", CubeListBuilder.create(), PartPose.offset(-1.0F, 3.0F, 1.0F));

		PartDefinition mouse_r1 = mouse.addOrReplaceChild("mouse_r1", CubeListBuilder.create().texOffs(0, 34).addBox(-1.5F, -0.1F, -3.0F, 3.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.1487F, 0.0F, 0.0F));

		PartDefinition antler_l = head.addOrReplaceChild("antler_l", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition antler_l1_r1 = antler_l.addOrReplaceChild("antler_l1_r1", CubeListBuilder.create().texOffs(0, 12).addBox(-0.5F, 0.0F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.0F, -1.0F, 1.0F, 1.0129F, 0.0F, 0.0F));

		PartDefinition antler_l2_r1 = antler_l.addOrReplaceChild("antler_l2_r1", CubeListBuilder.create().texOffs(0, 12).addBox(-0.5F, -1.0F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.0F, -1.0F, 0.0F, -0.1396F, 0.0F, 0.0F));

		PartDefinition antler_big_r1 = antler_l.addOrReplaceChild("antler_big_r1", CubeListBuilder.create().texOffs(46, 11).addBox(-0.49F, -1.0F, 0.0F, 1.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.0F, 0.0F, 0.0F, 0.3346F, 0.0F, 0.0F));

		PartDefinition antler_r = head.addOrReplaceChild("antler_r", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition antler_r1_r1 = antler_r.addOrReplaceChild("antler_r1_r1", CubeListBuilder.create().texOffs(0, 12).addBox(-0.5F, 0.0F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-3.0F, -1.0F, 1.0F, 1.0129F, 0.0F, 0.0F));

		PartDefinition antler_r2_r1 = antler_r.addOrReplaceChild("antler_r2_r1", CubeListBuilder.create().texOffs(0, 12).addBox(-0.5F, -1.0F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-3.0F, -1.0F, 0.0F, -0.1396F, 0.0F, 0.0F));

		PartDefinition antler_big_r2 = antler_r.addOrReplaceChild("antler_big_r2", CubeListBuilder.create().texOffs(46, 11).addBox(-0.51F, -1.0F, 0.0F, 1.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-3.0F, 0.0F, 0.0F, 0.3346F, 0.0F, 0.0F));

		PartDefinition wool = wolba.addOrReplaceChild("wool", CubeListBuilder.create().texOffs(0, 16).addBox(-3.5F, -0.5F, -1.0F, 7.0F, 5.0F, 11.0F, new CubeDeformation(0.0F))
		.texOffs(37, 21).addBox(-2.5F, -1.5F, -0.5F, 5.0F, 2.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -12.0F, -4.5F));

		PartDefinition tail = wolba.addOrReplaceChild("tail", CubeListBuilder.create().texOffs(66, 0).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 8.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -12.0F, 4.5F));

		PartDefinition leg_fr = wolba.addOrReplaceChild("leg_fr", CubeListBuilder.create().texOffs(36, 0).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 7.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-2.3F, -10.0F, -3.0F));

		PartDefinition shin_fr = leg_fr.addOrReplaceChild("shin_fr", CubeListBuilder.create().texOffs(36, 9).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 6.0F, 0.0F));

		PartDefinition leg_fl = wolba.addOrReplaceChild("leg_fl", CubeListBuilder.create().texOffs(36, 0).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 7.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(2.3F, -10.0F, -3.0F));

		PartDefinition shin_fl = leg_fl.addOrReplaceChild("shin_fl", CubeListBuilder.create().texOffs(36, 9).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 6.0F, 0.0F));

		PartDefinition leg_br = wolba.addOrReplaceChild("leg_br", CubeListBuilder.create().texOffs(36, 0).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 7.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.3F, -10.0F, 3.0F, 0.0F, 0.0F, 0.0F));

		PartDefinition shin_br = leg_br.addOrReplaceChild("shin_br", CubeListBuilder.create().texOffs(36, 9).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 6.0F, 0.0F));

		PartDefinition leg_bl = wolba.addOrReplaceChild("leg_bl", CubeListBuilder.create().texOffs(36, 0).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 7.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.3F, -10.0F, 3.0F, 0.0F, 0.0F, 0.0F));

		PartDefinition shin_bl = leg_bl.addOrReplaceChild("shin_bl", CubeListBuilder.create().texOffs(36, 9).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 6.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		wolba.render(poseStack, buffer, packedLight, packedOverlay);
	}
}